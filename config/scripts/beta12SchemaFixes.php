<?php
/**
 * Drush script to fix schemas in MANH Drupal 8 beta 12.
 * Required before updating to later betas.
 */

$entityManager = \Drupal::entityManager();

$entity_types = [
  'block_content',
  'contact_message',
  //'field_collection',
  'file',
  'node',
  'redirect',
  'shortcut',
  'taxonomy_term',
  //'user',
  'menu_link_content',
];

foreach ($entity_types as $type) {
  drush_print($type);
  $fs_schema = $entityManager->getFieldStorageDefinitions($type);
  $installed_fs_schema = $entityManager->getLastInstalledFieldStorageDefinitions($type);
  foreach ($fs_schema as $key => $def) {
    if (!empty($installed_fs_schema[$key]) /* && $installed_fs_schema[$key] != $fs_schema[$key] */) {
      drush_print(' -- update ' . $key);
      try {
        $entityManager->onFieldStorageDefinitionUpdate($fs_schema[$key], $installed_fs_schema[$key]);
      }
      catch (Exception $ex) {
        drush_print(' ---- Exception: ' . $ex->getMessage());
        drush_print(' ---- continuing.');
      }
    }
    else {
      drush_print(' -- create ' . $key);
      $entityManager->onFieldStorageDefinitionCreate($fs_schema[$key]);
    }
  }
}

/*
// Field Collection
$fs_schema = $entityManager->getFieldStorageDefinitions('field_collection_item');
$installed_fs_schema = $entityManager->getLastInstalledFieldStorageDefinitions('field_collection_item');
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_bp_page'], $installed_fs_schema['field_bp_page']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_bp_page_text'], $installed_fs_schema['field_bp_page_text']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_cust_success'], $installed_fs_schema['field_cust_success']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_cust_success_title'], $installed_fs_schema['field_cust_success_title']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_list_link'], $installed_fs_schema['field_list_link']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_list_text'], $installed_fs_schema['field_list_text']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_media_company'], $installed_fs_schema['field_media_company']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_media_email'], $installed_fs_schema['field_media_email']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_media_name'], $installed_fs_schema['field_media_name']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_media_phone'], $installed_fs_schema['field_media_phone']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_page'], $installed_fs_schema['field_page']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_product_list_item'], $installed_fs_schema['field_product_list_item']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_related'], $installed_fs_schema['field_related']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_related_text'], $installed_fs_schema['field_related_text']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['field_company'], $installed_fs_schema['field_company']);

// User
$fs_schema = $entityManager->getFieldStorageDefinitions('user');
$installed_fs_schema = $entityManager->getLastInstalledFieldStorageDefinitions('user');
$entityManager->onFieldStorageDefinitionCreate($fs_schema['default_langcode']);
$entityManager->onFieldStorageDefinitionCreate($fs_schema['path']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['roles'], $installed_fs_schema['roles']);
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['user_picture'], $installed_fs_schema['user_picture']);
\Drupal::database()->query('update users_field_data set signature=NULL, signature_format=NULL');
$entityManager->onFieldStorageDefinitionDelete($installed_fs_schema['signature']);
$entityManager->onFieldStorageDefinitionDelete($installed_fs_schema['signature_format']);

// Contact Message language field
$fs_schema = $entityManager->getFieldStorageDefinitions('contact_message');
$installed_fs_schema = $entityManager->getLastInstalledFieldStorageDefinitions('contact_message');
$entityManager->onFieldStorageDefinitionUpdate($fs_schema['langcode'], $installed_fs_schema['langcode']);

*/
