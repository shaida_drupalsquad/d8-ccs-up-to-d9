<?php
/**
 * Drush script to update redirect, user, and menu link entities.
 */

// Redirect entity.
// Obtain the most recent field definitions.
$entity_manager = \Drupal::entityManager();
$field_storage_definitions = $entity_manager->getLastInstalledFieldStorageDefinitions('redirect');
if (!isset($field_storage_definitions['created'])) {
  drush_print("Adding 'created' to redirect.");
  // Define the new field. Copy/paste from the entity class’s BaseFieldDefinition.
  // DO NOT call it directly, because the schema may have other changes.
  $field_storage_definition = \Drupal\Core\Field\BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('The date when the redirect was created.'))

    // Specify these values by hand because EntityManager::buildBaseFieldDefinitions()
    // is bypassed when we create/update a single field at a time.
    ->setProvider('redirect')
    ->setName('created')
    ->setTargetEntityTypeId('redirect')
    ->setTargetBundle(NULL);

  // Now that we have a field definition in a known state, let the Entity Manager know about it. It will then delegate to all the necessary handlers to do things like create the database tables and whatever else is needed.
  $entity_manager->onFieldStorageDefinitionCreate($field_storage_definition);
}


// User entity.
// Obtain the most recent field definitions.
// $field_storage_definitions = $entity_manager->getLastInstalledFieldStorageDefinitions('user');
// $new_definitions = $entity_manager->getFieldStorageDefinitions('user');

/*
// User: update UUID.
$entity_manager->onFieldStorageDefinitionUpdate($field_storage_definitions['uuid'], $new_definitions['uuid']);
$entity_manager->onFieldStorageDefinitionUpdate($field_storage_definitions['langcode'], $new_definitions['langcode']);
$entity_manager->onFieldStorageDefinitionUpdate($field_storage_definitions['preferred_langcode'], $new_definitions['preferred_langcode']);
$entity_manager->onFieldStorageDefinitionUpdate($field_storage_definitions['preferred_admin_langcode'], $new_definitions['preferred_admin_langcode']);
$entity_manager->onFieldStorageDefinitionUpdate($field_storage_definitions['roles'], $new_definitions['roles']);
$entity_manager->onFieldStorageDefinitionUpdate($field_storage_definitions['user_picture'], $new_definitions['user_picture']);
*/