#!/bin/bash
# Update Views configuration after the beta7-beta9 update.

# Drush command will be different depending on Vagrant or Acquia.
if [ "$USER" = "manh8" ]
then
  DRUSH=/home/manh8/mydrush/drush
  CONFIG=/var/www/html/manh8.$AH_SITE_ENVIRONMENT/config
  echo "We appear to be running on Acquia, so using $DRUSH as drush command."
else
  DRUSH=drush
  CONFIG=$HOME/config
  echo "We appear to be running on local environment, so using $DRUSH as drush command."
fi

# Export current site configuration to a temporary directory
TEMPOLD=$HOME/__config_temp_orig
TEMPNEW=$HOME/__config_temp_new
echo mkdir $TEMPOLD
mkdir $TEMPOLD

echo ---
echo Exporting configuration to $TEMPOLD
$DRUSH config-export --destination=$TEMPOLD

echo Copying exported configuration to $TEMPNEW for changes
cp -r $TEMPOLD $TEMPNEW

echo Updating Views files in $TEMPNEW
for FILENAME in $TEMPOLD/views.view.*
do
  UUID=`grep uuid $FILENAME`
  CLEANFILE=$CONFIG/beta9views/$(basename $FILENAME)
  NEWFILE=$TEMPNEW/$(basename $FILENAME)
  echo Updating $NEWFILE with $UUID
  cp $CLEANFILE $NEWFILE
  SEDCOMMAND="1s/^/"$UUID"\n/"
  sed -i "$SEDCOMMAND" $NEWFILE
done

echo Importing configuration from $TEMPNEW
$DRUSH config-import -y --source=$TEMPNEW

echo Cleaning up files. Removing $TEMPOLD and $TEMPNEW .
rm -rf $TEMPOLD
rm -rf $TEMPNEW
echo ---
echo Done. 

