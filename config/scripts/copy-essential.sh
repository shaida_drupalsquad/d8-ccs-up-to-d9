#!/bin/bash
# Copy essential configuration files from an exported configuration directory
# to an empty directory.
#
# Usage: copy-essential.sh [from_dir] [to_dir]

# Print a usage line to explain parameters
function print_usage {
  echo "Usage: copy-essential.sh [from_dir] [to_dir]"
}

# Copy file(s) from source to destination
function copy {
  echo "cp $SOURCE_DIR/$1 $DEST_DIR/."
  cp $SOURCE_DIR/$1 $DEST_DIR/.
}

###### Main code ######

# Check parameters
if [[ -z "$1" ]]; then
  print_usage
  exit
fi

if [[ -z "$2" ]]; then
  print_usage
  exit
fi

if [ ! -d "$1" ]; then
  echo "Directory $1 does not exist."
  exit
fi

if [ ! -d "$2" ]; then
  echo "Directory $2 does not exist."
  exit
fi

SOURCE_DIR=$1
DEST_DIR=$2

# Node settings
copy *node*.yml

# Field collection settings
copy *field_collection*.yml

# Taxonomy settings
copy taxonomy.vocabulary.*.yml

# Field settings
copy field.field.*.yml

# Field storage settings
copy field.storage.node.*.yml

# Language settings
copy language.*.yml

# Editor and text format settings
copy editor.editor.restricted_html.yml
copy filter.format.restricted_html.yml

# User settings
copy user.role.*.yml

# Block settings
copy block.block.*.yml

# Module settings
copy crazyegg.settings.yml
copy lingotek.settings.yml

# Theme settings
copy *theme*.yml

# Miscellaneous
copy system.site.yml
copy update.settings.yml
