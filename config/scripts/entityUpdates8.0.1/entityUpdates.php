<?php
/**
 * Drush script to update entity schemas.
 */
drush_print('Entity updates');

$entity_manager = \Drupal::entityManager();

$types = ['block_content', 'field_collection_item', 'taxonomy_term'];
foreach ($types as $type) {
  drush_print('Updating ' . $type);
  $new = $entity_manager->getDefinition($type);
  $old = $entity_manager->getLastInstalledDefinition($type);
  $entity_manager->onEntityTypeUpdate($new, $old);
}
