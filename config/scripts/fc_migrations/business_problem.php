<?php
/**
 * Migrate Business Problem Page field collection to
 * Business Problem Page Reference node, and update references.
 */

use Drupal\node\Entity\Node;

// Retrieve all Business Problem nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'business_problem', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

// Build a table of all Business Problem Page field collection data, in all translations.
$table = [];
/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  drush_print('----------');
  drush_print('Node ' . $node->id() . ': ' . $node->getTitle());
  $languages = $node->getTranslationLanguages();
  /** @var \Drupal\Core\Language\LanguageInterface $lang */
  foreach ($languages as $lang) {
    $langcode = $lang->getId();
    drush_print ('Langcode ' . $langcode);
    $translation = $node->getTranslation($langcode);
    $page_nodes[$node->id()][$langcode] = $translation;
    foreach ($translation->field_bp_page_fc as $delta => $fc) {
      if (!empty($fc->value)) {
        drush_print("Delta $delta; Revision {$fc->revision_id}");
        $fc_entity = \Drupal::entityTypeManager()->getStorage('field_collection_item')->loadRevision($fc->revision_id);
        $target_id = $fc_entity->field_bp_page->target_id;
        $view_mode = $fc_entity->field_bp_page->view_mode;
        $text = $fc_entity->field_bp_page_text->value;
        // Text will be the new node title - if empty, skip this.
        if (empty($text)) {
          drush_print(" -- Skipping $langcode delta $delta because text is empty.");
        } else {
          $source_langcode = $translation->content_translation_source->value;
          $data = new stdClass();
          $data->delta = $delta;
          $data->source_langcode = $source_langcode;
          $table[$node->id()][$target_id][$view_mode][$text][$langcode] = $data;
        }
      }
    }
  }
}

// Delete existing bp_page_reference nodes.
$existing_bpp_nids = \Drupal::entityQuery('node')
  ->condition('type', 'bp_page_reference', '=')
  ->execute();
drush_print('There are ' . count($existing_bpp_nids) . ' existing bp_page_reference nodes. Deleting them now...');
$existing_bpp_nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($existing_bpp_nids);
\Drupal::entityTypeManager()->getStorage('node')->delete($existing_bpp_nodes);

// Iterate through the table, create new bp_page_reference nodes, and reference them.
drush_print_r($table);
foreach ($table as $nid => $data) {
  drush_print('Migrating data for node ' . $nid);
  drush_print('-------');
  foreach ($data as $target_id => $data2) {
    drush_print("Target id: $target_id");
    $bpr_node = NULL;
    $bpr_nid = 0;
    foreach ($data2 as $view_mode => $data3) {
      drush_print("View mode: $view_mode");
      foreach ($data3 as $text => $data4) {
        drush_print("Text: $text");
        foreach ($data4 as $langcode => $item) {
          drush_print("Lang: $langcode");
          // Create node from the source translation.
          if ($item->source_langcode == 'und') {
            // Create bp_page_reference.
            drush_print('Creating BP page reference:');
            drush_print("Langcode: $langcode, Target: $target_id, Mode: $view_mode, Text: $text");
            $bpr_node[$langcode] = Node::create([
              'type' => 'bp_page_reference',
              'langcode' => $langcode,
              'uid' => 1,
              'title' => $text,
              'field_bp_page' => [
                [
                  'target_id' => $target_id,
                  'view_mode' => $view_mode,
                ]
              ]
            ]);
            $bpr_node[$langcode]->save();
            $bpr_nid = $bpr_node[$langcode]->id();
            drush_print('New node ID is ' . $bpr_nid);
            break;
          }
        }
      }
    }


    // Create translations.
    drush_print('Creating translations if any...');
    foreach ($data2 as $view_mode => $data3) {
      drush_print("View mode: $view_mode");
      foreach ($data3 as $text => $data4) {
        drush_print("Text: $text");
        for ($i = 0, $finished = FALSE; $i <= count($data4) && !$finished; $i++) {
          foreach ($data4 as $langcode => $item) {
            drush_print("Langcode: $langcode");
            if ($item->source_langcode != 'und') {
              $source = $item->source_langcode;
              if (!empty($bpr_node[$source]) && empty($bpr_node[$langcode])) {
                drush_print("Langcode: $langcode, Source: $source");
                $bpr_node[$langcode] = $bpr_node[$source]->addTranslation($langcode);
                $bpr_node[$langcode]->uid = 1;
                $bpr_node[$langcode]->title = $text;
                $bpr_node[$langcode]->field_bp_page = [
                  'target_id' => $target_id,
                  'view_mode' => $view_mode,
                ];
                $bpr_node[$langcode]->content_translation_source = \Drupal::languageManager()
                  ->getLanguage($source);
                $bpr_node[$langcode]->save();
              }
              else {
                if (empty($bpr_node[$source])) {
                  drush_print("Source $source not created yet.");
                }
                else {
                  drush_print("$langcode translation already created. Moving on.");
                }
              }
            }
            else {
              drush_print("Not a translation");
            }
          }
          // End the loop if all translations are created.
          foreach($data4 as $langcode => $item) {
            $finished = !empty($bpr_node[$langcode]);
            if (!$finished) {
              drush_print("Missing translation for $langcode, do another pass.");
              break;
            }
          }
        }
        drush_print("Done trying. All translations created? $finished");

        // Add entity references to business problem page.
        foreach($data4 as $langcode => $item) {
          $page_nodes[$nid][$langcode]->field_bp_pages[$item->delta] = [
            'target_id' => $bpr_nid,
          ];
        }
      }
    }
  }
  foreach ($page_nodes[$nid] as $langcode => $page_translation) {
    drush_print("Saving $langcode translation of $nid: {$page_translation->getTitle()}");
    $page_translation->save();
  }
}