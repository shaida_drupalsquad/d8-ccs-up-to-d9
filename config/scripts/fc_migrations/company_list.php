<?php
/**
 * Migrate Company field collection to
 * Company entity references.
 */

// Retrieve all Company List nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'company_list', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

// Build a table of all Company field collection data, in all translations.
$table = [];
/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  drush_print('----------');
  drush_print('Node ' . $node->id() . ': ' . $node->getTitle());
  $languages = $node->getTranslationLanguages();
  /** @var \Drupal\Core\Language\LanguageInterface $lang */
  foreach ($languages as $lang) {
    $langcode = $lang->getId();
    drush_print ('Langcode ' . $langcode);
    $translation = $node->getTranslation($langcode);
    $page_nodes[$node->id()][$langcode] = $translation;
    foreach ($translation->field_company_fc as $delta => $fc) {
      if (!empty($fc->value)) {
        drush_print("Delta $delta: FC ID {$fc->value}, Revision {$fc->revision_id}");
        $fc_entity = \Drupal::entityTypeManager()->getStorage('field_collection_item')->loadRevision($fc->revision_id);
        $target_id = $fc_entity->field_company->target_id;
        $view_mode = $fc_entity->field_company->view_mode;
        $text = $fc_entity->field_bp_page_text->value;
        $field_data = [
          'target_id' => $target_id,
          'view_mode' => $view_mode,
        ];

        drush_print_r($field_data);
        $table[$node->id()][$langcode][$delta] = $field_data;
      }
    }
  }
}

// Iterate through the table, create new company references.
foreach ($table as $nid => $data) {
  drush_print('Migrating data for node ' . $nid);
  drush_print('-------');
  foreach ($data as $langcode => $items) {
    foreach ($items as $delta => $field_data) {
      drush_print("Lang: $langcode, Delta: $delta");
      drush_print_r($field_data);
      $page_nodes[$nid][$langcode]->field_company[$delta] = $field_data;
    }
  }
  foreach (array_keys($page_nodes[$nid]) as $langcode) {
    drush_print("Saving $langcode translation of $nid: {$page_nodes[$nid][$langcode]->getTitle()}");
    $page_nodes[$nid][$langcode]->save();
  }
}
