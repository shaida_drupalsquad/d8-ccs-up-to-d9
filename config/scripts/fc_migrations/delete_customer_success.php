<?php
/**
 * Delete customer success references so we can try migration again.
 */

// Retrieve all Customer Success nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'customer_success', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  drush_print('----------');
  drush_print('Node ' . $node->id() . ': ' . $node->getTitle());
  $languages = $node->getTranslationLanguages();
  /** @var \Drupal\Core\Language\LanguageInterface $lang */
  foreach ($languages as $lang) {
    $langcode = $lang->getId();
    drush_print('Langcode ' . $langcode);
    $translation = $node->getTranslation($langcode);
    $count = $translation->field_customer_success->count();
    for ($i = $count - 1; $i >= 0; $i--) {
      drush_print(" >>> removing item $i");
      $translation->field_customer_success->removeItem($i);
    }
    $translation->save();
  }
}