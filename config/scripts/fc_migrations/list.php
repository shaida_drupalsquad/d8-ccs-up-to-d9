<?php
/**
 * List field collection to
 * List Item node, and update references.
 */

use Drupal\node\Entity\Node;

// Retrieve all Business Problem nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'list', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

// Build a table of all List field collection data, in all translations.
$table = [];
/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  drush_print('----------');
  drush_print('Node ' . $node->id() . ': ' . $node->getTitle());
  $languages = $node->getTranslationLanguages();
  /** @var \Drupal\Core\Language\LanguageInterface $lang */
  foreach ($languages as $lang) {
    $langcode = $lang->getId();
    drush_print ('Langcode ' . $langcode);
    $translation = $node->getTranslation($langcode);
    $page_nodes[$node->id()][$langcode] = $translation;
    foreach ($translation->field_list_fc as $delta => $fc) {
      if (!empty($fc->value)) {
        drush_print("Delta $delta; Revision {$fc->revision_id}");
        $fc_entity = \Drupal::entityTypeManager()->getStorage('field_collection_item')->loadRevision($fc->revision_id);
        $list_uri = $fc_entity->field_list_link->uri;
        $list_title = $fc_entity->field_list_link->title;
        $list_options = $fc_entity->field_list_link->options;
        $text = $fc_entity->field_list_text->value;
        // Text will be the new node title - if empty, skip this.
        if (empty($text)) {
          drush_print(" -- Skipping $langcode delta $delta because text is empty.");
        } else {
          $source_langcode = $translation->content_translation_source->value;
          $data = new stdClass();
          $data->delta = $delta;
          $data->text = $text;
          $data->source_langcode = $source_langcode;
          $data->list_uri = $list_uri;
          $data->list_title = $list_title;
          $data->list_options = $list_options;
          $table[$node->id()][$delta][$langcode] = $data;
          drush_print("---- {$node->id()} {$fc->value} $langcode ----");
          drush_print_r($data);
        }
      }
    }
  }
}

// Delete existing list_item nodes.
$existing_li_nids = \Drupal::entityQuery('node')
  ->condition('type', 'list_item', '=')
  ->execute();
drush_print('There are ' . count($existing_li_nids) . ' existing list_item nodes. Deleting them now...');
$existing_li_nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($existing_li_nids);
\Drupal::entityTypeManager()->getStorage('node')->delete($existing_li_nodes);

// Iterate through the table, create new bp_page_reference nodes, and reference them.
drush_print_r($table);
foreach ($table as $nid => $data) {
  drush_print('Migrating data for node ' . $nid);
  drush_print('-------');
  $new_items = [];
  foreach ($data as $fcid => $data2) {
    $li_node = NULL;
    $li_nid = 0;
    foreach ($data2 as $langcode => $item) {
      drush_print("Lang: $langcode, Text: {$item->text}");
      // Create node from the source translation.
      if ($item->source_langcode == 'und') {
        // Create list_item.
        drush_print('Creating List Item:');
        drush_print("Langcode: $langcode, Text: $text, URI: $item->list_uri");
        $li_node[$langcode] = Node::create([
          'type' => 'list_item',
          'langcode' => $langcode,
          'uid' => 1,
          'title' => $item->text,
          'field_list_link' => [
            [
              'uri' => $item->list_uri,
              'title' => $item->list_title,
              'options' => $item->list_options
            ]
          ]
        ]);
        $li_node[$langcode]->save();
        $li_nid = $li_node[$langcode]->id();
        drush_print('New node ID is ' . $li_nid);
        break;
      }
    }

    // Create translations.
    drush_print('Creating translations if any...');
    for ($i = 0, $finished = FALSE; $i <= count($data2) && !$finished; $i++) {
      foreach ($data2 as $langcode => $item) {
        drush_print("Langcode: $langcode");
        if ($item->source_langcode != 'und') {
          $source = $item->source_langcode;
          if (!empty($li_node[$source]) && empty($li_node[$langcode])) {
            drush_print("Langcode: $langcode, Source: $source");
            $li_node[$langcode] = $li_node[$source]->addTranslation($langcode);
            $li_node[$langcode]->uid = 1;
            $li_node[$langcode]->title = $item->text;
            $li_node[$langcode]->field_list_link = [
              'uri' => $item->list_uri,
              'title' => $item->list_title,
              'options' => $item->list_options
            ];
            $li_node[$langcode]->content_translation_source = \Drupal::languageManager()
              ->getLanguage($source);
            $li_node[$langcode]->save();
          }
          else {
            if (empty($li_node[$source])) {
              drush_print("Source $source not created yet.");
            }
            else {
              drush_print("$langcode translation already created. Moving on.");
            }
          }
        }
        else {
          drush_print("Not a translation");
        }
      }
      // End the loop if all translations are created.
      foreach ($data2 as $langcode => $item) {
        $finished = !empty($li_node[$langcode]);
        if (!$finished) {
          drush_print("Missing translation for $langcode, do another pass.");
          break;
        }
      }
    }
    drush_print("Done trying. All translations created? $finished");

    // Add entity references to list page.
    foreach($data2 as $langcode => $item) {
      $new_items[$langcode][$item->delta] = [
        'target_id' => $li_nid,
      ];
    }
  }

  drush_print("New items for node $nid:");
  drush_print_r($new_items);

  foreach ($page_nodes[$nid] as $langcode => $page_translation) {
    drush_print("Saving $langcode translation of $nid: {$page_translation->getTitle()}");

    // Must add new items in delta order.
    ksort($new_items[$langcode]);
    foreach($new_items[$langcode] as $delta => $data) {
      $page_translation->field_list_items[$delta] = $data;
    }

    $page_translation->save();
  }
}
