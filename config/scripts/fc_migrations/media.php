<?php
/**
 * Migrate Media field collection to
 * Media Contact node, and update references.
 */

use Drupal\node\Entity\Node;

// Retrieve all Business Problem nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'media', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

// Build a table of all Media field collection data, in all translations.
$table = [];
/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  drush_print('----------');
  drush_print('Node ' . $node->id() . ': ' . $node->getTitle());
  $languages = $node->getTranslationLanguages();
  /** @var \Drupal\Core\Language\LanguageInterface $lang */
  foreach ($languages as $lang) {
    $langcode = $lang->getId();
    drush_print ('Langcode ' . $langcode);
    $translation = $node->getTranslation($langcode);
    $page_nodes[$node->id()][$langcode] = $translation;
    foreach ($translation->field_media_fc as $delta => $fc) {
      if (!empty($fc->value)) {
        drush_print("Delta $delta; Revision {$fc->revision_id}");
        $fc_entity = \Drupal::entityTypeManager()->getStorage('field_collection_item')->loadRevision($fc->revision_id);
        $media_name = $fc_entity->field_media_name->value;
        $media_company = $fc_entity->field_media_company->value;
        $media_email = $fc_entity->field_media_email->value;
        $media_phone = $fc_entity->field_media_phone->value;
        $media_title = $fc_entity->field_media_title->value;
        // Media Name will be the new node title - if empty, skip this.
        if (empty($media_name)) {
          drush_print(" -- Skipping $langcode delta $delta because Media Name is empty.");
        } else {
          $source_langcode = $translation->content_translation_source->value;
          $data = new stdClass();
          $data->delta = $delta;
          $data->media_name = $media_name;
          $data->source_langcode = $source_langcode;
          $data->media_company = $media_company;
          $data->media_email = $media_email;
          $data->media_phone = $media_phone;
          $data->media_title = $media_title;
          $table[$node->id()][$delta][$langcode] = $data;
          drush_print("---- {$node->id()} {$fc->value} $langcode ----");
          drush_print_r($data);
        }
      }
    }
  }
}

// Delete existing list_item nodes.
$existing_mcontact_nids = \Drupal::entityQuery('node')
  ->condition('type', 'media_contact', '=')
  ->execute();
drush_print('There are ' . count($existing_mcontact_nids) . ' existing media_contact nodes. Deleting them now...');
$existing_mcontact_nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($existing_mcontact_nids);
\Drupal::entityTypeManager()->getStorage('node')->delete($existing_mcontact_nodes);

// Iterate through the table, create new bp_page_reference nodes, and reference them.
drush_print_r($table);
foreach ($table as $nid => $data) {
  drush_print('Migrating data for node ' . $nid);
  drush_print('-------');
  $new_items = [];
  foreach ($data as $fcid => $data2) {
    $mc_node = NULL;
    $mc_nid = 0;
    foreach ($data2 as $langcode => $item) {
      drush_print("Lang: $langcode, Text: {$item->media_name}");
      // Create node from the source translation.
      if ($item->source_langcode == 'und') {
        // Create list_item.
        drush_print('Creating Media Contact:');
        drush_print("Langcode: $langcode, Text: $media_name");
        $mc_node[$langcode] = Node::create([
          'type' => 'media_contact',
          'langcode' => $langcode,
          'uid' => 1,
          'title' => $item->media_name,
          'field_media_company' => ['value' => $item->media_company],
          'field_media_email' => ['value' => $item->media_email],
          'field_media_phone' => ['value' => $item->media_phone],
          'field_media_title' => ['value' => $item->media_title],
        ]);
        $mc_node[$langcode]->save();
        $mc_nid = $mc_node[$langcode]->id();
        drush_print('New node ID is ' . $mc_nid);
        break;
      }
    }

    // Create translations.
    drush_print('Creating translations if any...');
    for ($i = 0, $finished = FALSE; $i <= count($data2) && !$finished; $i++) {
      foreach ($data2 as $langcode => $item) {
        drush_print("Langcode: $langcode");
        if ($item->source_langcode != 'und') {
          $source = $item->source_langcode;
          if (!empty($mc_node[$source]) && empty($mc_node[$langcode])) {
            drush_print("Langcode: $langcode, Source: $source");
            $mc_node[$langcode] = $mc_node[$source]->addTranslation($langcode);
            $mc_node[$langcode]->uid = 1;
            $mc_node[$langcode]->title = $item->media_name;
            $mc_node[$langcode]->field_media_company = ['value' => $item->media_company];
            $mc_node[$langcode]->field_media_email = ['value' => $item->media_email];
            $mc_node[$langcode]->field_media_phone = ['value' => $item->media_phone];
            $mc_node[$langcode]->field_media_title = ['value' => $item->media_title];
            $mc_node[$langcode]->content_translation_source = \Drupal::languageManager()
              ->getLanguage($source);
            $mc_node[$langcode]->save();
          }
          else {
            if (empty($mc_node[$source])) {
              drush_print("Source $source not created yet.");
            }
            else {
              drush_print("$langcode translation already created. Moving on.");
            }
          }
        }
        else {
          drush_print("Not a translation");
        }
      }
      // End the loop if all translations are created.
      foreach ($data2 as $langcode => $item) {
        $finished = !empty($mc_node[$langcode]);
        if (!$finished) {
          drush_print("Missing translation for $langcode, do another pass.");
          break;
        }
      }
    }
    drush_print("Done trying. All translations created? $finished");

    // Add entity references to list page.
    foreach($data2 as $langcode => $item) {
      $new_items[$langcode][$item->delta] = [
        'target_id' => $mc_nid,
      ];
    }
  }

  drush_print("New items for node $nid:");
  drush_print_r($new_items);

  foreach ($page_nodes[$nid] as $langcode => $page_translation) {
    drush_print("Saving $langcode translation of $nid: {$page_translation->getTitle()}");

    // Must add new items in delta order.
    ksort($new_items[$langcode]);
    foreach($new_items[$langcode] as $delta => $data) {
      $page_translation->field_media_contacts[$delta] = $data;
    }

    $page_translation->save();
  }
}
