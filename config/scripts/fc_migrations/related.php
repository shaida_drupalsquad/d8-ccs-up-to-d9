<?php
/**
 * Migrate Related field collection to
 * Related Item node, and update references.
 */

use Drupal\node\Entity\Node;

// Retrieve all Related nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'related', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

// Build a table of all Related field collection data, in all translations.
$table = [];
/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  drush_print('----------');
  drush_print('Node ' . $node->id() . ': ' . $node->getTitle());
  $languages = $node->getTranslationLanguages();
  /** @var \Drupal\Core\Language\LanguageInterface $lang */
  foreach ($languages as $lang) {
    $langcode = $lang->getId();
    drush_print ('Langcode ' . $langcode);
    $translation = $node->getTranslation($langcode);
    $page_nodes[$node->id()][$langcode] = $translation;
    foreach ($translation->field_related_fc as $delta => $fc) {
      if (!empty($fc->value)) {
        drush_print("Delta $delta; Revision {$fc->revision_id}");
        $fc_entity = \Drupal::entityTypeManager()->getStorage('field_collection_item')->loadRevision($fc->revision_id);
        $target_id = $fc_entity->field_related->target_id;
        $view_mode = $fc_entity->field_related->view_mode;
        $text = $fc_entity->field_related_text->value;
        // Text will be the new node title - if empty, skip this.
        if (empty($text)) {
          drush_print(" -- Skipping $langcode delta $delta because text is empty.");
        } else {
          $source_langcode = $translation->content_translation_source->value;
          $data = new stdClass();
          $data->delta = $delta;
          $data->source_langcode = $source_langcode;
          $table[$node->id()][$target_id][$view_mode][$text][$langcode] = $data;
        }
      }
    }
  }
}

// Delete existing related_item nodes.
$existing_related_item_nids = \Drupal::entityQuery('node')
  ->condition('type', 'related_item', '=')
  ->execute();
drush_print('There are ' . count($existing_related_item_nids) . ' existing related_item nodes. Deleting them now...');
$existing_related_item_nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($existing_related_item_nids);
\Drupal::entityTypeManager()->getStorage('node')->delete($existing_related_item_nodes);

// Iterate through the table, create new related_item nodes, and reference them.
drush_print_r($table);
foreach ($table as $nid => $data) {
  drush_print('Migrating data for node ' . $nid);
  drush_print('-------');
  foreach ($data as $target_id => $data2) {
    drush_print("Target id: $target_id");
    $item_node = NULL;
    $item_nid = 0;
    foreach ($data2 as $view_mode => $data3) {
      drush_print("View mode: $view_mode");
      foreach ($data3 as $text => $data4) {
        drush_print("Text: $text");
        foreach ($data4 as $langcode => $item) {
          drush_print("Lang: $langcode");
          // Create node from the source translation.
          if ($item->source_langcode == 'und') {
            // Create bp_page_reference.
            drush_print('Creating Related Item:');
            drush_print("Langcode: $langcode, Target: $target_id, Mode: $view_mode, Text: $text");
            $item_node[$langcode] = Node::create([
              'type' => 'related_item',
              'langcode' => $langcode,
              'uid' => 1,
              'title' => $text,
              'field_related' => [
                [
                  'target_id' => $target_id,
                  'view_mode' => $view_mode,
                ]
              ]
            ]);
            $item_node[$langcode]->save();
            $item_nid = $item_node[$langcode]->id();
            drush_print('New node ID is ' . $item_nid);
            break;
          }
        }
      }
    }


    // Create translations.
    drush_print('Creating translations if any...');
    foreach ($data2 as $view_mode => $data3) {
      drush_print("View mode: $view_mode");
      foreach ($data3 as $text => $data4) {
        drush_print("Text: $text");
        for ($i = 0, $finished = FALSE; $i <= count($data4) && !$finished; $i++) {
          foreach ($data4 as $langcode => $item) {
            drush_print("Langcode: $langcode");
            if ($item->source_langcode != 'und') {
              $source = $item->source_langcode;
              if (!empty($item_node[$source]) && empty($item_node[$langcode])) {
                drush_print("Langcode: $langcode, Source: $source");
                $item_node[$langcode] = $item_node[$source]->addTranslation($langcode);
                $item_node[$langcode]->uid = 1;
                $item_node[$langcode]->title = $text;
                $item_node[$langcode]->field_related = [
                  'target_id' => $target_id,
                  'view_mode' => $view_mode,
                ];
                $item_node[$langcode]->content_translation_source = \Drupal::languageManager()
                  ->getLanguage($source);
                $item_node[$langcode]->save();
              }
              else {
                if (empty($item_node[$source])) {
                  drush_print("Source $source not created yet.");
                }
                else {
                  drush_print("$langcode translation already created. Moving on.");
                }
              }
            }
            else {
              drush_print("Not a translation");
            }
          }
          // End the loop if all translations are created.
          foreach ($data4 as $langcode => $item) {
            $finished = !empty($item_node[$langcode]);
            if (!$finished) {
              drush_print("Missing translation for $langcode, do another pass.");
              break;
            }
          }
        }
        drush_print("Done trying. All translations created? $finished");

        // Add entity references to related page.
        foreach($data4 as $langcode => $item) {
          $page_nodes[$nid][$langcode]->field_related_items[$item->delta] = [
            'target_id' => $item_nid,
          ];
        }
      }
    }
  }
  foreach ($page_nodes[$nid] as $langcode => $page_translation) {
    drush_print("Saving $langcode translation of $nid: {$page_translation->getTitle()}");
    $page_translation->save();
  }
}