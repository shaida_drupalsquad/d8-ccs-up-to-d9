<?php
/**
 * Drush script to export taxonomy terms to serialized array.
 */

$all_the_terms = [];

$vocab_query = \Drupal::entityQuery('taxonomy_vocabulary');
$vids = $vocab_query->execute();

foreach (array_keys($vids) as $vid) {
  $term_query = \Drupal::entityQuery('taxonomy_term')
    ->condition('vid', $vid);
  $tids = $term_query->execute();
  foreach($tids as $tid) {
    $term = \Drupal\taxonomy\Entity\Term::load($tid);
    $all_the_terms[$vid][] = $term->label();
  }
}

drush_print(json_encode($all_the_terms));