<?php
/**
 * Drush script to import taxonomy terms as serialized.
 */

// Import serialized file.
$serialized = trim(fgets(STDIN));
$all_the_terms = json_decode($serialized);

drush_print('Terms:');
drush_print_r($all_the_terms);

drush_print('Checking vocabularies:');
foreach ($all_the_terms as $vid => $terms) {
  drush_print($vid);
  $term_query = \Drupal::entityQuery('taxonomy_term')
    ->condition('vid', $vid);
  $tids = $term_query->execute();
  if (!empty($tids)) {
    drush_print_r($tids);
    drush_print("I don't think he'll be very keen. You see, we already got one.");
  }
  else {
    foreach ($terms as $term) {
      drush_print("Creating $term");
      $new = \Drupal\taxonomy\Entity\Term::create(['vid' => $vid, 'name' => $term]);
      $new->save();
    }
  }
}