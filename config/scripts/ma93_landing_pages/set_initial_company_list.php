<?php
/**
 * This script is intended to be run from drush.
 * For all Company List content for which field_company_display_type is not set,
 * it sets field_company_display_type to 'text'.
 */

// Load all company_list nodes.
$query = \Drupal::entityQuery('node')
  ->condition('type', 'company_list', '=');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  // Load all translations.
  $translations = $node->getTranslationLanguages();
  foreach ($translations as $language) {
    $node_translation = $node->getTranslation($language->getId());
    if (empty($node_translation->field_company_display_type->value)) {
      drush_print("Updating node {$node_translation->id()} ({$language->getId()}), {$node_translation->title->value}");
      $node_translation->field_company_display_type->value = 'text';
      try {
        $node_translation->save();
      }
      catch (Exception $ex) {
        drush_print("  ===>>> EXCEPTION: {$ex->getMessage()}");
      }
    }
  }
}