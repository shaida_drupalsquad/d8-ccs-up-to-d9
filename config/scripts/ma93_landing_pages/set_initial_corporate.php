<?php
/**
 * This script, intended to be run from drush.
 * For all content for which field_display_type is not set,
 * it sets field_display_type to 'corporate'.
 */

// Load all nodes of all supported types.
$query = \Drupal::entityQuery('node')
  ->condition('type', ['page', 'article', 'video', 'webinar', 'document'], 'IN');
$nids = $query->execute();
$nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

/** @var \Drupal\node\Entity\Node $node */
foreach ($nodes as $node) {
  // Load all translations.
  $translations = $node->getTranslationLanguages();
  foreach ($translations as $language) {
    $node_translation = $node->getTranslation($language->getId());
    if (empty($node_translation->field_display_type->value)) {
      drush_print("Updating node {$node_translation->id()} ({$language->getId()}), {$node_translation->title->value}");
      $node_translation->field_display_type->value = 'corporate';
      try {
        $node_translation->save();
      }
      catch (Exception $ex) {
        drush_print("  ===>>> EXCEPTION: {$ex->getMessage()}");
      }
    }
  }
}