<?php
/**
 * This script is intended to be run from drush.
 * It migrates all images and documents from existing image/file fields
 * to media entity reference fields.
 */
$progress_filename = 'public://.migrate_media_progress';
$contents = file_get_contents($progress_filename);
$progress = [];
if (!empty($contents)) {
  $progress = unserialize($contents);
}

// Provide a configurable skiplist. $skip[nid][langcode] = TRUE
$skip_filename = 'public://.migrate_media_skip.inc';
if (file_exists($skip_filename)) {
  include_once ($skip_filename);
}
else {
  $skip = [];
}

/**
 * Migrations array:
 *  content type => original field => data.
 */
$migrations = [
  'article' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_doc' => [
      'bundle' => 'document',
      'media_field' => 'field_doc_media',
      'name_field' => 'field_doc_name',
      'document_type' => 'Article',
    ],
  ],
  'banner' => [
    'field_teaser_image_alt' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_alt_media',
      'name_field' => 'field_teaser_image_alt_name',
      'image_type' => ['Teaser alternate'],
    ],
    'field_teaser_image_large' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_large_media',
      'name_field' => 'field_teaser_image_lg_name',
      'image_type' => ['Banner', 'Teaser large'],
    ],
    'field_teaser_image_medium' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_medium_media',
      'name_field' => 'field_teaser_image_md_name',
      'image_type' => ['Banner', 'Teaser medium'],
    ],
    'field_teaser_image_small' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_small_media',
      'name_field' => 'field_teaser_image_sm_name',
      'image_type' => ['Banner', 'Teaser small'],
    ],
  ],
  'biography' => [
    'field_bio_doc' => [
      'bundle' => 'document',
      'media_field' => 'field_bio_doc_media',
      'name_field' => 'field_bio_doc_name',
      'document_type' => 'Biography',
    ],
    'field_bio_image' => [
      'bundle' => 'image',
      'media_field' => 'field_bio_image_media',
      'name_field' => 'field_bio_image_name',
      'image_type' => ['Biography'],
    ],
  ],
  'customer' => [
    'field_company_logo' => [
      'bundle' => 'image',
      'media_field' => 'field_company_logo_media',
      'name_field' => NULL,
      'image_type' => ['Company Logo'],
    ],
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Customer', 'Teaser'],
    ],
  ],
  'document' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_teaser_image_thumbnail' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_thumb_media',
      'name_field' => 'field_teaser_image_thumb_name',
      'image_type' => ['Teaser thumbnail'],
    ],
    'field_doc' => [
      'bundle' => 'document',
      'media_field' => 'field_doc_media',
      'name_field' => 'field_doc_name',
      'document_type_field' => 'field_doc_type',
    ],
  ],
  'event' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
  ],
  'link' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_doc' => [
      'bundle' => 'document',
      'media_field' => 'field_doc_media',
      'name_field' => 'field_bio_doc_name',
      'document_type' => 'News',
    ],
  ],
  'office' => [
    'field_of_sheet' => [
      'bundle' => 'document',
      'media_field' => 'field_of_sheet_media',
      'name_field' => 'field_of_sheet_name',
      'document_type' => 'Office Information',
    ],
  ],
  'page' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_teaser_image_alt' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_alt_media',
      'name_field' => 'field_teaser_image_alt_name',
      'image_type' => ['Teaser alternate'],
    ],
    'field_teaser_image_thumbnail' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_thumb_media',
      'name_field' => 'field_teaser_image_thumb_name',
      'image_type' => ['Teaser thumbnail'],
    ],
  ],
  'partner' => [
    'field_company_logo' => [
      'bundle' => 'image',
      'media_field' => 'field_company_logo_media',
      'name_field' => NULL,
      'image_type' => ['Company Logo'],
    ],
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Partner', 'Teaser'],
    ],
  ],
  'press_release' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_earnings_doc' => [
      'bundle' => 'document',
      'media_field' => 'field_earnings_doc_media',
      'name_field' => 'field_earnings_doc_name',
      'document_type' => 'Press Release',
    ],
  ],
  'promotion' => [
    'field_teaser_image_large' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_large_media',
      'name_field' => 'field_teaser_image_lg_name',
      'image_type' => ['Promotion', 'Teaser large'],
    ],
    'field_teaser_image_medium' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_medium_media',
      'name_field' => 'field_teaser_image_md_name',
      'image_type' => ['Promotion', 'Teaser medium'],
    ],
    'field_teaser_image_small' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_small_media',
      'name_field' => 'field_teaser_image_sm_name',
      'image_type' => ['Promotion', 'Teaser small'],
    ],
  ],
  'slide' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_teaser_image_thumbnail' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_thumb_media',
      'name_field' => 'field_teaser_image_thumb_name',
      'image_type' => ['Teaser thumbnail'],
    ],
  ],
  'statistic' => [
    'field_teaser_image_large' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_large_media',
      'name_field' => 'field_teaser_image_lg_name',
      'image_type' => ['Statistic', 'Teaser large'],
    ],
    'field_teaser_image_medium' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_medium_media',
      'name_field' => 'field_teaser_image_md_name',
      'image_type' => ['Statistic', 'Teaser medium'],
    ],
    'field_teaser_image_small' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_small_media',
      'name_field' => 'field_teaser_image_sm_name',
      'image_type' => ['Statistic', 'Teaser small'],
    ],
  ],
  'video' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_video_up_img' => [
      'bundle' => 'image',
      'media_field' => 'field_video_up_img_media',
      'name_field' => 'field_video_upload_image_name',
      'image_type' => ['Video Still'],
    ],
  ],
  'webinar' => [
    'field_teaser_image' => [
      'bundle' => 'image',
      'media_field' => 'field_teaser_image_media',
      'name_field' => 'field_teaser_image_name',
      'image_type' => ['Teaser'],
    ],
    'field_webinar_video_up_img' => [
      'bundle' => 'image',
      'media_field' => 'field_video_up_img_media',
      'name_field' => 'field_webinar_video_up_img_name',
      'image_type' => ['Video Still'],
    ],
  ],
];

foreach ($migrations as $type => $fields) {
  drush_print('');
  drush_print('Migrating fields for ' . $type);
  drush_print('-------------------');

  // Load nodes for this type.
  $query = \Drupal::entityQuery('node')
    ->condition('type', $type, '=');
  $nids = $query->execute();
  $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

  /** @var \Drupal\node\Entity\Node $node */
  foreach ($nodes as $node) {
    /** @var \Drupal\Core\Language\Language $language */
    foreach($node->getTranslationLanguages() as $language) {
      $langcode = $language->getId();
      if ($node->hasTranslation($langcode)
           && empty($progress[$node->id()][$langcode])
           && empty($skip[$node->id()][$langcode])) {
        $trans = $node->getTranslation($langcode);
        drush_print("$type node {$node->id()}: {$trans->title->value} -- {$langcode}");
        $created = $trans->getCreatedTime();
        $changed = $trans->getChangedTime();
        $trans_changed = FALSE;
        foreach ($fields as $old_field => $data) {
          $name_field = $data['name_field'];
          $media_field = $data['media_field'];
          drush_print("  - field: {$old_field}");
          $field_item = $trans->$old_field->first();
          if (!empty($name_field)) {
            $name = $trans->$name_field->value;
          }
          else {
            // If no name field, use the node title.
            $name = $trans->title->value;
            if ($old_field == 'field_company_logo') {
              // For company logos, append the word "Logo."
              $name .= ' Logo';
            }
          }

          if (empty($field_item)) {
            drush_print("    field is empty.");
            continue;
          }

          drush_print("      Creating new media {$data['bundle']} entity: $name");
          $media = \Drupal\media_entity\Entity\Media::create([
            'name' => $name,
            'bundle' => $data['bundle'],
            'uid' => '1',
            'langcode' => $langcode,
            'status' => \Drupal\media_entity\Entity\Media::PUBLISHED,
            'created' => $created,
            'changed' => $changed,
          ]);

          // Copy the file.
          $file_data = $field_item->getValue();
          drush_print('File info:');
          drush_print_r($file_data);

          /** @var \Drupal\file\Entity\File $file_entity */
          $file_entity = \Drupal\file\Entity\File::load($file_data['target_id']);
          if (empty($file_entity)) {
            drush_print("    Error: no file entity with target id {$file_data['target_id']}!");
            drush_print("    Cannot create corresponding media entity.");
            continue;
          }

          $file_contents = file_get_contents($file_entity->getFileUri());
          $yyyy = date('Y', $created);
          $mm = date('m', $created);

          $new_filename = 'manh-' . \Drupal::service('transliteration')->transliterate($name);
          $new_filename = \Drupal::service('pathauto.alias_cleaner')->cleanString($new_filename);
          $new_filename .= '-' . $langcode;
          $mime = $file_entity->getMimeType();
          $extension = '';
          if ($mime == 'image/jpeg') {
            $extension = '.jpg';
          }
          elseif ($mime == 'image/gif') {
            $extension = '.gif';
          }
          elseif ($mime == 'image/png') {
            $extension = '.png';
          }
          elseif ($mime = 'application/pdf') {
            $extension = '.pdf';
          }
          $new_filename .= $extension;

          // Set taxonomy/reference fields.
          $reference_fields = ['field_term_industry', 'field_term_product',
            'field_term_suite', 'field_term_topic', 'field_customer',
            'field_partner'];
          foreach ($reference_fields as $reference_field) {
            if (!empty($trans->$reference_field)) {
              foreach ($trans->$reference_field as $item) {
                if (!empty($item->entity)) {
                  $media->$reference_field->appendItem($item->entity->id());
                }
              }
            }
          }

          if ($data['bundle'] == 'image') {
            $destination = "public://sys/images/$yyyy/$mm";
            file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
            $new_uri = "public://sys/images/$yyyy/$mm/$new_filename";
            drush_print("New file URI is $new_uri");
            /** @var \Drupal\file\Entity\File $new_file */
            $new_file = file_save_data($file_contents, $new_uri);
            if (!$new_file) {
              drush_print("Failed to save $new_uri");
            }
            else {
              $values = [];
              foreach ($data['image_type'] as $value) {
                $values[] = ['value' => $value];
              }
              $media->field_image_type->setValue($values);
              $media->field_image_file->setValue([
                'target_id' => $new_file->id(),
                'alt' => $file_data['alt'],
                'title' => $file_data['title'],
              ]);
            }
          }
          elseif ($data['bundle'] == 'document') {
            $destination = "public://sys/documents/$yyyy/$mm";
            file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
            $new_uri = "public://sys/documents/$yyyy/$mm/$new_filename";
            drush_print("New file URI is $new_uri");
            $new_file = file_save_data($file_contents, $new_uri);
            if (!$new_file) {
              drush_print("Failed to save $new_uri");
            }
            else {
              // Document type will either be hardcoded in the data or will come from a field.
              if (!empty($data['document_type_field'])) {
                $doc_type_field = $data['document_type_field'];
                $document_type = $trans->$doc_type_field->value;
              }
              else {
                $document_type = $data['document_type'];
              }
              $media->field_document_type->setValue($document_type);
              $media->field_document_file->setValue($new_file);
            }

            // Add a redirect from the old file to the node.
            $old_path = str_replace('public://', 'sites/default/files/', $file_entity->getFileUri());

            // Does redirect already exist?
            $redirect_query = \Drupal::entityQuery('redirect')
              ->condition('redirect_source__path', $old_path, '=');
            $rids = $redirect_query->execute();
            if (empty($rids)) {
              drush_print("Creating redirect: $old_path to $node_uri");
              $node_uri = 'entity:node/' . $node->id();
              $redirect = \Drupal\redirect\Entity\Redirect::create([
                'type' => 'redirect',
                'redirect_source' => ['path' => $old_path],
                'redirect_redirect' => ['uri' => $node_uri],
                'status_code' => '301',
                'language' => $langcode,
                'uid' => 1,
              ]);
              $redirect->save();
            }
            else {
              drush_print ("Redirect already exists for $old_path");
            }
          }
          $media->save();

          // Attach to media field.
          $trans->$media_field->setValue($media);
          drush_print("    -- Set $media_field");
          $trans_changed = TRUE;
        }

        if ($trans_changed) {
          drush_print("    -- Saving {$trans->language()->getId()} translation of node {$node->id()}");
          $trans->save();
        }

        // Save progress.
        drush_print("Saving progress: node {$node->id()} langcode $langcode");
        $progress[$node->id()][$langcode] = TRUE;
        $contents = serialize($progress);
        file_put_contents($progress_filename, $contents);
      }
    }
  }
}