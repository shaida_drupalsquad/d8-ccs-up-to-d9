<?php
/**
 * @file
 * Drush script to delete all items from the menus.
 */
use Drupal\Component\Serialization\Yaml;

$menu_file_path = dirname(__FILE__) . '/menu_build.menu_items.yml';
$menu_yaml = Yaml::decode(file_get_contents($menu_file_path));

// Menus from the Yaml file.
$menus = array_keys($menu_yaml); 

foreach ($menus as $menu_name) {
  drush_print('Deleting links from ' . $menu_name);
  _delete_menu_links($menu_name);
}

_delete_menu('how_can_we_help');
_delete_menu('connect');

function _delete_menu_links($menu_name) {
  $ids = \Drupal::entityQuery('menu_link_content')
    ->condition('menu_name', $menu_name)
    ->execute();
  if (!empty($ids)) {
    entity_delete_multiple('menu_link_content', $ids);
    drush_print('Deleted ' . count($ids) . ' menu links.');
  }
  else {
    drush_print('Nothing to delete.');
  }
}

function _delete_menu($menu_name) {
  $ids = \Drupal::entityQuery('menu')
    ->condition('id', $menu_name)
    ->execute();
  if (!empty($ids)) {
    entity_delete_multiple('menu', $ids);
    drush_print('Deleted ' . count($ids) . ' menus.');
  }
  else {
    drush_print('Nothing to delete.');
  }
}
