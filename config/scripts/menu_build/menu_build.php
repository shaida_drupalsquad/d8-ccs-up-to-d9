<?php
/**
 * @file
 * Drush script to build menus and menu items.
 */

use Drupal\Core\Url;
use Drupal\Component\Serialization\Yaml;

$menu_file_path = dirname(__FILE__) . '/menu_build.menu_items.yml';
$menus = Yaml::decode(file_get_contents($menu_file_path));

foreach ($menus as $menu_name => $data) {
  // If menu entity does not already exist, create it.
  $result = \Drupal::entityQuery('menu')->condition('id', $menu_name)->execute();
  if (empty($result)) {
    drush_print('Creating ' . $menu_name . ' (' . $data['title'] . ')');
    entity_create('menu', array(
      'id' => $menu_name,
      'label' => $data['title'],
      'langcode' => $data['langcode'],
    ))->save();
  }
  else {
    drush_print($menu_name . ' (' . $data['title'] . ') already exists');
  }

  // Build menu items.
  if (!empty($data['items'])) {
    _create_menu_items($menu_name, $data['langcode'], $data['items']);
  }
}

/**
 * Create menu items.
 */
function _create_menu_items($menu_name, $langcode, $items, $parent = NULL) {
  $weight = 0;
  foreach ($items as $data) {
    if ($data['path'] == '<dummy>') {
      $uri = 'entity:node/61';
    }
    elseif ($data['path'] == '<front>') {
      $uri = 'internal:/';
    }
    else {
      $uri = $data['path'];
    }
    drush_print(' - creating ' . $data['title'] . ' at ' . $uri);

    $item_info = [
      'menu_name' => $menu_name,
      'title' => $data['title'],
      'link' => ['uri' => $uri],
      'langcode' => $langcode,
      'weight' => $weight++,
    ];
    if (!empty($parent)) {
      $item_info['parent'] = $parent;
    }
    $new_item = entity_create('menu_link_content', $item_info);
    $new_item->save();

    // Build sub-items.
    if (!empty($data['items'])) {
      _create_menu_items($menu_name, $langcode, $data['items'], $new_item->getPluginId());
    }
  }
}