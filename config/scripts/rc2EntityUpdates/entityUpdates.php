<?php
/**
 * Drush script to update entity schemas.
 */
drush_print('Entity updates');

$entity_manager = \Drupal::entityManager();

$types = ['comment', 'shortcut', 'taxonomy_term', 'menu_link_content', 'node', 'user'];
foreach ($types as $type) {
  drush_print('Updating ' . $type);
  $new = $entity_manager->getDefinition($type);
  $old = $entity_manager->getLastInstalledDefinition($type);
  $entity_manager->onEntityTypeUpdate($new, $old);
}
