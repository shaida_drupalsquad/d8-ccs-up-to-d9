<?php
/**
 * Drush script to update entity field schemas.
 */
drush_print('Field updates');

$entity_manager = \Drupal::entityManager();

$types = [
  'contact_message' => [
    'langcode',
  ],
  'menu_link_content' => [
    'uuid', 'bundle', 'menu_name', 'langcode',
  ],
  'user' => [
    'uuid', 'langcode', 'preferred_langcode', 'preferred_admin_langcode', 'roles',
  ],
  'node' => [
    'field_event_type',
  ],
];

foreach ($types as $type => $fields) {
  drush_print('Updating fields for ' . $type);
  $field_storage_definitions = $entity_manager->getLastInstalledFieldStorageDefinitions($type);
  $new_definitions = $entity_manager->getFieldStorageDefinitions($type);
  foreach($fields as $field) {
    drush_print(' -- Updating field ' . $field);
    $entity_manager->onFieldStorageDefinitionUpdate($new_definitions[$field], $field_storage_definitions[$field]);
  }
}
