#!/bin/bash
# Update content type configuration.

# Usage message
function print_usage {
  echo "Usage: update-content-types.sh [new_config_dir]"
}

# Check for deleted configuratoin files
function check_deleted {
  MATCH=$1
  for FILE in $TEMPNEW/$MATCH
  do
    if [ ! -e "$NEW_CONFIG/$(basename $FILE)" ]
    then
      echo Deleting $(basename $FILE) because it is not in new configuration.
      rm $TEMPNEW/$(basename $FILE)
    fi
  done
}

# Check parameters
if [[ -z "$1" ]]; then
  print_usage
  exit
fi

if [ ! -d "$1" ]; then
  echo "Directory $1 does not exist."
  exit
fi
NEW_CONFIG=$1

# Drush command will be different depending on Vagrant or Acquia.
if [ "$USER" = "manh8" ]
then
  DRUSH=/home/manh8/mydrush/drush
  CONFIG=/var/www/html/manh8.$AH_SITE_ENVIRONMENT/config
  echo "We appear to be running on Acquia, so using $DRUSH as drush command."
else
  DRUSH=drush
  CONFIG=$HOME/config
  echo "We appear to be running on local environment, so using $DRUSH as drush command."
fi

# Export current site configuration to a temporary directory
TEMPOLD=$HOME/__config_temp_orig
TEMPNEW=$HOME/__config_temp_new
echo mkdir $TEMPOLD
mkdir $TEMPOLD

echo ---
echo Exporting configuration to $TEMPOLD
$DRUSH config-export --destination=$TEMPOLD

echo Copying exported configuration to $TEMPNEW for changes
cp -r $TEMPOLD $TEMPNEW

#Iterate through content types in the new config.
for FILENAME in $NEW_CONFIG/node.type.*
do
  echo -----
  echo $FILENAME
  if [[ $FILENAME =~ node\.type\.(.+)\.yml ]]
  then
    TYPE=${BASH_REMATCH[1]}
    echo Content type: $TYPE
    if [ ! -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      echo -- new type --
    fi
    
    #Base field overrides
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/core.base_field_override.node.${TYPE}.*.yml"
    fi
    echo Copying base field overrides
    cp $NEW_CONFIG/core.base_field_override.node.${TYPE}.*.yml $TEMPNEW/.

    #Form displays
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/core.entity_form_display.node.${TYPE}.*.yml"
    fi
    echo Copying form displays
    cp $NEW_CONFIG/core.entity_form_display.node.${TYPE}.*.yml $TEMPNEW/.

    #View displays
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/core.entity_view_display.node.${TYPE}.*.yml"
    fi
    echo Copying view displays
    cp $NEW_CONFIG/core.entity_view_display.node.${TYPE}.*.yml $TEMPNEW/.

    #Fields
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/field.field.node.${TYPE}.*.yml"
    fi
    echo Copying fields
    cp $NEW_CONFIG/field.field.node.${TYPE}.*.yml $TEMPNEW/.

    echo Copying node type
    cp $NEW_CONFIG/$(basename $FILENAME) $TEMPNEW/.

    echo Copying node RDF mapping
    cp $NEW_CONFIG/rdf.mapping.node.${TYPE}.yml $TEMPNEW/.

    echo Copying language settings.
    cp $NEW_CONFIG/language.content_settings.node.${TYPE}.yml $TEMPNEW/.

  else
    echo Error with file $FILENAME
  fi
done

#Check whether any content types from the old config are NOT in the new config.
for FILENAME in $TEMPNEW/node.type.*
do
  if [ ! -e $NEW_CONFIG/$(basename $FILENAME) ]
  then
    if [[ $FILENAME =~ node\.type\.(.+)\.yml ]]
    then
      TYPE=${BASH_REMATCH[1]}
      echo All files for content type $TYPE will be deleted because the type is not in the new configuration.
      rm $TEMPNEW/node.type.${TYPE}.yml
      rm $TEMPNEW/core.base_field_override.node.${TYPE}.*.yml
      rm $TEMPNEW/core.entity_form_display.node.${TYPE}.*.yml
      rm $TEMPNEW/core.entity_view_display.node.${TYPE}.*.yml
      rm $TEMPNEW/field.field.node.${TYPE}.*.yml
      rm $TEMPNEW/rdf.mapping.node.${TYPE}.yml
      rm $TEMPNEW/language.content_settings.node.${TYPE}.yml
    fi
  fi
done

#Iterate through field collections in the new config.
for FILENAME in $NEW_CONFIG/field_collection.field_collection.*.yml
do
  echo -----
  echo $FILENAME
  if [[ $FILENAME =~ field_collection\.field_collection\.(.+)\.yml ]]
  then
    TYPE=${BASH_REMATCH[1]}
    echo Field collection: $TYPE
    if [ ! -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      echo -- new field collection --
    fi
    
    #Form displays
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/core.entity_form_display.field_collection_item.${TYPE}.*.yml"
    fi
    echo Copying form displays
    cp $NEW_CONFIG/core.entity_form_display.field_collection_item.${TYPE}.*.yml $TEMPNEW/.

    #View displays
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/core.entity_view_display.field_collection_item.${TYPE}.*.yml"
    fi
    echo Copying view displays
    cp $NEW_CONFIG/core.entity_view_display.field_collection_item.${TYPE}.*.yml $TEMPNEW/.

    #Fields
    if [ -e "$TEMPNEW/$(basename $FILENAME)" ]
    then
      check_deleted "/field.field.field_collection_item.${TYPE}.*.yml"
    fi
    echo Copying fields
    cp $NEW_CONFIG/field.field.field_collection_item.${TYPE}.*.yml $TEMPNEW/.

    echo Copying field collection confiuration
    cp $NEW_CONFIG/$(basename $FILENAME) $TEMPNEW/.

  else
    echo Error with file $FILENAME
  fi
done

#Check whether any field collections from the old config are NOT in the new config.
for FILENAME in $TEMPNEW/field_collection.field_collection.*.yml
do
  if [ ! -e $NEW_CONFIG/$(basename $FILENAME) ]
  then
    if [[ $FILENAME =~ field_collection\.field_collection\.(.+)\.yml ]]
    then
      TYPE=${BASH_REMATCH[1]}
      echo All files for field collection $TYPE will be deleted because the type is not in the new configuration.
      rm $TEMPNEW/field_collection.field_collection.${TYPE}.yml
      rm $TEMPNEW/core.entity_form_display.field_collection_item.${TYPE}.*.yml
      rm $TEMPNEW/core.entity_view_display.field_collection_item.${TYPE}.*.yml
      rm $TEMPNEW/field.field.field_collection_item.${TYPE}.*.yml
    fi
  fi
done

#Check whether any field storage settings from the old config are NOT in the new config.
for FILENAME in $TEMPNEW/field_collection.field_collection.*.yml
do
  if [ ! -e $NEW_CONFIG/$(basename $FILENAME) ]
  then
    echo Deleting $(basename $FILENAME) because it does not exist in the new configuration
    rm $FILENAME
  fi
done

#Copy field storage from the new config.
cp $NEW_CONFIG/field.storage.*.yml $TEMPNEW/.



echo Importing configuration from $TEMPNEW
$DRUSH config-import --source=$TEMPNEW

echo Cleaning up files. Removing $TEMPOLD and $TEMPNEW .
rm -rf $TEMPOLD
rm -rf $TEMPNEW
echo ---
echo Done. 

