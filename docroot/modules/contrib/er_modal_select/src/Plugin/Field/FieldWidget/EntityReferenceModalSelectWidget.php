<?php
/**
 * @file
 * Contains \Drupal\er_modal_select\Field\Plugin\Field\FieldWidget\EntityReferenceModalSelectWidget.
 */

namespace Drupal\er_modal_select\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\er_modal_select\SelectWidget\FilteredEntityQueryWidget;

/**
 * Plugin implementation of the 'er_modal_select' widget.
 *
 * @FieldWidget(
 *   id = "er_modal_select",
 *   label = @Translation("Entity Reference Modal Select"),
 *   description = @Translation("Browse and select referenced entities in a modal dialog."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceModalSelectWidget extends WidgetBase {
  /**
   * ID of the Ajax wrapper element.
   *
   * @var string
   */
  protected $ajaxWrapperId;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->ajaxWrapperId = Html::getUniqueId($this->fieldDefinition->getName() . '-ajax-wrapper');
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $elements = parent::form($items, $form, $form_state, $get_delta);

    // For single-value fields, add the wrapper ID for Ajax.
    if (empty($elements['widget']['add_more'])) {
      $elements['#id'] = $this->ajaxWrapperId;
      $wrapper_id = $this->ajaxWrapperId;
    }
    else {
      $wrapper_id = $elements['widget']['add_more']['#ajax']['wrapper'];
    }

    // Set the ajax wrapper for each element.
    for ($i = 0 ; $i <= $elements['widget']['#max_delta']; $i++) {
      $widget = &$elements['widget'][$i];
      $erms_wrapper = &$this->traverseToValue($widget, 'erms_wrapper');
      $erms_wrapper['ajax_update']['#ajax']['wrapper'] = $wrapper_id;
      if (isset($erms_wrapper['remove'])) {
        $erms_wrapper['remove']['#ajax']['wrapper'] = $wrapper_id;
      }
    }

    $input = $form_state->getUserInput();
    if (!empty($input['er_modal_select_field_id'])) {
      // Modal dialog is open. If it is for this field, it must be rerendered.
      $current_field = str_replace('_', '-', $items->getName());
      if (substr_compare($current_field, $input['er_modal_select_field_id'], 0, strlen($current_field)) == 0) {
        return $this->renderModalAjaxResponse($input['er_modal_select_field_id'], $form, $form_state);
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $referenced_entities = $items->referencedEntities();

    if ($form_state->isRebuilding()) {
      $trigger = $form_state->getTriggeringElement();
      $is_removing = FALSE;
      if (!empty($trigger['#parents'])
          && $trigger['#parents'][0] == $items->getName()
          && $trigger['#parents'][1] === $delta) {
        foreach($trigger['#parents'] as $item) {
          if ($item === 'remove') {
            // The "remove" button was clicked for this element.
            $is_removing = TRUE;
            break;
          }
        }
      }
      if ($is_removing) {
        $target_id = NULL;
      }
      else {
        $input = $form_state->getUserInput();
        $field_values = $input[$this->fieldDefinition->getName()];
        $target_id = $field_values[$delta]['target_id'];
        if (is_array($target_id)) {
          $target_id = $this->traverseToValue($target_id, 'target_id');
        }
      }
      $entity = !empty($target_id) ? entity_load($this->getFieldSetting('target_type'), $target_id) : NULL;
    }
    else {
      $entity = isset($referenced_entities[$delta]) ? $referenced_entities[$delta] : NULL;
      $target_id = empty($entity) ? NULL : $entity->id();
    }
    $field_name = str_replace('_', '-', $items->getName());

    if (!empty($entity)) {
      $selected_entity = $entity->label() . ' (' . $target_id . ')';
      $select_text = t('Change');
    }
    else {
      $selected_entity = '';
      $select_text = t('Select');
    }
    $element += [
      '#type' => 'container',
      'target_id' => [
        '#type' => 'hidden',
        '#attributes' => ['class' => ['erms-target-id']],
        '#value' => $target_id,
      ],
      'target_entity_type' => [
        '#type' => 'hidden',
        '#value' => $this->getFieldSetting('target_type'),
      ],
      'erms_wrapper' => [
        '#type' => 'container', // @todo may not need this container any more.
        '#prefix' => '<div id="erms-wrapper-' . $field_name . '-' . $delta .'">',
        '#suffix' => '</div>',
        '#attributes' => ['class' => ['erms-wrapper']],
        'current' => [
          '#type' => 'markup',
          '#prefix' => '<div class="selected-target-id">',
          '#markup' => $selected_entity,
          '#suffix' => '</div>',
          '#weight' => 0,
        ],
        // Hidden button to trigger field update after entity is selected.
        'ajax_update' => [
          '#type' => 'button',
          '#attributes' => ['style' => 'display: none', 'class' => ['erms-ajax-update']],
          '#id' => 'erms-ajax-update-' . $field_name . '-' . $delta,
          '#name' => 'erms-ajax-update-' . $field_name . '-' . $delta,
          '#limit_validation_errors' => array(array_merge($form['#parents'], array($field_name))),
          '#ajax' => [
            'callback' => 'Drupal\er_modal_select\Plugin\Field\FieldWidget\EntityReferenceModalSelectWidget::selectEntityCallback',
            'event' => 'update_entity',
            // The wrapper ID will be set in the form() method.
          ],
        ],
        'select' => [
          '#type' => 'button',
          '#prefix' => '<span class="erms-link er-modal-select">',
          '#id' => 'er-modal-select-' . $field_name . '-' . $delta,
          '#name' => 'er-modal-select-' . $field_name . '-' . $delta,
          '#value' => $select_text,
          '#suffix' => '</span>',
          '#weight' => 5,
          '#attributes' => [
            'class' => [
              'entity-type-' . $this->getFieldSetting('target_type'),
            ],
          ],
          '#limit_validation_errors' => array(array_merge($form['#parents'], array($field_name))),
          '#ajax' => [
            'callback' => [$this, 'displayModal'],
            'event' => 'click',
            'progress' => array(
              'type' => 'throbber',
              'message' => NULL,
            ),
          ],
        ],
      ],
      '#attached' => ['library' => ['er_modal_select/er_modal_select']],
    ];

    // If an entity is selected, provide a removal link.
    if (!empty($entity)) {
      $element['erms_wrapper']['remove'] = [
        '#type' => 'button',
        '#prefix' => '<span class="erms-link er-remove">',
        '#id' => 'er-remove-' . $field_name . '-' . $delta,
        '#name' => 'er-remove-' . $field_name . '-' . $delta,
        '#value' => t('Clear'),
        '#suffix' => '</span>',
        '#weight' => 10,
        '#limit_validation_errors' => array(array_merge($form['#parents'], array($field_name))),
        '#ajax' => [
          'callback' => 'Drupal\er_modal_select\Plugin\Field\FieldWidget\EntityReferenceModalSelectWidget::removeValue',
          'event' => 'click',
          'progress' => array(
            'type' => 'throbber',
            'message' => NULL,
            // The wrapper ID will be set in the form() method.
          ),
        ],
      ];
    }

    return $element;
  }

  /**
   * Traverse an array to find a matching key.
   *
   * @param array $tree
   * @param string $key
   * @return
   *   reference to value, or null
   */
  protected function &traverseToValue(array &$tree, $key) {
    foreach ($tree as $k => $v) {
      if ($k === $key) {
        return $tree[$k];
      }
      elseif (is_array($v)) {
        $v2 = &$this->traverseToValue($tree[$k], $key);
        if (!empty($v2)) {
          return $v2;
        }
      }
    }
    // Prevent PHP notice: must return a reference to a variable.
    $null = NULL;
    return $null;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      // Our form element mucks with target_id.
      // Get it back to the way EntityReference expects it.
      if (is_array($value) && isset($value['target_id']) && is_array($value['target_id'])) {
        if (isset($value['target_id']['target_id'])) {
          $values[$key]['target_id'] = $value['target_id']['target_id'];
        }
        else {
          unset($values[$key]['target_id']);
        }
      }

      // If empty, target_id must be exactly NULL, not any other empty value.
      if (isset($values[$key]['target_id']) && empty($values[$key]['target_id'])) {
        $values[$key]['target_id'] = NULL;
      }
    }

    return $values;
  }

  /**
   * Ajax callback to display a modal dialog for selecting an entity.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function displayModal(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $reference_field_id = substr($trigger['#id'], strlen('er-modal-select-'));

    return $this->renderModalAjaxResponse($reference_field_id, $form, $form_state);
  }

  /**
   * Returns an Ajax OpenModalDialogCommand for the select dialog.
   *
   * @param $reference_field_id
   *   Field ID for the entity reference.
   * @param array $form
   *   Field form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The OpenModalDialogCommand for the select dialog.
   */
  public function renderModalAjaxResponse($reference_field_id, array &$form, FormStateInterface $form_state) {
    $modal_widget = new FilteredEntityQueryWidget($form_state, $reference_field_id, $this->getFieldSettings());
    $modal_form = \Drupal::formBuilder()->getForm($modal_widget);
    $response = new AjaxResponse();

    // Attach the libraries we need and
    // set the attachments for this Ajax response.
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $response->setAttachments($form['#attached']);

    // Open a modal with the widget element.
    $response->addCommand(new OpenModalDialogCommand(t('Select an entity'), $modal_form, ['width' => '70%']));

    return $response;
  }

  /**
   * AJAX form callback: update_entity event.
   */
  public static function selectEntityCallback(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#parents'];
    $replace_element = $form[$parents[0]];
    return $replace_element;
  }

  /**
   * AJAX form callback: remove value.
   */
  public static function removeValue(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#parents'];
    $replace_element = $form[$parents[0]];
    return $replace_element;
  }
}