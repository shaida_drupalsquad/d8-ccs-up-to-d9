<?php
/**
 * @file
 * Contains \Drupal\er_modal_select\SelectWidget\FilteredEntityQueryWidget.
 */

namespace Drupal\er_modal_select\SelectWidget;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\er_modal_select\Ajax\SelectEntityCommand;

class FilteredEntityQueryWidget extends SelectWidgetBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'er_modal_select_filtered_entity_query';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $bundle_filter = NULL;
    $bundles = $this->getAllowedBundles();
    if (!empty($bundles) && count($bundles) > 1) {
      $bundle_filter = array('0' => t('- Any -')) + $bundles;
    }

    // Hidden fields needed for Ajax.
    $form['er_field_id'] = array(
      '#type' => 'hidden',
      '#value' => 'edit-' . $this->fieldId,
    );
    $form['target_entity_type'] = array(
      '#type' => 'hidden',
      '#value' => $this->getTargetEntityTypeId(),
    );
    // Filter by Type and Title.
    $form['filter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Filter:'),
    );
    if (!empty($bundle_filter)) {
      $form['filter']['bundle'] = array(
        '#type' => 'select',
        '#title' => t('Type'),
        '#options' => $bundle_filter,
        '#default_value' => 0,
        '#ajax' => array(
          'callback' => array($this, 'updateMatches'),
          'wrapper' => 'entity-query-matches',
          'progress' => array(
            'type' => 'throbber',
            'message' => NULL,
          ),
        ),
      );
    }
    $form['filter']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title contains'),
      '#description' => t('Use the Tab key to apply changes to this field.'),
      '#default_value' => '',
      '#ajax' => array(
        'callback' => array($this, 'updateMatches'),
        'wrapper' => 'entity-query-matches',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
    );

    $lang_filter = ['0' => t('- Any -')];
    $languages = \Drupal::languageManager()->getLanguages();
    foreach($languages as $lang) {
      $lang_filter[$lang->getId()] = $lang->getName();
    }
    $form['filter']['langcode'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#options' => $lang_filter,
      '#default_value' => 0,
      '#ajax' => array(
        'callback' => array($this, 'updateMatches'),
        'wrapper' => 'entity-query-matches',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
    );

    $target_type = $this->getTargetEntityTypeId();
    if ($this->isEntityStatusSupported()) {
      $status_filter = ['-1' => t('- Any -')];
      if ($target_type == 'menu') {
        $status_filter[0] = t('Disabled');
        $status_filter[1] = t('Enabled');
      }
      else {
        $status_filter[0] = t('Unpublished');
        $status_filter[1] = t('Published');
      }

      $form['filter']['status'] = array(
        '#type' => 'select',
        '#title' => t('Status'),
        '#options' => $status_filter,
        '#default_value' => -1,
        '#ajax' => array(
          'callback' => array($this, 'updateMatches'),
          'wrapper' => 'entity-query-matches',
          'progress' => array(
            'type' => 'throbber',
            'message' => NULL,
          ),
        ),
      );
    }

    // List of entities.
    $form['entities'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="entity-query-matches">',
      '#suffix' => '</div>',
    );

    $form['entities'] += $this->getMatchingEntitiesElement($form_state);

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Select'),
      '#ajax' => array(
        'callback' => array($this, 'selectItem'),
      ),
    );

    return $form;
  }

  /**
   * Returns array of entities matching the filter.
   *
   * @param string $bundle
   *   Bundle filter.
   * @param string $title_substring
   *   Title filter.
   * @param string $langcode
   *   Language code filter.
   * @param int $status
   *   Status filter.
   *
   * @return array
   *   Array of Entity objects.
   */
  protected function getMatchingEntities($bundle = NULL, $title_substring = NULL, $langcode = NULL, $status = NULL) {
    $entities = array();

    $query = $this->buildMatchQuery($bundle, $title_substring, $langcode, $status);
    $ids = $query->execute();
    if (!empty($ids)) {
      $controller = \Drupal::entityTypeManager()->getStorage($this->getTargetEntityTypeId());
      $entities = $controller->loadMultiple($ids);
    }

    return $entities;
  }

  /**
   * Build the EntityQuery to query matching entities based on filters.
   *
   * @param string $bundle
   *   Bundle filter.
   * @param string $title_substring
   *   Title filter.
   * @param string $langcode
   *   Language code filter.
   * @param int $status
   *   Status filter.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   Entity query, ready to modify and execute.
   */
  protected function buildMatchQuery($bundle = NULL, $title_substring = NULL, $langcode = NULL, $status = NULL) {
    $query = \Drupal::entityQuery($this->getTargetEntityTypeId())
      ->pager(25);
    if ($this->getTargetEntityTypeId() == 'node') {
      if (!empty($bundle)) {
        $query->condition('type', $bundle);
      }
      else {
        $allowed = $this->getAllowedBundles();
        if (!empty($allowed) && count($allowed) > 1) {
          $bundles = array_keys($allowed);
          if (count($bundles) == 1) {
            $bundle = reset($bundles);
            $query->condition('type', $bundle);
          }
          else {
            $or = $query->orConditionGroup();
            foreach($bundles as $bundle) {
              $or->condition('type', $bundle);
            }
            $query->condition($or);
          }
        }
      }
    }
    if (!empty($title_substring)) {
      if ($this->getTargetEntityTypeId() == 'block_content') {
        $query->condition('info', $title_substring, 'CONTAINS');
      }
      elseif ($this->getTargetEntityTypeId() == 'menu') {
        $query->condition('label', $title_substring, 'CONTAINS');
      }
      else {
        $query->condition('title', $title_substring, 'CONTAINS');
      }
    }
    if (!empty($langcode)) {
      $query->condition('langcode', $langcode, '=');
    }
    if (isset($status)) {
      if ($this->getTargetEntityTypeId() == 'node') {
        $query->condition('status', $status, '=');
      }
      elseif ($this->getTargetEntityTypeId() == 'block_content' && $this->entityDefinition->isTranslatable()) {
        $query->condition('status', $status);
      }
    }
    if ($this->getTargetEntityTypeId() == 'block_content') {
      $handler_settings = isset($this->fieldSettings['handler_settings']) ? $this->fieldSettings['handler_settings'] : NULL;
      $target_bundles = isset($handler_settings) ? $handler_settings['target_bundles'] : NULL;
      if (!empty($target_bundles)) {
        $query->condition('type', $target_bundles, 'IN');
      }
    }

    return $query;
  }

  /**
   * Returns a form element with entities matching the current filter selections.
   *
   * @param FormStateInterface $form_state
   *   Current form state
   *
   * @return array
   *   Form element.
   */
  protected function getMatchingEntitiesElement(FormStateInterface $form_state) {
    $bundle = $form_state->getValue('bundle');
    $title = $form_state->getValue('title');
    $langcode = $form_state->getValue('langcode');
    if (empty($langcode)) {
      // Convert the "Any" status to NULL.
      $langcode = NULL;
    }

    if ($this->isEntityStatusSupported()) {
      $status = $form_state->getValue('status');
      if ($status == -1) {
        // Convert the "Any" status to NULL.
        $status = NULL;
      }
    }
    else {
      $status = NULL;
    }

    // Set up the Type (bundle) filter, if appropriate.
    $allowed_bundles = $this->getAllowedBundles();
    if (!empty($allowed_bundles)) {
      if (count($allowed_bundles) == 1) {
        $bundle_keys = array_keys($allowed_bundles);
        $bundle = reset($bundle_keys);
      }
    }
    $matching_entities = $this->getMatchingEntities($bundle, $title, $langcode, $status);
    $header['title'] = array('data' => t('Title'));
    if (!empty($allowed_bundles)) {
      $header['type'] = array('data' => t('Type'));
    }
    $header['lang'] = array('data' => t('Language'));
    if ($this->isEntityStatusSupported()) {
      $header['status'] = array('data' => t('Status'));
    }
    $options = array();
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    foreach ($matching_entities as $entity) {
      $translations = [];
      /** @var  \Drupal\Core\TypedData\TranslatableInterface $entity */
      if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
        $languages = $entity->getTranslationLanguages();
        foreach ($languages as $l) {
          if ($langcode == NULL || $langcode == $l->getId()) {
            $translations[] = $entity->getTranslation($l->getId());
          }
        }
      }
      else {
        $translations[] = $entity;
      }

      /** @var \Drupal\Core\Entity\EntityInterface $item */
      foreach ($translations as $item) {
        $status_value = $item->status->getValue();

        if (is_null($status) || $status == $status_value[0]['value']) {
          $option_key = $item->id() . ':' . $item->language()->getId();
          if (method_exists($item, 'getTitle')) {
            $options[$option_key]['title'] = $item->getTitle();
          }
          else {
            $options[$option_key]['title'] = $item->label();
          }
          if (!empty($allowed_bundles)) {
            if (method_exists($item, 'getType')) {
              $options[$option_key]['type'] = $item->getType();
            }
            else {
              $options[$option_key]['type'] = $item->bundle();
            }
          }
          $options[$option_key]['lang'] = $item->language()->getName();
          if ($this->isEntityStatusSupported()) {
            $target_type = $this->getTargetEntityTypeId();
            if (((!is_null($item->status)) && ($target_type == 'node')) || $target_type == 'block_content') {
              $options[$option_key]['status'] = $status_value[0]['value'] ? t('Published') : t('Unpublished');
            }
          }
        }
      }
    }

    return array(
      'match' => array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $options,
        '#empty' => t('No matching items.'),
        '#multiple' => FALSE,
        '#js_select' => TRUE,
      ),
    );
  }

  /**
   * Returns TRUE if this widget supports a status value for the entity type.
   *
   * @return boolean
   */
  protected function isEntityStatusSupported() {
    $target_type = $this->getTargetEntityTypeId();
    return in_array($target_type, ['node', 'block_content']);
  }

  /**
   * Ajax callback for updating matching entities.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return AjaxResponse
   */
  public function updateMatches(array $form, FormStateInterface &$form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#entity-query-matches', $form['entities']));
    return $response;
  }

  /**
   * Ajax callback for when the select button is pressed.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function selectItem(array $form, FormStateInterface &$form_state) {
    $field_id = $form_state->getValue('er_field_id');
    $match = $form_state->getValue('match');

    //Parse the match value into entity ID and langcode.
    $split = explode(':', $match);
    $entity_id = $split[0];

    $response = new AjaxResponse();
    if (!empty($entity_id)) {
      // Load the selected entity.
      $entity_type = $form_state->getValue('target_entity_type');
      $controller = \Drupal::entityManager()->getStorage($entity_type);
      $entity = $controller->load($entity_id);
      if (!empty($entity)) {
        $response->addCommand(new SelectEntityCommand($field_id, array(
          'entity_id' => $entity_id,
          'label' => $entity->label())));
      }
      else {
        \Drupal::logger('er_modal_select')
          ->warning(t('Failed to load selected entity. Type: @type, ID: @id',
            array('@type' => $entity_type, '@id' => $entity_id)));
      }
    }
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }
}