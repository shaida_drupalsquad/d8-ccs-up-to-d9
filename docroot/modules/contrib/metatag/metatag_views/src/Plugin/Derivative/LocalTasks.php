<?php

namespace Drupal\metatag_views\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines local tasks for Metatag Views configuration.
 */
class LocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs new LocalTasks deriver instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if ($this->moduleHandler->moduleExists('config_translation')) {
      $this->derivatives['metatag_views.metatags.translate_overview'] = $base_plugin_definition;
      $this->derivatives['metatag_views.metatags.translate_overview']['title'] = 'Translate';
      $this->derivatives['metatag_views.metatags.translate_overview']['route_name'] = 'metatag_views.metatags.translate_overview';
      $this->derivatives['metatag_views.metatags.translate_overview']['base_route'] = 'metatag_views.metatags.edit';
    }
    return $this->derivatives;
  }

}
