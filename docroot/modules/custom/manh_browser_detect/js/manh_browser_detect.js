/**
 * @file
 * Detect old browsers and provide a compatibility message.
 */
(function($, Drupal) {
  var options = {
    reject : {
      msie: 9
    },
    browserInfo: {
      // Exclude Opera from supported list.
      opera: {
        allow: {all: false}
      }
    },
    header : Drupal.t('Your browser is out of date and may not display our site properly.'),
    paragraph1 : Drupal.t('The best browsers to view our site can be found below.'),
    paragraph2 : Drupal.t('Click on the icon for your preferred browser to go to the download page for the latest version.'),
    closeMessage : Drupal.t('By closing this window you acknowledge that your experience '+
    'on this website may be degraded'),
    closeLink : Drupal.t('Close This Window'),
    imagePath : '/modules/custom/manh_browser_detect/vendor/jReject/images/'
  };

  // Not using Drupal behaviors because it's not meant for new elements, just the page.
  $(document).ready(function(){
    $.reject(options);
  });
}(jQuery, Drupal));