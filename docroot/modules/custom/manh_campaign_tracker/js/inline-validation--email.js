/**
 * @file
 * Inline validation functionality for email fields.
 */

(function ($, Drupal) {
  // Invalidate most punctuation marks and spaces before the `@`: <>()\[\]\\.,;:
  // Validate IP address or domain with at least a 1 character domain and 2 character TLD.
  Drupal.manhInlineValidation.emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  Drupal.behaviors.inlineEmailValidationBehavior = {
    /**
     * Behavior attachment function for email validation.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      var validateCallback = this.validate;
      $(context).find('input[type="email"]').once('inlineEmailValidation').each(function() {
        $(this).on('blur', validateCallback);
        $(this).on('manh_prepopulate:complete', validateCallback);

        // Listen for a reset event if prepopulation or progressive profiling needs to reset the field value.
        $(this).on('manh_prepopulate:reset', function(eventObject) {
          var element = $(eventObject.target);
          Drupal.manhInlineValidation.reset(element);
        });
      });
    },

    /**
     * Validation callback for email fields.
     *
     * @param eventObject
     */
    validate: function (eventObject) {
      var element = $(eventObject.target);
      var value = element.val();

      if (value === '') {
        // Display error for missing entry in a required field.
        Drupal.manhInlineValidation.setInvalid(element, Drupal.t('Please enter a valid email address.'));
      }
      else if (Drupal.manhInlineValidation.emailPattern.test(value)) {
        // Display a successful validation of the entered value.
        Drupal.manhInlineValidation.setValid(element);
      }
      else {
        // Display error for an invalid entry.
        Drupal.manhInlineValidation.setInvalid(element, Drupal.t('Please enter a valid email address.'));
      }
    }
  };
}(jQuery, Drupal));
