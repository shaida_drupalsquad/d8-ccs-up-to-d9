/**
 * @file
 * Inline validation functionality for phone fields.
 */

(function ($, Drupal) {
  var phoneUtil = Drupal.manhInlineValidation.phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();

  // Cache the phone and country values to avoid unnecessary re-validation.
  var cachedPhoneValue = '';
  var cachedCountryValue = '';

  // Save each element selector in one place for reuse.
  var phoneElementSelector = 'input[name="field_telephone[0][value]"], ' +
    'input[name="field_logistics_telephone[0][value]"]';
  var countryElementSelector = 'select[name="field_country"]';

  Drupal.behaviors.inlinePhoneValidationBehavior = {
    /**
     * Behavior attachment function for phone validation.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      var validateCallback = this.validate;
      var countryChangeCallback = this.countryChange;

      $(context).find(phoneElementSelector)
        .once('inlinePhoneValidation').each(function() {
          $(this).on('blur', validateCallback);
          $(this).on('manh_prepopulate:complete', validateCallback);

        // Listen for a reset event if prepopulation or progressive profiling needs to reset the field value.
        $(this).on('manh_prepopulate:reset', function(eventObject) {
          var element = $(eventObject.target);
          Drupal.manhInlineValidation.reset(element);

          // Also reset the cached values to ensure validation continues as expected.
          cachedPhoneValue = '';
          cachedCountryValue = '';
        });
        });
      $(context).find(countryElementSelector).each(function() {
        $(this).on('change', countryChangeCallback);
      });
    },

    /**
     * Validation callback for phone fields.
     *
     * @param eventObject
     */
    validate: function (eventObject) {

      var phoneElement = $(eventObject.target);
      var phoneValue = phoneElement.val();
      var countryElement = $(countryElementSelector);
      var countryValue = countryElement.val();

      // Break if the values haven't changed to avoid re-validating the same value.
      if (phoneValue === cachedPhoneValue && countryValue === cachedCountryValue) {
        return;
      }
      else {
        // Update cached values for follow-up validations.
        cachedPhoneValue = phoneValue;
        cachedCountryValue = countryValue;
      }

      // Break for empty values first. All non-empty values should pass further
      // through the validation process for checking.
      if (phoneValue === '') {
        // Display error for missing entry in a required field.
        Drupal.manhInlineValidation.setInvalid(phoneElement, Drupal.t('Please enter a valid phone number.'));
        return;
      }

      var regionCode;
      // Check with hasOwnProperty() to avoid traversing prototypes.
      if (!drupalSettings.inline_validation.region_code_lookup.hasOwnProperty(countryValue)) {
        regionCode = false;
      }
      else {
        regionCode = drupalSettings.inline_validation.region_code_lookup[countryValue];
      }

      var isValid;
      var validationMessage;
      try {
        // Handle validation with or without a region code selection.
        // @todo Refactor to better indicate requirement of the country field.
        if (regionCode === false) {
          var number = i18n.phonenumbers.PhoneNumberUtil.extractPossibleNumber(phoneValue);

          isValid = i18n.phonenumbers.PhoneNumberUtil.isViablePhoneNumber(number);
          if (!isValid) {
            validationMessage = Drupal.t('Phone number is not valid. Please enter a valid phone number.');
          }
        }
        else {
          var phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
          var number = phoneUtil.parseAndKeepRawInput(phoneValue, regionCode);

          var isPossible = phoneUtil.isPossibleNumberWithReason(number);
          var PNV = libphonenumber.PhoneNumberUtil.ValidationResult;

          switch (isPossible) {
            case PNV.INVALID_COUNTRY_CODE:
              isValid = false;
              validationMessage = Drupal.t('An invalid country code was provided.');
              break;

            case PNV.TOO_SHORT:
            case PNV.TOO_LONG:
              isValid = false;
              validationMessage = Drupal.t('Phone number is not valid. Please enter a valid phone number.');
              break;

            default:
              var phoneRegion = phoneUtil.getRegionCodeForNumber(number);
              if (phoneRegion != regionCode) {
                isValid = false;
                validationMessage = Drupal.t('Phone number is not valid for the selected country. Please update or confirm and submit as is.');
              }
              else {
                isValid = true;
              }
          }
        }
      }
      catch (error) {
        console.error(error);
        switch (error.message) {
          case 'The string supplied did not seem to be a phone number':
            isValid = false;
            validationMessage = Drupal.t('Phone number is not valid. Please enter a valid phone number.');
            break;

          case 'Invalid country calling code':
            isValid = false;
            validationMessage = Drupal.t('A country selection is required.');
        }
      }

      if (isValid === false) {
        Drupal.manhInlineValidation.setWarning(phoneElement, validationMessage);
      }
      else {
        Drupal.manhInlineValidation.setValid(phoneElement);
      }
    },

    /**
     * Validation callback triggered by country changes.
     * @param eventObject
     */
    countryChange: function(eventObject) {
      var element = $(eventObject.target);
      var value = element.val();

      var phoneElement = $(phoneElementSelector);
      var phoneValue = phoneElement.val();

      // Re-validate the phone number if it's populated.
      if (phoneValue != '' && value != '_none') {
        phoneElement.trigger('blur');
      }
    }
  };
}(jQuery, Drupal));
