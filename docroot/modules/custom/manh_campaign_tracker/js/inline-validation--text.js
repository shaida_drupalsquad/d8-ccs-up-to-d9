/**
 * @file
 * Inline validation functionality for text fields and text areas.
 */

(function ($, Drupal) {
  Drupal.behaviors.inlineTextValidationBehavior = {
    // Need to exclude text fields for phone numbers.
    phoneFieldNames: [
      'field_telephone[0][value]',
      'field_logistics_telephone[0][value]'
    ],

    /**
     * Behavior attachment function for text validation.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      var validateCallback = this.validate;

      // Disambiguate "this" for use within closures.
      var validator = this;

      $(context).find('input[type="text"]').once('inlineTextValidation').each(function () {
        var name = $(this).attr('name');
        var isNotWithinChosen = $(this).closest('.chosen-container').length == 0;

        // Skip phone fields and text inputs within a "chosen" widget.
        if (isNotWithinChosen && validator.phoneFieldNames.indexOf(name) === -1) {
          $(this).on('blur', validateCallback);
          $(this).on('manh_prepopulate:complete', validateCallback);

          // Listen for a reset event if prepopulation or progressive profiling needs to reset the field value.
          $(this).on('manh_prepopulate:reset', function(eventObject) {
            var element = $(eventObject.target);
            Drupal.manhInlineValidation.reset(element);
          });
        }
      });
      $(context).find('textarea').once('inlineTextValidation').each(function () {
        $(this).on('blur', validateCallback);
        $(this).on('manh_prepopulate:complete', validateCallback);

        // Listen for a reset event if prepopulation or progressive profiling needs to reset the field value.
        $(this).on('manh_prepopulate:reset', function(eventObject) {
          var element = $(eventObject.target);
          Drupal.manhInlineValidation.reset(element);
        });
      });
    },

    /**
     * Validation callback for text fields.
     *
     * @param eventObject
     */
    validate: function (eventObject) {
      var element = $(eventObject.target);
      var value = element.val();

      if (value === '') {
        // Display error for missing entry in a required field.
        Drupal.manhInlineValidation.setInvalid(element, Drupal.t('This field is required.'));
      }
      else {
        // Display a successful validation of the entered value.
        Drupal.manhInlineValidation.setValid(element);
      }
    }
  }
}(jQuery, Drupal));

