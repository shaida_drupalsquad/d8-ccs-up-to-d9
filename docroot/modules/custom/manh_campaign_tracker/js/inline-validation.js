/**
 * @file
 * Shared functionality for inline validation scripts.
 */

(function (Drupal, $) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.manhInlineValidation = {
    /**
     * Flag an element as invalid with an appropriate message.
     *
     * @param element
     * @param message
     */
    setInvalid: function (element, message) {
      var wrapper = element.closest('.form-wrapper');

      // Remove existing validation indicators.
      wrapper.removeClass('inline-validation--warning inline-validation--success');

      // Add error indicators.
      wrapper.addClass('inline-validation--error');
      Drupal.manhInlineValidation.displayValidationMessage(element, message);
    },

    /**
     * Flag an element with a validation warning and appropriate message.
     *
     * An element flagged with a validation warning should be highlighted as potentially needing
     * review, but will not block submission in case the input is still deemed valid by the user.
     *
     * @param element
     * @param message
     */
    setWarning: function (element, message) {
      var wrapper = element.closest('.form-wrapper');

      // Remove existing validation indicators.
      wrapper.removeClass('inline-validation--error inline-validation--success');

      // Add validation indicators.
      wrapper.addClass('inline-validation--warning');
      Drupal.manhInlineValidation.displayValidationMessage(element, message);
    },

    /**
     * Flag an element as valid.
     *
     * @param element
     */
    setValid: function (element) {
      var wrapper = element.closest('.form-wrapper');

      // Remove error and warning indicators.
      wrapper.removeClass('inline-validation--warning inline-validation--error');
      Drupal.manhInlineValidation.hideValidationMessage(element)

      // Add success indicator.
      wrapper.addClass('inline-validation--success');
    },

    /**
     * Display a validation message with a specific element.
     *
     * @param element
     * @param message
     */
    displayValidationMessage: function (element, message) {
      // Search for validation messages already being displayed.
      var label = element.siblings('label.inline-validation--message');

      if (label.length > 0) {
        // Update existing validation message labels.
        label.first().html(message);
      }
      else {
        element.after('<label for="' + element.attr('id') + '" class="inline-validation--message">' + message + '</label>');
      }
    },

    /**
     * Hide an element's validation message if one exists.
     *
     * @param element
     */
    hideValidationMessage: function (element) {
      // Search for validation messages already being displayed.
      var label = element.siblings('label.inline-validation--message');

      label.remove();
    },

    /**
     * Reset an element's validation indicators.
     *
     * @param element
     */
    reset: function (element) {
      var wrapper = element.closest('.form-wrapper');

      // Remove existing validation indicators.
      wrapper.removeClass('inline-validation--error inline-validation--warning inline-validation--success');

      Drupal.manhInlineValidation.hideValidationMessage(element);
    }
  };
})(Drupal, jQuery);
