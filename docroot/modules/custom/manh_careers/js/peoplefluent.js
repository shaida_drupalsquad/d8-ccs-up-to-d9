/**
 * @file
 * Facilitates email referrals on Manhattan's Career page (provided by PeopleFluent).
 */

(function($) {
  // Set the career iframe source based on getUrl() function from PeopleFluent.
  $(document).ready(function(){
    var iframeUrl = getUrl();
    $('#jobframe').attr('src', iframeUrl);
  });

  // This function was provided by PeopleFluent.
  function getUrl() {
    /* ***************************************Do not modify these!***************************************** */
    var sOriginalUrl = document.URL;
    sOriginalUrl = sOriginalUrl.replace(/%3A/g, ":");
    sOriginalUrl = sOriginalUrl.replace(/%2F/g, "/");
    sOriginalUrl = sOriginalUrl.replace(/%3F/g, "?");
    sOriginalUrl = sOriginalUrl.replace(/%3D/g, "=");
    sOriginalUrl = sOriginalUrl.replace(/%26/g, "&");
    var aArgs = sOriginalUrl.substring(document.URL.indexOf("?") + 1).split("&");
    var validURLs = ["http://careers.peopleclick.com/careerscp", "https://careers.peopleclick.com/careerscp", "http://careers-v1.peopleclick.com/careerscp", "https://careers-v1.peopleclick.com/careerscp", "http://careers-s1.peopleclick.com/careerscp", "https://careers-s1.peopleclick.com/careerscp", "http://careers.ri1.peopleclick.com/careerscp", "https://careers.ri1.peopleclick.com/careerscp", "http://careers-i1.peopleclick.com/careerscp", "https://careers-i1.peopleclick.com/careerscp", "http://careers-q0.peopleclick.com/careerscp/", "https://careers-q0.peopleclick.com/careerscp/", "http://careers.peopleclick.eu.com/careerscp", "https://careers.peopleclick.eu.com/careerscp"];
    var finalParameters = "";
    var finalURL = null;
    /* ***************************************Do not modify these!*****************************************     */

    /* Please set this to the production external site.  All you should need to change for this is the
     client_clientname, and possibly the site name (external)                                                                                           */

    var fallbackURL = "http://careers.peopleclick.com/careerscp/client_manhassociates/external/search.do";


    /* Task #1:          Loop through the querystring parameters.  If the parameter matches the base URL parameter,
     we place that in its own string variable as it has to go in front.  The rest we simply
     rewrite into a separate string which will be tacked on to the base URL (if found).       */

    for (x = 0; x < aArgs.length; x++) {
      var argName = aArgs[x].substring(0, aArgs[x].indexOf("="));
      var argValue = aArgs[x].substring(aArgs[x].indexOf("=") + 1);

      if (argName.toUpperCase() == "CPURL") {

        /* Here we are going to check to make sure that the passed URL matches one of our matched ones, to
         prevent phishing with the frames page                                                                                                                              */

        for (y = 0; y < validURLs.length; y++) {
          if (argValue.toUpperCase().indexOf(validURLs[y].toUpperCase()) == 0) {
            finalURL = argValue;
          }
        }
      }
      else {
        finalParameters = finalParameters + "&" + encodeURI(argName) + "=" + encodeURI(argValue);
      }
    }


    /* Task #2:          See if we found the proper base URL in the query string.  If not, then someone has hit
     this page manually and we should then go to the base search page                                                       */

    if (finalURL != null) {
      finalURL += finalParameters
    }
    else {
      finalURL = fallbackURL;
    }

    return finalURL;
  }
}(jQuery));