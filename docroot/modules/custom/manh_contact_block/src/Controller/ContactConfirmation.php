<?php

/**
 * @file
 * Contains Drupal\manh_contact_block\Controller\ContactConfirmation.
 */

namespace Drupal\manh_contact_block\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ContactConfirmation.
 *
 * @package Drupal\manh_contact_block\Controller
 */
class ContactConfirmation extends ControllerBase {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ContactConfirmation constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   * @param \Drupal\Component\Datetime\TimeInterface $time
   */
  public function __construct(EntityRepositoryInterface $entityRepository, TimeInterface $time, ConfigFactoryInterface $config_factory) {
    $this->entityRepository = $entityRepository;
    $this->time = $time;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('datetime.time'),
      $container->get('config.factory')
    );
  }

  /**
   * Confirmation page.
   *
   * @return array
   *   Return confirmation page render array.
   */
  public function confirmationPage($contact_message) {
    // Need to load this manually because the parameter upcasting doesn't work
    // for contact_message type entities.
    /** @var \Drupal\contact\Entity\Message $message */
    $message_array = $this->entityTypeManager()
      ->getStorage('contact_message')
      ->loadByProperties(['uuid' => $contact_message]);
    $message = reset($message_array);

    // If a message is not found, show 403 error for anonymous or 404 for
    // logged in users.
    if (empty($message)) {
      $this->accessError();
    }

    $translation = $this->entityRepository->getTranslationFromContext($message);

    // Get settings for confirmation field labels.
    $settings = $this->configFactory->get('manh_contact_block.settings');
    $labels = $settings->get('confirmation_labels');

    // Get fields from the user-submitted contact message.
    $fields = $translation->getFields();

    // Compare created time to current time. Invalidate tags, delete the
    // message if older than 30 minutes (1800 seconds), and 403 or 404 error.
    $current_time = $this->time->getCurrentTime();
    $created_time = $fields['created']->getValue()[0]['value'];
    $max_age = 1800;

    if ($current_time - $created_time >= $max_age) {
      $message->delete();
      $this->accessError();
    }

    // Country field is a select list with translated options.
    // Get the translation.
    $country_key = $fields['field_country']->value;
    $settings = $fields['field_country']->getSettings();
    if (isset($settings['allowed_values'][$country_key])) {
      $country = $settings['allowed_values'][$country_key];
    }
    else {
      $country = $country_key;
    }

    // Build the basic element with common field names.
    $confirmation_element = [
      '#theme' => 'contact_confirmation',
      '#your_request_label' => $labels['your_request'],
      '#your_information_label' => $labels['your_information'],
      '#interested_in_label' => $labels['interest'],
      '#name_label' => $labels['name'],
      '#job_title_label' => $labels['job_title'],
      '#company_label' => $labels['company'],
      '#email_label' => $labels['email'],
      '#telephone_label' => $labels['telephone'],
      '#country_label' => $labels['country'],
      '#message_label' => $labels['message'],
      '#mail' => $fields['field_email']->value,
      '#country' => $country,
      '#interested_in' => $fields['field_interested_in']->value,
      '#job_title' => $fields['field_title']->value,
      '#message' => $fields['message']->value,
      '#first_name' => $fields['field_first_name']->value,
      '#last_name' => $fields['field_last_name']->value,
      '#company' => $fields['field_company_name']->value,
      '#telephone' => $fields['field_telephone']->value,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return $confirmation_element;
  }

  /**
   * Throw either a 403 or 404 error depending on user anonymity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
   */
  public function accessError() {
    $user_is_anonymous = $this->currentUser()->isAnonymous();

    if ($user_is_anonymous) {
      throw new AccessDeniedHttpException();
    }
    else {
      throw new NotFoundHttpException();
    }
  }

}
