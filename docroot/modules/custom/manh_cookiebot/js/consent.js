/**
 * @file
 * Cookiebot check for consent.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';

  // Add a utilities object if one doesn't already exist.
  Drupal.manhUtils = Drupal.manhUtils || {};

  /**
   * Return whether a user has consented to tracking cookies via
   * cookiebot.
   *
   * @type {function}
   * @return {boolean}
   */
  Drupal.manhUtils.manhCookiebot = {
    hasConsent: function() {
      // Get cookiebot consent, if user has not consented, do not paginate form.
      // Responses are returned in string format for the following values:
      // 0: User has not given consent.
      // -1: User is not within a region requiring consent.
      // Array: Types of cookies returned.
      // More details on outline can be found at:
      // https://www.cookiebot.com/en/developer/.

      var cookieConsent = $.cookie('CookieConsent');

      var cookiebotCategory;

      // Make sure the needed settings are available.
      if (typeof drupalSettings.cookiebot_category !== 'undefined') {
        cookiebotCategory = drupalSettings.cookiebot_category;
      }
      else {
        // If they aren't, bounce.
        return;
      }

      // If user didn't agree to cookiebot consent, exit.
      if (cookieConsent === "0") {
        return false;
      }

      // The user is not within a region that requires consent.
      if (cookieConsent === "-1") {
        return true;
      }

      // Decode url.
      cookieConsent = decodeURIComponent(cookieConsent);

      // Separate string to identify categories and values.
      cookieConsent = cookieConsent.split(',');

      // Remove non categories values.
      cookieConsent.pop();
      cookieConsent.shift();

      // Create js object to capture results of formatting below.
      var cookiebotCategories = {};

      cookieConsent.forEach(function(element){
        // Convert string to array. E.g. "category:value" and change it to
        // ["category", "value"]
        var categoryValue = element.split(':');
        cookiebotCategories[categoryValue[0]] = categoryValue[1];
      });

      // If cookiebot's cookieConsent has the consent category then we can track cookies.
      if (cookiebotCategories.hasOwnProperty(cookiebotCategory)) {
        return cookiebotCategories[cookiebotCategory];
      }

      return false;
    }
  }
})(jQuery, Drupal, drupalSettings);
