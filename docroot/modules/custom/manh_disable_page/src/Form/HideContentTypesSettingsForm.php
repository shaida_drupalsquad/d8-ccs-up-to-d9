<?php

/**
 * @file
 * Contains Drupal\hide_content_types\Form\HideContentTypesSettingsForm.
 */

namespace Drupal\hide_content_types\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Returns responses for Hide Content Types module routes.
 */
class HideContentTypesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hide_content_types_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hide_content_types.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hide_content_types.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = [];

    $options = array();
    foreach (\Drupal::entityManager()->getBundleInfo('node') as $bundle => $info) {
      $options[$bundle] = $info['label'];
    }
    $default_values = \Drupal::config('hide_content_types.settings')->get('hide_content_types_enabled');

    $form['hide_content_types_enabled'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Enable Hide Content Types'),
      '#options' => $options,
      '#default_value' => $default_values,
    );

    return parent::buildForm($form, $form_state);
  }

}
