(function ($) {
  "use strict";

  var GatedContent = {
    init: function() {
      // Get the content node.
      var $node = $('.node--type-interactive');
      // When the user has passed the gate there will be a .gate-passed-message.
      var passed = $node.find('.gate-passed-message');

      // If .gate-passed-message exists, hide the sidebar.
      if (passed.length !== 0) {
        $node.find('.l-has-right-sidebar').removeClass('l-has-right-sidebar');
        $node.find('.l-content-body').removeClass('l-content-body');
        $node.find('.l-sidebar').hide();
      }
    }
  }

  GatedContent.init();

})(jQuery);
