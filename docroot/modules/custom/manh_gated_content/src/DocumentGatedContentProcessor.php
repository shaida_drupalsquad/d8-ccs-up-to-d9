<?php
/**
 * @file
 * GatedContentProcessor object for a Document node.
 */

namespace Drupal\manh_gated_content;


use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\file\Entity\File;
use Drupal\media_entity\Entity\Media;

class DocumentGatedContentProcessor extends GatedContentProcessorBase implements GatedContentProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function alterForGated(array &$build, ContentEntityInterface $entity,
                                EntityViewDisplayInterface $display, $view_mode) {

    // Set up post-render cache callbacks.
    // These get called even when the node render array is cached.
    $message_callback = '\Drupal\manh_gated_content\DocumentGatedContentProcessor::messagePostRenderCallback';

    $gate_id = $this->getGateId();
    $gate_hash = $this->getGateHash();

    $doc_download_url = NULL;
    if (!empty($build['field_doc_media']['0']['#plain_text'])) {
      $entity_id = $build['field_doc_media']['0']['#plain_text'];
      $media_entity = Media::load($entity_id);
      // Get media entity in current lang.
      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $available_langs = $media_entity->getTranslationLanguages();
      if (isset($available_langs[$lang])) {
        $media_entity = $media_entity->getTranslation($lang);
      }
      // Build URL.
      if (!empty($media_entity->field_document_file->target_id)) {
        $file = File::load($media_entity->field_document_file->target_id);
        if (!empty($file)) {
          $doc_download_url = $file->url();
        }
      }
    }

    $doc_external_link = '';
    if (!empty($entity->field_doc_external_link->uri)) {
      $doc_external_link = $entity->field_doc_external_link->uri;
    }

    if (!empty($build['field_doc_text']['0']['#context']['value'])) {
      $doc_text = $build['field_doc_text']['0']['#context']['value'];
    }
    else {
      $doc_text = NULL;
    }

    $langcode = $entity->language()->getId();

    $build['message_placeholder'] = [
      '#lazy_builder' => [$message_callback, [
          'langcode' => $langcode,
          'gate_id' => $gate_id,
          'gate_hash' => $gate_hash,
          'doc_url' => $doc_download_url,
          'doc_external_link' => $doc_external_link,
          'doc_text' => $doc_text,
        ]
      ],
      '#create_placeholder' => TRUE,
    ];

    $this->addDocumentLinkPlaceholder($build, $langcode, $gate_id, $gate_hash, $doc_download_url, $doc_external_link, $doc_text);

    // Sidebar placeholder/callback are in the base class.
    $this->addSidebarPlaceholder($build, $langcode, $gate_id, $gate_hash);
  }

  /**
   * Add placeholder for links.
   *
   * @param array $build
   * @param string $langcode
   * @param string $gate_id
   * @param string $gate_hash
   * @param string $doc_download_url
   * @param string $doc_external_link
   * @param string $doc_text
   */
  protected function addDocumentLinkPlaceholder(&$build, $langcode, $gate_id, $gate_hash, $doc_download_url, $doc_external_link, $doc_text) {
    $link_callback = '\Drupal\manh_gated_content\DocumentGatedContentProcessor::documentLinkPostRenderCallback';
    if (!empty($build['field_contact_link']['0']['#url'])) {
      $contact_url = $build['field_contact_link']['0']['#url']->toString();
    }
    else {
      $contact_url = NULL;
    }

    if (!empty($build['field_contact_text']['0']['#context']['value'])) {
      $contact_text = $build['field_contact_text']['0']['#context']['value'];
    }
    else {
      $contact_text = NULL;
    }

    $build['link_placeholder'] = [
      '#lazy_builder' => [$link_callback, [
          'langcode' => $langcode,
          'gate_id' => $gate_id,
          'gate_hash' => $gate_hash,
          'contact_url' => $contact_url,
          'contact_text' => $contact_text,
          'doc_url' => $doc_download_url,
          'doc_external_link' => $doc_external_link,
          'doc_text' => $doc_text,
        ]
      ],
      '#create_placeholder' => TRUE,
    ];
  }

  /**
   * Post-render-cache callback to alter markup for gated content contact link.
   */
  public static function documentLinkPostRenderCallback($langcode, $gate_id, $gate_hash, $contact_url, $contact_text, $doc_download_url, $doc_external_link, $doc_text) {
    if (self::isGatePassed($gate_hash)) {
      if (!empty($doc_external_link)) {
        $url = $doc_external_link;
      }
      else {
        $url = $doc_download_url;
      }

      // Create a document and contact link.
      $replace = [
        'document_link' => [
          '#text' => $doc_text,
          '#theme' => 'document_link',
          '#url' => $url,
          '#locked' => FALSE,
          '#marketo_track' => TRUE,
        ],
      ];
      if (!empty($contact_text) && !empty($contact_url)) {
        $replace['contact_link'] = [
          '#contact_url' => $contact_url,
          '#contact_text' => $contact_text,
          '#theme' => 'contact_link',
        ];
      }
    }
    else {
      // Need some markup, or else the node template logic will fail.
      $replace = [
        '#type' => 'markup',
        '#markup' => '<span class="empty"></span>',
      ];
    }

    return $replace;
  }

  /**
   * Post-render-cache callback to alter markup for a message on the top of the screen.
   */
  public static function messagePostRenderCallback($langcode, $gate_id, $gate_hash, $doc_download_url, $doc_external_link, $doc_text) {
    // Load adwords script from configuration.
    $config = \Drupal::config('manh_gated_content.adwords_conversion');
    $script = '';
    if (!empty($config)) {
      $script = $config->get('adwords_script');
    }

    if (!empty($doc_external_link)) {
      $url = $doc_external_link;
    }
    else {
      $url = $doc_download_url;
    }

    // Replace the placeholder with either a gated form button, or nothing.
    if (parent::isGatePassed($gate_hash)) {
      // Replace with message and download link.
      $replace = [
        '#theme' => 'gate_passed_message',
        '#message' => t('Thank you. Your document is ready.'),
        '#link' => [
          '#text' => $doc_text,
          '#theme' => 'document_link',
          '#url' => $url,
          '#locked' => FALSE,
          '#marketo_track' => TRUE,
        ],
        '#adwords_script' => $script,
      ];
    }
    else {
      // Replace with nothing.
      $replace = [];
    }

    return $replace;
  }

}
