<?php
/**
 * @file
 * Interface definition for GatedContentProcessor objects.
 */

namespace Drupal\manh_gated_content;


use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

interface GatedContentProcessorInterface {

  /**
   * If the entity is gated, return TRUE.
   *
   * @return boolean
   */
  public function isGated();

  /**
   * Alter the passed-in build array to block content and link to a gate form.
   *
   * @param array &$build
   *   A renderable array representing the entity content.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity object.
   *
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the entity components.
   *
   * @param $view_mode
   *   The view mode the entity is rendered in.
   */
  public function alterForGated(array &$build, ContentEntityInterface $entity,
    EntityViewDisplayInterface $display, $view_mode);
}