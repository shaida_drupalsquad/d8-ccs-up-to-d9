<?php
/**
 * @file
 * GatedContentProcessor object for a Interactive node.
 */

namespace Drupal\manh_gated_content;


use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

class InteractiveGatedContentProcessor extends GatedContentProcessorBase implements GatedContentProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function alterForGated(array &$build, ContentEntityInterface $entity,
                                EntityViewDisplayInterface $display, $view_mode) {
    // Set up post-render cache callbacks.
    // These get called even when the node render array is cached.
    $message_callback = '\Drupal\manh_gated_content\InteractiveGatedContentProcessor::messagePostRenderCallback';
    $interactive_callback = '\Drupal\manh_gated_content\InteractiveGatedContentProcessor::interactivePostRenderCallback';

    $gate_id = $this->getGateId();
    $gate_hash = $this->getGateHash();
    $langcode = $entity->language()->getId();

    $build['message_placeholder'] = [
      '#lazy_builder' => [$message_callback, [
          'langcode' => $langcode,
          'gate_id' => $gate_id,
          'gate_hash' => $gate_hash,
        ],
      ],
      '#create_placeholder' => TRUE,
    ];

    // Serialize render arrays: lazy builder only allows primitive types.
    $teaser_image = serialize($build['field_teaser_image_media']);
    if (!empty($build['#node']->field_video_embed_code->value)) {
      $video = serialize($build['field_video_embed_code']);
    }
    else {
      // If no embed code, use uploaded video.
      $video = serialize($build['field_video_up_img_media']);
    }
    $build['interactive_placeholder'] = [
      '#lazy_builder' => [$interactive_callback, [
          $langcode,
          $gate_id,
          $gate_hash,
          $teaser_image,
          $video,
        ],
      ],
      '#create_placeholder' => TRUE,
    ];

    // Sidebar and link placeholders/callbacks are in the base class.
    $this->addSidebarPlaceholder($build, $langcode, $gate_id, $gate_hash);
    $this->addLinkPlaceholder($build, $langcode, $gate_id, $gate_hash);
  }

  /**
   * Post-render-cache callback to alter markup for a message on the top of the screen.
   */
  public static function messagePostRenderCallback($langcode, $gate_id, $gate_hash) {
    // Load adwords script from configuration.
    $config = \Drupal::config('manh_gated_content.adwords_conversion');
    $script = '';
    if (!empty($config)) {
      $script = $config->get('adwords_script');
    }

    // Replace the placeholder with either a gated form button, or nothing.
    if (parent::isGatePassed($gate_hash)) {
      // Replace with message and download link.
      $replace = [
        '#theme' => 'gate_passed_message',
        '#message' => t('Thank you. Your interactive content is now ready to view.'),
        '#adwords_script' => $script,
      ];
    }
    else {
      // Replace with nothing.
      $replace = [];
    }

    return $replace;
  }

  /**
   * Callback to render either a teaser image or video depending on gate status.
   *
   * @param $langcode
   * @param $gate_id
   * @param $gate_hash
   * @param $field_teaser_image
   * @param $video
   *
   * @return array
   *   Render array for either teaser image or video.
   */
  public static function interactivePostRenderCallback($langcode, $gate_id, $gate_hash, $field_teaser_image, $video) {
    if (parent::isGatePassed($gate_hash)) {
      // Render the interactive.
      $replace = unserialize($video);
    }
    else {
      // Render the teaser image.
      $replace = unserialize($field_teaser_image);
    }
    return $replace;
  }
}
