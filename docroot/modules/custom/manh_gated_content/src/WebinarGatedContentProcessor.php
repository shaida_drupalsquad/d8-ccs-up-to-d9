<?php
/**
 * @file
 * GatedContentProcessor object for a Webinar node.
 */

namespace Drupal\manh_gated_content;


use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

class WebinarGatedContentProcessor extends GatedContentProcessorBase implements GatedContentProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function alterForGated(array &$build, ContentEntityInterface $entity,
                                EntityViewDisplayInterface $display, $view_mode) {
    // Set up post-render cache callbacks.
    // These get called even when the node render array is cached.
    $message_callback = '\Drupal\manh_gated_content\WebinarGatedContentProcessor::messagePostRenderCallback';
    $video_callback = '\Drupal\manh_gated_content\WebinarGatedContentProcessor::videoPostRenderCallback';

    $gate_id = $this->getGateId();
    $gate_hash = $this->getGateHash();
    $langcode = $entity->language()->getId();

    $build['message_placeholder'] = [
      '#lazy_builder' => [$message_callback, [
        'langcode' => $langcode,
        'gate_id' => $gate_id,
        'gate_hash' => $gate_hash,
      ],
      ],
      '#create_placeholder' => TRUE,
    ];

    // Serialize render arrays: lazy builder only allows primitive types.
    $teaser_image = serialize($build['field_teaser_image_media']);
    if (!empty($build['#node']->field_webinar_video_embed->value)) {
      $video = serialize($build['field_webinar_video_embed']);
    }
    else {
      // If no embed code, use uploaded video.
      $video = serialize($build['field_webinar_video_up']);
    }
    $build['video_placeholder'] = [
      '#lazy_builder' => [$video_callback, [
          $langcode,
          $gate_id,
          $gate_hash,
          $teaser_image,
          $video,
        ],
      ],
      '#create_placeholder' => TRUE,
    ];

    // Sidebar and link placeholders/callbacks are in the base class.
    $this->addSidebarPlaceholder($build, $langcode, $gate_id, $gate_hash);
    $this->addLinkPlaceholder($build, $langcode, $gate_id, $gate_hash);
  }

  /**
   * Post-render-cache callback to alter markup for a message on the top of the screen.
   */
  public static function messagePostRenderCallback($langcode, $gate_id, $gate_hash) {
    // Load adwords script from configuration.
    $config = \Drupal::config('manh_gated_content.adwords_conversion');
    $script = '';
    if (!empty($config)) {
      $script = $config->get('adwords_script');
    }

    // Replace the placeholder with either a message, or nothing.
    if (parent::isGatePassed($gate_hash)) {
      // Replace with message and download link.
      $replace = [
        '#theme' => 'gate_passed_message',
        '#message' => t('Thank you. Your video is now ready to play.'),
        '#adwords_script' => $script,
      ];
    }
    else {
      // Replace with nothing.
      $replace = [];
    }

    return $replace;
  }
  /**
   * Callback to render either a teaser image or video depending on gate status.
   *
   * @param $langcode
   * @param $gate_id
   * @param $gate_hash
   * @param $field_teaser_image
   * @param $video
   *
   * @return array
   *   Render array for either teaser image or video.
   */
  public static function videoPostRenderCallback($langcode, $gate_id, $gate_hash, $field_teaser_image, $video) {
    if (parent::isGatePassed($gate_hash)) {
      // Render the video.
      $replace = unserialize($video);
    }
    else {
      // Render the teaser image.
      $replace = unserialize($field_teaser_image);
    }
    return $replace;
  }

  /**
   * Callback to render the gate link markup depending on the gate status.
   *
   * @param string $langcode
   * @param string $gate_id
   * @param string $gate_hash
   *
   * @return array
   *   Render array for correct element.
   */
  public static function gateLinkPostRenderCallback($langcode, $gate_id, $gate_hash) {
    if (parent::isGatePassed($gate_hash)) {
      // The gate form link should not be rendered. Replace placeholder with empty string.
      $new = [];
    }
    else {
      // Replace placeholder with a link to the gated form.
      $new = [
        '#text' => t('View'),
        '#theme' => 'document_link',
        '#url' => parent::generateGateFormUrl($gate_id, $langcode),
        '#locked' => TRUE,
        '#marketo_track' => FALSE,
      ];
    }
    return $new;
  }
}