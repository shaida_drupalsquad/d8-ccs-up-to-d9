<?php
/**
 * Contains \Drupal\manh_landing_pages\Plugin\Block\NodeLanguageSelector.
 */
namespace Drupal\manh_landing_pages\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;


/**
 * Provides NodeLanguageSelector block.
 *
 * @Block(
 *   id = "manh_node_language_selector",
 *   admin_label = @Translation("Node Language Selector for Landing Pages"),
 *   category = @Translation("Blocks")
 * )
 */
class NodeLanguageSelector extends BlockBase {
  private $node = NULL;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $render = [
      '#cache' => [
        'contexts' => [
          'url',
          'languages:language_interface',
        ],
      ],
    ];

    // Are we viewing a node page?
    /** @var Node node */
    $this->node = \Drupal::request()->get('node');
    if (empty($this->node)) {
      $render['#type'] = 'markup';
      $render['#markup'] = '<!-- Not a node -->';
    }
    else {
      $render['#cache']['keys']= [
        'node' => [$this->node->id()],
      ];
      $render['#cache']['tags'] = $this->node->getCacheTags();
      $language_names = \Drupal::config('manh_landing_pages.settings')->get('language_names');
      $languages = $this->getLanguageOptions();
      if (!empty($languages) && count($languages) > 1) {
        $render['#theme'] = 'manh_landing_pages_language_select';

        foreach($languages as $langcode => $value) {
          /** @var Node $translation */
          $translation = $this->node->getTranslation($langcode);
          $url = $translation->toUrl();
          if (isset($language_names[$value])) {
            $name = $language_names[$value];
          }
          else {
            $name = $value;
          }
          $render['#languages'][$langcode] = [
            'langcode' => $langcode,
            'name' => $name,
            'url' => $url->toString(),
          ];
        }

        // Set default to current language.
        $current_lang = \Drupal::languageManager()->getCurrentLanguage();
        $render['#default'] = $current_lang->getId();

        // Attach javascript library.
        $render['#attached']['library'] = 'manh_landing_pages/language_select';
      }
      else {
        $render['#type'] = 'markup';
        $render['#markup'] = '<!-- Only one language -->';
      }
    }



    return $render;
  }

  /**
   * Provide a list of languages for this node.
   *
   * @return array
   *   Array keyed by language code with value of language name.
   */
  private function getLanguageOptions() {
    $retval = [];

    // Get translation languages for this node.
    if (!empty($this->node)) {
      $languages = $this->node->getTranslationLanguages();
      foreach (array_keys($languages) as $langcode) {
        // TODO get configured language names in the current interface language.
        $retval[$langcode] = $langcode;
      }
    }

    return $retval;
  }
}