<?php

namespace Drupal\manh_landing_pages\Service;

use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\node\Entity\Node;

/**
 * Helper service to identify subtypes of pages within the site.
 */
class PageHelper {

  /**
   * URL patterns keyed by related page type.
   */
  const PAGE_PATTERNS = [
    "industry" => [
      "/industries/*",
      "/industrie/*",
      "/industrieen/*",
      "/industria/*",
      "/industrias/*",
    ],
    "product" => [
      "/products/*",
      "/produtos/*",
      "/productos/*",
      "/producten/*",
      "/produits/*",
    ],
  ];

  /**
   * The path alias manager service to be used for alias lookups.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface $aliasManager
   */
  protected $aliasManager;

  /**
   * The path matcher service to be used for alias matching.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   */
  protected $pathMatcher;

  /**
   * PageHelper constructor.
   *
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   *   The path alias manager service to be used for alias lookups.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service to be used for matching aliases.
   */
  public function __construct(AliasManagerInterface $alias_manager, PathMatcherInterface $path_matcher) {
    $this->aliasManager = $alias_manager;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * Identify a page subtype from a given node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object to identify.
   *
   * @return string
   *   A string representation of the page subtype. Returns the bundle ID if a
   *   more specific subtype was not found.
   */
  public function getPageType(Node $node) {
    // Load alias by internal path to avoid matching errors with frontpage path.
    $alias = $this->aliasManager->getAliasByPath('/' . $node->toUrl()->getInternalPath());

    // Check each page type for matches against multilingual URL patterns.
    $page_type = $node->bundle();
    foreach ($this::PAGE_PATTERNS as $type => $patterns) {
      // @todo Statically cache joined patterns for use in matching.
      if ($this->pathMatcher->matchPath($alias, join("\n", $this::PAGE_PATTERNS[$type]))) {
        $page_type = $type;
        break;
      }
    }

    return $page_type;
  }

}
