<?php

/**
 * @file
 * Manh Langcode Override drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function manh_langcode_override_drush_command() {
  $items = [];

  $items['manh-publish-translation'] = [
    'description' => dt('Publish all content for a given language.'),
    'aliases' => ['ma-pt'],
    'arguments' => [
      'lang' => 'The langcode (such as en-sg, or es-es) to publish content for.',
    ],
  ];

  return $items;
}

/**
 * Callback to publish translated content.
 *
 * @param string $lang
 *   (Optional) The langcode (such as en-sg, or es-es) to publish content for.
 */
function drush_manh_langcode_override_manh_publish_translation($lang = NULL) {
  drush_print(dt('Publishing translated content...'));

  if (empty($lang)) {
    drush_print(dt('An invalid langcode was chosen, exiting early.'));
    return;
  }
  else {
    // Make sure that the langcode is correct.
    $lang = strtolower($lang);
    /** @var \Drupal\manh_langcode_override\OverrideManager $override */
    $override = \Drupal::service('langcode_override.manager');
    $lang_codes = $override->getLanguageCodeOverrides();

    // Check to see if we're using an overridden lang code (ex: zh-cn).
    if (in_array($lang, $lang_codes)) {
      $lang = array_search($lang, $lang_codes);
    }
    // Otherwise, check to see if we're using a Drupal lang code (ex: zh-hans).
    else {
      if (!isset($lang_codes[$lang])) {
        drush_print(dt('An invalid langcode was chosen, exiting early.'));
        return;
      }
    }
  }

  // Query the taxonomy terms.
  $node_ids = \Drupal::entityQuery('node')
    ->condition('langcode', $lang, '=')
    ->condition('status', 0)
    ->execute();
  $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($node_ids);
  $updated_count = 0;

  foreach ($nodes as $node_id => $node) {
    $translated_node = $node->getTranslation($lang);
    $translated_node->setPublished(TRUE);
    $translated_node->save();
    $updated_count++;
  }

  // If no terms were updated, let the user know.
  if ($updated_count <= 0) {
    drush_print(dt("No nodes needed to be updated."));
  }
  else {
    drush_print(dt("Published $updated_count nodes."));
  }
}
