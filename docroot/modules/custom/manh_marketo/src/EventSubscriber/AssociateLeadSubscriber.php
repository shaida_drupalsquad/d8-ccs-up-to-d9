<?php

namespace Drupal\manh_marketo\EventSubscriber;

use Drupal\manh_marketo\Event\LeadSyncedEvent;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to LeadSyncedEvent::LEAD_SYNCED event.
 *
 * Associate a Marketo Lead using the data submitted to the form.
 */
class AssociateLeadSubscriber implements EventSubscriberInterface {

  /**
   * The Marketo MA API Client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * Creates a new ContactMessageInsert instance.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $marketo_ma_api_client
   *   The Marketo MA API Client.
   */
  public function __construct(MarketoMaApiClientInterface $marketo_ma_api_client) {
    $this->marketoMaApiClient = $marketo_ma_api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LeadSyncedEvent::LEAD_SYNCED][] = ['associateLead'];
    return $events;
  }

  /**
   * Associate activity to a Lead in Marketo.
   *
   * Called whenever the LeadSyncedEvent::LEAD_SYNCED event is dispatched.
   *
   * @param Drupal\manh_marketo\Event\LeadSyncedEvent $event
   *   A LeadSyncedEvent object.
   */
  public function associateLead(LeadSyncedEvent $event) {
    // Make the API request to associate the Lead activity in Marketo.
    $response = $this->marketoMaApiClient->associateLead(
      $event->getLeadId(),
      $event->getLead()->getCookie()
    );

    // Record response data from associating the Lead activity.
    $event->getFormSubmission()->addApiCall(
      'associate_lead_activity',
      $response
    );
  }

}
