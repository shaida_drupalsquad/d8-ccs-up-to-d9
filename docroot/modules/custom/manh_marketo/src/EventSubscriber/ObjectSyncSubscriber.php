<?php

namespace Drupal\manh_marketo\EventSubscriber;

use Drupal\contact\MessageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\manh_marketo\Event\LeadSyncedEvent;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to LeadSyncedEvent::LEAD_SYNCED event.
 *
 * Also create a Marketo custom object using the data submitted to the form.
 */
class ObjectSyncSubscriber implements EventSubscriberInterface {

  /**
   * The target Marketo object.
   *
   * @var string
   *   The object identifier being targeted for synchronization in Marketo.
   */
  protected $targetObject = 'webForms_c';

  /**
   * The Marketo MA Service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The Marketo MA API Client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * The Entity Type Manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the loaded mapping configuration.
   *
   * @var array|null
   */
  protected $mappingConfiguration;

  /**
   * Creates a new ContactMessageInsert instance.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The Marketo MA Service.
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $marketo_ma_api_client
   *   The Marketo MA API Client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(MarketoMaServiceInterface $marketo_ma_service, MarketoMaApiClientInterface $marketo_ma_api_client, EntityTypeManagerInterface $entityTypeManager) {
    $this->marketoMaService = $marketo_ma_service;
    $this->marketoMaApiClient = $marketo_ma_api_client;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('marketo_ma'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LeadSyncedEvent::LEAD_SYNCED][] = ['createCustomObject'];
    return $events;
  }

  /**
   * Create a Marketo custom object.
   *
   * Called whenever the LeadSyncedEvent::LEAD_SYNCED event is dispatched.
   *
   * @param Drupal\manh_marketo\Event\LeadSyncedEvent $event
   *   A LeadSyncedEvent object.
   */
  public function createCustomObject(LeadSyncedEvent $event) {
    $data = $event->getFormSubmission()->getMappedData();
    $data['leadID'] = $event->getLeadId();

    // Make the API call to create the custom object in Marketo.
    $response = $this->marketoMaApiClient->syncCustomObject(
      $this->targetObject,
      'createOnly',
      [$data]
    );

    // Record response data from creating the custom object.
    $event->getFormSubmission()->addApiCall(
      'sync_custom_object',
      $response
    );
  }

}
