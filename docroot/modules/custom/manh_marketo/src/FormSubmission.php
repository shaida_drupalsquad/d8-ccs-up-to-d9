<?php

namespace Drupal\manh_marketo;

use Drupal\contact\Entity\Message;
use Drupal\contact\MessageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\marketo_ma\MarketoFieldDefinition;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;

/**
 * Provides an object to hold Marketo form submission data and functionality.
 *
 * @package Drupal\manh_marketo
 */
class FormSubmission {

  /**
   * The data submitted to the form.
   *
   * @var array
   */
  protected $submissionData;

  /**
   * The data that is already mapped against Marketo enabled fields.
   *
   * @var array
   */
  protected $mappedData;

  /**
   * Marketo values like campaign ID, Lead ID, etc.
   *
   * @var array
   */
  protected $values;

  /**
   * An array of API calls to be executed in response to a form submission.
   *
   * @var array
   */
  protected $apiCalls;

  /**
   * The Marketo MA Service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The Marketo MA API Client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * The Entity Type Manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the loaded mapping configuration.
   *
   * @var array|null
   */
  protected $mappingConfiguration;

  /**
   * Constructs a \Drupal\manh_marketo\FormSubmission object.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The Marketo MA Service.
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $marketo_ma_api_client
   *   The Marketo MA API Client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param array $submission_data
   *   Form submission data.
   * @param array $mapped_data
   *   The data that has already been mapped against Marketo enabled fields.
   * @param array $api_calls
   *   The API calls that will be executed in response to this form submission.
   * @param array $values
   *   Marketo values.
   */
  public function __construct(MarketoMaServiceInterface $marketo_ma_service, MarketoMaApiClientInterface $marketo_ma_api_client, EntityTypeManagerInterface $entityTypeManager, array $submission_data, array $mapped_data = [], array $api_calls = [], array $values = []) {
    $this->marketoMaService = $marketo_ma_service;
    $this->marketoMaApiClient = $marketo_ma_api_client;
    $this->entityTypeManager = $entityTypeManager;
    $this->submissionData = $submission_data;
    $this->mappedData = $mapped_data;
    $this->apiCalls = $api_calls;
    $this->values = $values;
  }

  /**
   * Create submission_data from a Message entity.
   *
   * @param Drupal\contact\Entity\Message $message
   *   Form submission Message entity.
   */
  public static function createFromMessage(Message $message, MarketoMaServiceInterface $marketo_ma_service, MarketoMaApiClientInterface $marketo_ma_api_client, EntityTypeManagerInterface $entity_type_manager) {
    $form_submission = new static($marketo_ma_service, $marketo_ma_api_client, $entity_type_manager, $message->toArray());
    $form_submission->setMappedData($message);

    return $form_submission;
  }

  /**
   * Setter for the mappedData attribute.
   *
   * @param Drupal\contact\Entity\Message $message
   *   Form submission data.
   */
  public function setMappedData(Message $message) {
    $this->mappedData = $this->determineMappedData($message);
  }

  /**
   * Getter for the mappedData attribute.
   *
   * @return array
   *   The mappedData attribute.
   */
  public function getMappedData() {
    return $this->mappedData;
  }

  /**
   * Setter for the submissionData attribute.
   *
   * @param array $submission_data
   *   Form submission data.
   */
  public function setSubmissionData(array $submission_data) {
    $this->submissionData = $submission_data;
  }

  /**
   * Getter for the submissionData attribute.
   *
   * @return array
   *   The submissionData attribute.
   */
  public function getSubmissionData() {
    return $this->submissionData;
  }

  /**
   * Getter for value array keyed at $key.
   *
   * @param string $key
   *   The attribute to get.
   *
   * @return Mixed
   *   The $key attribute.
   */
  public function get($key) {
    return $this->values[$key];
  }

  /**
   * Setter for the value array at $key.
   *
   * @param string $key
   *   The attribute to set.
   * @param Mixed $value
   *   The value to assign to the $key attribute.
   */
  public function set($key, $value) {
    $this->values[$key] = $value;
  }

  /**
   * Adds an API Call to the apiCalls attribute.
   *
   * Keys are 'sync_lead', 'create_custom_object' and 'trigger_campaign'.
   */
  public function addApiCall($key, $response) {
    // If the request to Marketo passes a NULL value, it seems that the response
    // returned is just NULL, so we need to at least add an API call in this
    // scenario and noted that it has failed.
    if ($response === NULL) {
      $this->apiCalls[$key]['status'] = FALSE;
      $this->apiCalls[$key]['code'] = NULL;
      $this->apiCalls[$key]['message'] = NULL;
    }
    // In most scenarios, the request to Marketo should return a Response
    // object, or an object that implements Response. In that case, we want to
    // determine if the request was successful. If it failed, we can obtain the
    // error message and code.
    else {
      $response_success = $response->isSuccess();
      $response_status = isset($response->getResult()[0]['status']) ? $response->getResult()[0]['status'] : '';

      if ($response_success && $response_status !== 'skipped') {
        $response_error_code = NULL;
        $response_error_message = NULL;
      }
      else {
        $response_error_code = $response->getError()['code'];
        $response_error_message = $response->getError()['message'];
      }

      $this->apiCalls[$key]['status'] = ($response_success && $response_status !== 'skipped');
      $this->apiCalls[$key]['code'] = $response_error_code;
      $this->apiCalls[$key]['message'] = $response_error_message;
    }
  }

  /**
   * Set the full list of API calls for this form submission.
   *
   * @param array $api_calls
   *   The array of API calls to be added to this form submission.
   *
   * @return $this
   */
  public function setApiCalls(array $api_calls) {
    $this->apiCalls = $api_calls;

    return $this;
  }

  /**
   * Getter for the apiCalls attribute.
   *
   * @return array
   *   The apiCalls attribute.
   */
  public function getApiCalls() {
    return $this->apiCalls;
  }

  /**
   * Determines if the form submission process and API calls are complete.
   *
   * @return bool
   *   Returns TRUE if the form submission process and API calls are complete.
   *   Otherwise, returns FALSE.
   */
  public function isComplete() {
    foreach ($this->getApiCalls() as $api_call) {
      // Return false if any API call did not succeed.
      if ($api_call['status'] === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Loads the mapping configuration for a specific contact form.
   *
   * @param string $contact_form_id
   *   The contact form id.
   *
   * @return array
   *   Returns the mapping configuration of the contact form.
   */
  protected function loadMappingConfiguration($contact_form_id) {
    if (!isset($this->mappingConfiguration)) {
      /** @var \Drupal\contact\ContactFormInterface $contact_form */
      $contact_form = $this->entityTypeManager->getStorage('contact_form')->load($contact_form_id);
      $this->mappingConfiguration = $contact_form->getThirdPartySetting('marketo_ma_contact', 'mapping', []);
    }
    return $this->mappingConfiguration;
  }

  /**
   * Determines whether some marketo tracking is enabled.
   *
   * @param string $contact_form_id
   *   The contact form id.
   *
   * @return bool
   *   TRUE if marketo tracking is enable.d
   */
  protected function isTrackingEnabled($contact_form_id) {
    $contact_form = $this->entityTypeManager->getStorage('contact_form')->load($contact_form_id);
    return ($contact_form->getThirdPartySetting('marketo_ma_contact', 'enabled', 0) === 1
      && !empty($this->loadMappingConfiguration($contact_form_id)));
  }

  /**
   * Determines data mapping from the contact form to marketo fields.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The contact form message.
   *
   * @return array
   *   The mapping data, keyed by marketo field name.
   */
  protected function determineMappedData(MessageInterface $message) {
    $enabled_fields = $this->marketoMaService->getEnabledFields();

    $mapping = $this->loadMappingConfiguration($message->bundle());
    $data = [];

    foreach ($mapping as $contact_field_name => $marketo_field_id) {
      // Make sure there is a value to set and the field is still enabled.
      if (($field_item = $message->get($contact_field_name)->first()) && isset($enabled_fields[$marketo_field_id])) {
        // Get the field name.
        if ($this->marketoMaService->trackingMethod() === 'api_client') {
          $field_name = (new MarketoFieldDefinition($enabled_fields[$marketo_field_id]))->getFieldName(MarketoMaServiceInterface::TRACKING_METHOD_MUNCHKIN);
        }
        else {
          $field_name = (new MarketoFieldDefinition($enabled_fields[$marketo_field_id]))->getFieldName(MarketoMaServiceInterface::TRACKING_METHOD_MUNCHKIN);
        }
        // Adds the field value to the mapped data.
        $data[$field_name] = $field_item->{$field_item->mainPropertyName()};
      }
    }
    return $data;
  }

}
