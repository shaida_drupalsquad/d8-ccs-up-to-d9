<?php

namespace Drupal\manh_marketo\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deletes old contact messages.
 *
 * @QueueWorker(
 *   id = "prune_contact_messages",
 *   title = @translation("Prune contact messages"),
 *   cron = {"time" = 15}
 * )
 */
class ContactMessagePruner extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The period of time that must pass before an item may be re-executed.
   *
   * @const int
   */
  const BLACKOUT_PERIOD = 300;

  /**
   * The contact message storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $messageStorage;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Max age of contact message before deletion.
   *
   * @var int
   */
  protected static $maxMessageAge;

  /**
   * ContactMessagePruner constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration factor.
   * @param \Drupal\Core\Entity\EntityStorageInterface $messageStorage
   *   The contact message storage handler.
   */
  public function __construct(ConfigFactory $configFactory, EntityStorageInterface $messageStorage) {
    $this->configFactory = $configFactory;
    $this->messageStorage = $messageStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getStorage('contact_message')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    // If this item was creating from an item in the queue that hasn't reached
    // the $max_message age yet, we will have a lastExecuted attribute to track
    // when the item was last processed.
    if (isset($item->lastExecuted)) {
      // Has the blackout period passed to allow this item to re-execute?
      if (time() - $item->lastExecuted < self::BLACKOUT_PERIOD) {
        // Suspend further queue execution since this item has likely already
        // passed all the way through the queue.
        throw new SuspendQueueException('All items have been re-queued and should be processed later.');
      }
    }

    $message_id = $item->id;
    $message_created_date = DrupalDateTime::createFromTimestamp($item->created);
    $today_date = DrupalDateTime::createFromDateTime(new \DateTime());

    $days_past_since_creation = $message_created_date->diff($today_date, TRUE)->days;
    // Max message age in days.
    $max_message_age = $this->getMaxMessageAge();

    // If the number of days since this item has been added to the queue is
    // greater than or equal to $max_message_age, we will delete this Message
    // entity and remove the item from the queue.
    if ($days_past_since_creation >= $max_message_age) {
      /** @var \Drupal\contact\Entity\Message $message */
      $message = $this->messageStorage->load($message_id);
      if ($message != NULL) {
        $message->delete();
      }

      // Returing here will remove this item from the queue.
      return;
    }
    // If the number of days that this item has been in the queue is less than
    // $max_message_age, we will add the item back into this queue (as a new
    // item) and remove the current item we are processing from the queue. This
    // is done, as opposed to using \Exception(), to circumvent copious logging
    // that would result from logging many exceptions on each cron run.
    else {
      // Make a copy of our data item to avoid any leftovers associated with it.
      $requeue_item = clone $item;

      // Update our queue data item to include the current time as the last
      // execution time. This will be used separately to ensure the same item
      // isn't processed repeatedly in the same cron execution.
      $requeue_item->lastExecuted = time();

      // Re-queue the item.
      // @todo Load this queue via DI.
      $queue = \Drupal::queue('prune_contact_messages');
      $queue->createItem($requeue_item);

      // Return to stop execution on this item and remove it from the queue.
      return;
    }
  }

  /**
   * Get self::$maxMessageAge if it exists. Otherwise retrieve the value from
   * config, use that value to set self::$maxMessageAge and return the value.
   *
   * @return int
   *   The allotted days until message will be deleted.
   */
  public function getMaxMessageAge() {
    // Check if $self::maxMessageAge is set and if not set it using the
    // manh_marketo.message_settings.message_max_age config object.
    if (!isset(self::$maxMessageAge)) {
      self::$maxMessageAge = $this->configFactory
        ->get('manh_marketo.message_settings')
        ->get('message_max_age');
    }

    return self::$maxMessageAge;
  }

}
