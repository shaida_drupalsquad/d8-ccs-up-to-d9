Match Redirect
--------------
Allows specific paths to be redirected.


Features
--------------------------------------------------------------------------------
The primary features include:

* Upon 404, if the requested URL matches a pattern, redirect to a destination.

