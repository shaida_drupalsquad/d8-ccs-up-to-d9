<?php
/**
 * @file
 * Contains Drupal\match_redirect\EventSubscriber\PathSubscriber.
 */

namespace Drupal\manh_match_redirect\EventSubscriber;

use Drupal\manh_langcode_override\OverrideManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PathSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('onKernelResponse', 35);
    return $events;
  }

  /**
   * Handle the KernelEvents::RESPONSE event.
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    if ($response->getStatusCode() === 404) {
      // Extract the current path.
      $request = $event->getRequest();
      $path_info = $request->getPathInfo();

      // Strip off the base_path().
      $base_path_length = strlen(base_path());
      $path_info = substr($path_info, $base_path_length);

      // Array keys are patterns to match.
      // Array values are redirect destination URIs.
      // Special characters must be run through urlencode.
      // However, the '/' character should NOT be run through urlencode.
      $pattern_redirects = [
        'bibliotheque/brochures/*' => 'internal:/ressources/publications?title=&doc_type%5B%5D=Product+Information',
        'bibliotheque/etude-cas/*' => 'internal:/ressources/publications?title=&doc_type%5B%5D=Case+Study',
        'bibliotheque/livres-blancs/*' => 'internal:/ressources/publications?title=&doc_type%5B%5D=White+Paper',
        'bibliotheque/portraits-de-leaders/*' => 'internal:/ressources/articles',
        'bibliotheque/presse/*' => 'internal:/ressources/actualites-manhattan',
        urlencode('bibliothèque') . '/' . urlencode('communiqués-de-presse') . '/*' => 'internal:/ressources/communiques-de-presse',
        urlencode('bibliothèque') . '/tendances/*' => 'internal:/ressources/articles',
        'bibliotheque/*' => 'internal:/ressources',
        'evenementen/webinar/*' => 'internal:/evenementen/webinars',
        'evenementen/*' => 'internal:/evenementen',
        'evenements/webinar/*' => 'internal:/evenements/webinars',
        'evenements/*' => 'internal:/evenements',
        'eventos/seminarios-web/*' => 'internal:/eventos/seminarios-web',
        'eventos/*' => 'internal:/eventos',
        'informatiebronnen/thought-leadership/*' => 'internal:/bronnen/artikelen',
        'middelen/casestudy/*' => 'internal:/bronnen/documenten?title=&doc_type%5B%5D=Case+Study',
        'middelen/het-nieuws/*' => 'internal:/bronnen/manhattan-nieuws',
        'middelen/news/*' => 'internal:/bronnen/manhattan-nieuws',
        'middelen/persberichten/*' => 'internal:/bronnen/persberichten',
        'middelen/portraits-leadership/*' => 'internal:/bronnen/artikelen',
        'middelen/product-information/*' => 'internal:/bronnen/documenten?title=&doc_type%5B%5D=Product+Information',
        'middelen/productinformatie/*' => 'internal:/bronnen/documenten?title=&doc_type%5B%5D=Product+Information',
        'middelen/thought-leadership/*' => 'internal:/bronnen/artikelen',
        'middelen/whitepaper/*' => 'internal:/bronnen/documenten?title=&doc_type%5B%5D=White+Paper',
        'middelen/*' => 'internal:/bronnen',
        'recursos/en-noticias/*' => 'internal:/recursos/noticias-de-manhattan',
        'recursos/estudio-caso/*' => 'internal:/recursos/documentos?title=&doc_type%5B%5D=Case+Study',
        'recursos/informacion-producto/*' => 'internal:/recursos/documentos?title=&doc_type%5B%5D=Product+Information',
        'recursos/informe-tecnico/*' => 'internal:/recursos/documentos?title=&doc_type%5B%5D=White+Paper',
        'recursos/retratos-de-liderazgo/*' => 'internal:/recursos/articulos',
        urlencode('イベント') . '/'. urlencode('webセミナー') . '/*' => 'internal:/events/webinars',
        urlencode('イベント') . '/*' => 'internal:/events',
        urlencode('リソース') . '/' . urlencode('プレスリリース') . '/*' => 'internal:/resources/press-releases',
        urlencode('リソース') . '/' . urlencode('ポートレイト・イン・リーダーシップ') . '/*' => 'internal:/resources/articles',
        urlencode('リソース') . '/' . urlencode('ホワイトペーパー') . '/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=White+Paper',
        urlencode('リソース') . '/' . urlencode('事例研究') . '/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=Case+Study',
        urlencode('リソース') . '/' . urlencode('先駆的実践活動') . '/*' => 'internal:/resources/articles',
        urlencode('リソース') . '/' . urlencode('最新ニュース') . '/*' => 'internal:/resources/manhattan-news',
        urlencode('リソース') . '/' . urlencode('製品情報') . '/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=Product+Information',
        urlencode('リソース') . '/*' => 'internal:/resources',
        urlencode('市场活动') . '/' . urlencode('网络研讨会') . '/*' => 'internal:/events/webinars',
        urlencode('市场活动') . '/*' => 'internal:/events',
        urlencode('资源中心') . '/' . urlencode('产品信息') . '/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=Product+Information',
        urlencode('资源中心') . '/' . urlencode('供应链领导者') . '/*' => 'internal:/resources/articles',
        urlencode('资源中心') . '/' . urlencode('先进理念') . '/*' => 'internal:/resources/articles',
        urlencode('资源中心') . '/' . urlencode('成功案例') . '/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=Case+Study',
        urlencode('资源中心') . '/' . urlencode('技术白皮书') . '/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=White+Paper',
        urlencode('资源中心') . '/' . urlencode('新闻发布') . '/*' => 'internal:/resources/press-releases',
        urlencode('资源中心') . '/' . urlencode('近期热点') . '/*' => 'internal:/resources/manhattan-news',
        urlencode('资源中心') . '/*' => 'internal:/resources',
        'articles/*' => 'internal:/resources/articles',
        'events/webinar/*' => 'internal:/events/webinars',
        'events/*' => 'internal:/events',
        'resources/case-study/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=Case+Study',
        'resources/in-the-news/*' => 'internal:/resources/manhattan-news',
        'resources/news/*' => 'internal:/resources/manhattan-news',
        'resources/portraits-leadership/*' => 'internal:/resources/articles',
        'resources/product-information/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=Product+Information',
        'resources/thought-leadership/*' => 'internal:/resources/articles',
        'resources/white-paper/*' => 'internal:/resources/documents?title=&doc_type%5B%5D=White+Paper',
      ];

      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
      foreach ($pattern_redirects as $pattern => $dest) {
        if ($lang != 'en') {
          /** @var \Drupal\manh_langcode_override\OverrideManager $override */
          $override = \Drupal::service('langcode_override.manager');
          $prefix = $override->getOverriddenLanguageCode($lang);
          // Prepend the language to the pattern.
          $pattern = $prefix . '/' . $pattern;
        }
        $match = \Drupal::service('path.matcher')->matchPath(strtolower($path_info), strtolower($pattern));
        if ($match) {
          $status_code = 301;
          $headers = [ ];
          // Language is automatically considered when building the URL.
          $dest_url = \Drupal\Core\Url::fromUri($dest);
          $response = new RedirectResponse($dest_url->toString(), $status_code, $headers);
          $response->isCacheable(TRUE);
          $event->setResponse($response);
          return;
        }
      }
    }
  }

}
