<?php
/**
 * @file
 * Contains \Drupal\manh_menu_formatter\Plugin\Field\FieldFormatter\EntityReferenceMenuFormatter.
 */

namespace Drupal\manh_menu_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\system\Entity\Menu;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Plugin implementation of the Two Level Menu Formatter.
 *
 * @FieldFormatter(
 *   id = "manh_menu_formatter_2_level",
 *   label = @Translation("Rendered menu: 2 levels"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TwoLevelMenuFormatter extends EntityReferenceFormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    /**
     * @var Menu $entity
     */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity instanceof Menu) {
        $menu_tree = \Drupal::menuTree();
        $menu_name = $entity->id();
        /** @var \Drupal\Core\Menu\MenuActiveTrailInterface $active_trail_service */
        $active_trail_service = \Drupal::getContainer()->get('menu.active_trail');
        $active_trail_ids = $active_trail_service->getActiveTrailIds($menu_name);
        $parameters = new MenuTreeParameters();
        $parameters->onlyEnabledLinks()->setActiveTrail($active_trail_ids)->setMaxDepth(1);
        $tree = $menu_tree->load($menu_name, $parameters);
        $manipulators = array(
          array('callable' => 'menu.default_tree_manipulators:checkAccess'),
          array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
        );
        $tree = $menu_tree->transform($tree, $manipulators);
        $level_1 = $menu_tree->build($tree);
        $level_1['#attributes']['class'][] = 'level-1';
        $level_1['#prefix'] = '<div class="secondarymenu-level-wrapper level-1">';
        $level_1['#suffix'] = '</div>';
        $level_2 = [];

        // Find the active element in the top level.
        $active_item = NULL;
        foreach ($tree as $item) {
          if ($item->inActiveTrail && $item->hasChildren) {
            // Build level 2.
            $parameters->setRoot($item->link->getPluginId())->excludeRoot();
            $tree2 = $menu_tree->load($menu_name, $parameters);
            $tree2 = $menu_tree->transform($tree2, $manipulators);
            $level_2 = $menu_tree->build($tree2);
            $level_2['#attributes']['class'][] = 'level-2';
            $level_2['#prefix'] = '<div class="secondarymenu-level-wrapper level-2">';
            $level_2['#suffix'] = '</div>';

            // Break the loop: no need to look further.
            break;
          }
        }

        $elements[$delta] = [
          '#level_1' => $level_1,
          '#level_2' => $level_2,
          '#theme' => 'manh_menu_2_level',
        ];
      }
      else {
        $elements[$delta] = array('#markup' => t('This is not a menu'));
      }
    }

    return $elements;
  }

  /**
   * Overrides getEntitiesToView to enforce that access is TRUE.
   *
   * @see ::getEntitiesToView()
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = [];

    foreach ($items as $delta => $item) {
      // Ignore items where no entity could be loaded in prepareView().
      if (!empty($item->_loaded)) {
        $entity = $item->entity;

        // Set the entity in the correct language for display.
        if ($entity instanceof TranslatableInterface) {
          $entity = \Drupal::entityManager()->getTranslationFromContext($entity, $langcode);
        }

        $access = TRUE;

        if ($access) {
          // Add the referring item, in case the formatter needs it.
          $entity->_referringItem = $items[$delta];
          $entities[$delta] = $entity;
        }
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for menu entities.
    $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    return $target_type == 'menu';
  }
}
