<?php

namespace Drupal\manh_metatag_teaser;

use Drupal\image\Entity\ImageStyle;

/**
 * Manhattan Metatag utility class.
 */
class ManhMetatagUtility {

  /**
   * Get the node's Metatag image.
   *
   * @param object $node
   *   Node object.
   * @param bool $save
   *   Whether to save the entity after running.
   * @param bool $recreate
   *   Used to determine if we have to recreate image.
   *
   * @return mixed
   *   Returns the file object.
   *
   * @throws
   */
  public static function getMetatagImage($node, $save = FALSE, $recreate = FALSE) {
    $silhouette_entity = $node->hasField('field_teaser_image_silhouette') ? $node->field_teaser_image_silhouette->entity : NULL;
    $silhouette_file_entity = (!empty($silhouette_entity) && $silhouette_entity->hasField('field_image_file')) ? $silhouette_entity->field_image_file->entity : NULL;
    $silhouette_uri = !empty($silhouette_file_entity) ? $silhouette_file_entity->uri->value : NULL;
    $silhouette_filename = !empty($silhouette_file_entity) ? $silhouette_file_entity->getFileName() : NULL;

    // Get the social image uri.
    $social_image_entity = $node->hasField('field_social_sharing_image') ? $node->field_social_sharing_image->entity : NULL;
    $social_image_uri = !empty($social_image_entity) && !$recreate ? $social_image_entity->getFileUri() : 'public://sys/images/metatag/' . $node->id() . '-social-share-' . substr($silhouette_filename, 0, -4) . '_0' . substr($silhouette_filename, -4);

    // Check for existing file before proceeding.
    $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $social_image_uri]);

    // If a file already exists, use it.
    $file = (!empty($files)) ? reset($files) : ManhMetatagUtility::generateMetatagImage($node, $silhouette_filename, $silhouette_uri, $social_image_uri);

    if ($save) {
      $node->save();
    }

    return $file;

  }

  /**
   * Generate new Metatag image from silhouette and background image.
   *
   * @param object $node
   *   Node object.
   * @param string $silhouette_filename
   *   Filename of the existing silhouette image.
   * @param string $silhouette_uri
   *   URI of the existing silhouette image.
   * @param string $social_image_uri
   *   Destination URI of newly created silhouette image.
   *
   * @return mixed
   *   Returns the file object or FALSE if it could not be generated.
   */
  public static function generateMetatagImage($node, $silhouette_filename, $silhouette_uri, $social_image_uri) {
    $file = FALSE;
    $image_style = ImageStyle::load('open_graph_media_1200_x_630');
    $image_style_is_saved = $image_style->createDerivative($silhouette_uri, $social_image_uri);

    $img_type = !empty($social_image_uri) && file_exists($social_image_uri) ? exif_imagetype($social_image_uri) : NULL;
    $supported_img_types = [
      1 => 'imagecreatefromgif',
      2 => 'imagecreatefromjpeg',
      3 => 'imagecreatefrompng',
      6 => 'imagecreatefrombmp',
    ];

    if (array_key_exists($img_type, $supported_img_types)) {

      // Generate the silhouette image using the correct function.
      switch ($img_type) {
        case 1:
          $silhouette_image = imagecreatefromgif($social_image_uri);
          break;

        case 2:
          $silhouette_image = imagecreatefromjpeg($social_image_uri);
          break;

        case 3:
          $silhouette_image = imagecreatefrompng($social_image_uri);
          break;

        case 6:
          $silhouette_image = imagecreatefrombmp($social_image_uri);
          break;
      }

      if (!empty($silhouette_image)) {
        // If changes are made to this array, they may need to be applied to
        // both docroot/themes/manh/styles/sass/utilities/_colors.scss and
        // docroot/themes/manh/styles/sass/components/_hero.scss.
        $background_map = [
          'hero_ma_dark_blue' => '#071D49',
          'hero_ma_dark_blue_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_dark_blue_brand.gif',
          'hero_ma_dark_blue_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_dark_blue_push.gif',
          'hero_ma_light_blue' => '#2C5697',
          'hero_ma_light_blue_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_light_blue_brand.gif',
          'hero_ma_light_blue_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_light_blue_push.gif',
          'hero_ma_flame' => '#E03C31',
          'hero_ma_flame_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_flame_brand.gif',
          'hero_ma_flame_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_flame_push.gif',
          'hero_ma_aqua' => '#77C5D5',
          'hero_ma_aqua_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_aqua_brand.gif',
          'hero_ma_aqua_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_aqua_push.gif',
          'hero_ma_lavender' => '#6F7BD4',
          'hero_ma_lavender_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_lavender_brand.gif',
          'hero_ma_lavender_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_lavender_push.gif',
          'hero_white' => '#ffffff',
          'hero_ma_dark_gray' => '#535353',
          'hero_ma_dark_gray_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_dark_gray_brand.gif',
          'hero_ma_dark_gray_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_dark_gray_push.gif',
          'hero_ma_gray' => '#767676',
          'hero_ma_gray_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_gray_brand.gif',
          'hero_ma_gray_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_gray_push.gif',
          'hero_ma_medium_gray' => '#BCBCBC',
          'hero_ma_medium_gray_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_medium_gray_brand.gif',
          'hero_ma_medium_gray_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_medium_gray_push.gif',
          'hero_ma_light_gray' => '#D9D9D6',
          'hero_ma_light_gray_brand_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_light_gray_brand.gif',
          'hero_ma_light_gray_push_pattern' => DRUPAL_ROOT . '/themes/manh/styles/images/ma_light_gray_push.gif',
        ];
        $header_background_value = $node->hasField('field_page_header_background') ? $node->field_page_header_background->getValue() : NULL;
        $header_background = !empty($header_background_value) ? $header_background_value[0]['value'] : NULL;

        // If we have a silhouette image and a background color or image, a
        // new image will be generated.
        if (array_key_exists($header_background, $background_map)) {
          $background = $background_map[$header_background];
          $silhouette_size = getimagesize(($social_image_uri));
          $dest_image = imagecreatetruecolor($silhouette_size[0], $silhouette_size[1]);

          // Ensure transparency is maintained.
          imagealphablending($dest_image, TRUE);
          imagesavealpha($dest_image, TRUE);

          // Solid color background.
          if (substr($background, 0, 1) === '#') {
            $r = hexdec(substr($background, 1, 2));
            $g = hexdec(substr($background, 3, 2));
            $b = hexdec(substr($background, 5));

            $color = imagecolorallocate($dest_image, $r, $g, $b);
            imagefilledrectangle($dest_image, 0, 0, $silhouette_size[0], $silhouette_size[1], $color);
          }
          // Tiled image background.
          else {

            // Generate the background image using the correct function.
            $fill_img_type = !empty($background) ? exif_imagetype($background) : NULL;
            switch ($fill_img_type) {
              case 1:
                $fill_image = imagecreatefromgif($background);
                break;

              case 2:
                $fill_image = imagecreatefromjpeg($background);
                break;

              case 3:
                $fill_image = imagecreatefrompng($background);
                break;

              case 6:
                $fill_image = imagecreatefrombmp($background);
                break;
            }

            // Use the tile image as the background of the new image.
            imagesettile($dest_image, $fill_image);
            imagefilledrectangle($dest_image, 0, 0, $silhouette_size[0], $silhouette_size[1], IMG_COLOR_TILED);
          }

          // Add the silhouette image to the new image.
          imagecopy($dest_image, $silhouette_image, 0, 0, 0, 0, $silhouette_size[0], $silhouette_size[1]);

          // @TODO: Save and set new image as the featured image for Metatag.
          imagejpeg($dest_image, 'temporary://temp-social-share-' . $silhouette_filename);
          $image_contents = file_get_contents('temporary://temp-social-share-' . $silhouette_filename);
          $file = file_save_data($image_contents, $social_image_uri);

          if ($node->hasField('field_social_sharing_image')) {
            $node->field_social_sharing_image->setValue([
              'target_id' => $file->id(),
            ]);
          }
        }
      }
    }

    return $file;
  }

}
