<?php
/**
 * @file
 * Contains \Drupal\manh_nav_drawers\Plugin\Block\TopNavDesktop
 */
namespace Drupal\manh_nav_drawers\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides desktop version of Top Nav block.
 *
 * @Block(
 *   id = "manh_top_nav_desktop",
 *   admin_label = @Translation("Top Navigation Drawer - Desktop"),
 *   category = @Translation("Blocks")
 * )
 */
class TopNavDesktop extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();

    $form['menu_block_id'] = array(
      '#type' => 'select',
      '#title' => t('Menu Block ID'),
      '#options' => $this->getMenuBlockOptions(),
      '#default_value' => isset($config['menu_block_id']) ? $config['menu_block_id'] : '',
    );
    $form['render_menu'] = array(
      '#type' => 'select',
      '#title' => t('Render Menu (ignore block)'),
      '#options' => $this->getRenderMenuOptions(),
      '#description' => t('If specified, Menu Block ID is ignored, and the selected menu is rendered instead.'),
      '#default_value' => isset($config['render_menu']) ? $config['render_menu'] : '',
    );

    return $form;
  }

  /**
   * Get options for the menu block selector.
   * @return array Available menu blocks to select.
   */
  private function getMenuBlockOptions() {
    $options[''] = t('- None -');

    // Query all custom_block entities of type top_navigation_menu.
    $query = \Drupal::entityQuery('block_content')
      ->condition('type', 'top_navigation_menu');
    $ids = $query->execute();
    $entities = entity_load_multiple('block_content', $ids);
    /** @var \Drupal\block_content\Entity\BlockContent $block */
    foreach ($entities as $block) {
      $options[$block->id()] = $block->label();
    }

    return $options;
  }

  /**
   * Get options for the menu selector.
   * @return array Available menus to select.
   */
  private function getRenderMenuOptions() {
    $options[''] = t('- None -');

    // Query all menu entities.
    $query = \Drupal::entityQuery('menu');
    $ids = $query->execute();
    $entities = entity_load_multiple('menu', $ids);
    /** @var \Drupal\system\Entity\Menu $menu */
    foreach ($entities as $menu) {
      $options[$menu->id()] = $menu->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('menu_block_id', $form_state->getValue('menu_block_id'));
    $this->setConfigurationValue('render_menu', $form_state->getValue('render_menu'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::configFactory()->get('block.block.topnavigationdrawerdesktop');
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    return manh_nav_drawers_get_desktop_content($config, $lang);
  }
}