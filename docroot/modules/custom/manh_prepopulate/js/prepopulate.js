/**
 * @file
 * Prepopulation and progressive profiling code.
 */
(function ($, Drupal) {
  'use strict';

  var marketoLookupIds = {
    field_email: 'edit-field-email-0-value',
    field_first_name: 'edit-field-first-name-0-value',
    field_last_name: 'edit-field-last-name-0-value',
    field_company_name: 'edit-field-company-name-0-value',
    field_country: 'edit-field-country',
    field_title: 'edit-field-title-0-value',
    field_telephone: 'edit-field-telephone-0-value',
    field_have_project: 'edit-field-have-project',
    field_project_timeline: 'edit-field-project-timeline',
    field_have_budget: 'edit-field-have-budget',
    field_heard_how: 'edit-field-heard-how',
    field_opt_in_gdpr_cookie: 'edit-field-opt-in-gdpr-cookie-value'
  };

  // This nested array contains all of the fields in the Gated Content form
  // nested by section. For example, progressiveProfFieldSetsPerPage[0] is an
  // array containing the first set of fields that will be displayed by the
  // Gated Content form. Once a user fills out those fields and submits the
  // form, when they return to a page that displays the Gated Content form, the
  // fields in progressiveProfFieldSetsPerPage[1] array will display and so on.
  var progressiveProfFieldSetsPerPage = [
    [
      {name: 'field_first_name', id: 'edit-field-first-name-0-value'},
      {name: 'field_last_name', id: 'edit-field-last-name-0-value'},
      {name: 'field_company_name', id: 'edit-field-company-name-0-value'},
      {name: 'field_country', id: 'edit-field-country'}
    ],
    [
      {name: 'field_title', id: 'edit-field-title-0-value'},
      {name: 'field_telephone', id: 'edit-field-telephone-0-value'}
    ],
    [
      {name: 'field_have_project', id: 'edit-field-have-project'},
      {name: 'field_project_timeline', id: 'edit-field-project-timeline'},
      {name: 'field_have_budget', id: 'edit-field-have-budget'}
    ]
  ];

  var fieldsToNotReset = [
    'field_first_name',
    'field_last_name',
    'field_company_name',
    'field_country',
    'field_opt_in_gdpr_cookie'
  ];

  Drupal.behaviors.manhProgressiveProfiling = {
    $forms: $(),
    originalEmail: null,
    attach: function(context, settings) {
      var cookieConsent = true;

      // Get variables from manhCookiebot, if defined.
      if (typeof Drupal.manhUtils.manhCookiebot !== 'undefined') {
        // Do not track cookies unless we have consent.
        if (!Drupal.manhUtils.manhCookiebot.hasConsent()) {
          cookieConsent = false;
        }
      }
      else {
        // If they aren't, bounce.
        cookieConsent = false;
      }

      var self = this;

      // Get variables from drupalSettings, if defined.
      if (typeof settings.manhProgressiveProfile !== 'undefined') {
        if (typeof settings.manhProgressiveProfile.forms !== 'undefined') {
          var formIDs = settings.manhProgressiveProfile.forms;
          for (var i = 0; i < formIDs.length; i++) {
            self.$forms = self.$forms.add('#' + formIDs[i], context);
          }
        }
      }

      // Add a required_fields input to forms which don't have one.
      self.$forms.not(":has(input[name='required_fields'])")
        .append('<input type="hidden" name="required_fields" value="" />');

      // When a prepopulate function sets the email field from AJAX response
      // or cookies, that value is saved. When the form is submitted, if the
      // value of the email field is different, this is not the expected user,
      // so clear out any hidden fields which were set by cookies.
      self.$forms.submit(function() {
        var newEmail = $('#' + marketoLookupIds.field_email).val();
        if (newEmail != self.originalEmail) {
          for (var field in marketoLookupIds) {
            if (marketoLookupIds.hasOwnProperty(field)) {
              var fieldID = marketoLookupIds[field];
              var $field = $('#' + fieldID);
              if ($field.closest('.form-wrapper').is(':not(:visible)')) {
                $field.val('');
              }
            }
          }
        }
      });
    },
    /**
     *  Run progressive profiling functionality on each form that needs it.
     */
    run: function() {
      var self = this;
      var cookieConsent = true;

      // Get variables from manhCookiebot, if defined.
      if (typeof Drupal.manhUtils.manhCookiebot !== 'undefined') {
        // Do not track cookies unless we have consent.
        if (!Drupal.manhUtils.manhCookiebot.hasConsent()) {
          cookieConsent = false;
        }
      }
      else {
        // If they aren't, bounce.
        cookieConsent = false;
      }

      // Force reanswer if fields weren't fully filled out in the last 2 months.
      // The expiration of these cookies is managed server-side in the
      // manh_prepopulate custom module.
      var isExpiredForm = (!cookieConsent || typeof $.cookie('manh.forms.lastSubmit') === 'undefined');

      self.$forms.each(function() {
        var $form = $(this);
        var requiredFieldCount = 0;

        // Iterate through all fields that can be used for progressive profiling.
        progressiveProfFieldSetsPerPage.forEach(function(progressiveProfFieldSet) {
          var numFieldsOnCurrentPage = progressiveProfFieldSet.length;

          if (requiredFieldCount < numFieldsOnCurrentPage
            && requiredFieldCount > 0) {
            // If we've already gone through the for-loop below and the length
            // of the number of fields on this page is greater than
            // requiredFieldCount, then set requiredFieldCount equal to one
            // greater than the set of fields on this page.
            requiredFieldCount = numFieldsOnCurrentPage + 1;
          }
          progressiveProfFieldSet.forEach(function(progressiveProfField) {
            var $field = $('#' + progressiveProfField.id);
            var fieldName = progressiveProfField.name;
            self.hideField($form, $field, fieldName);

            // If the field exists and we haven't required all of the fields
            // that should display on this page yet, this might be one we want
            // to require.
            if ($field.length && requiredFieldCount < numFieldsOnCurrentPage) {
              // Was this field filled out in the last 2 months? The expiration
              // of these cookies is managed server-side in the manh_prepopulate
              // custom module.
              var isExpiredField = (!cookieConsent || typeof $.cookie('manh.forms.answerTime.' + fieldName) !== 'undefined');

              // If the field is 'empty', or hasn't been validated server-side in
              // the last 2 months, show it.
              if (self.fieldNeedsResponse($field.val(), isExpiredForm, isExpiredField)) {
                self.showField($form, $field, fieldName);
                requiredFieldCount++;

                // If expired, clear value unless it is a field in
                // fieldsToNotReset.
                if (fieldsToNotReset.indexOf(fieldName) == -1) {
                  // Reset a field value to empty.
                  self.resetFieldValue($form, $field, fieldName);
                }
              }
            }
          });
        });
      });
    },

    // Show field and make required.
    showField: function($form, $field, name) {
      $field.attr('required', 'required').addClass('required');
      // For select element, add "required" class to related chosen element.
      if ($field.is('select')) {
        var chosen = $field.nextAll('.chosen-container');
        if (chosen.length > 0) {
          chosen.addClass('required');
        }
      }
      $field.closest('.form-wrapper').removeClass('hidden')
        .find('label').addClass('js-form-required form-required');
      Drupal.behaviors.manhProgressiveProfiling.addFieldToRequired($form, name);
    },

    // Add a field name to the hidden "required fields" input.
    addFieldToRequired: function($form, name) {
      var $requiredField = $form.find("input[name='required_fields']");
      var required = $requiredField.val();
      required = (required === "" ? [] : required.split(','));
      var index = required.indexOf(name);
      if (index === -1) {
        required.push(name);
        $requiredField.val(required.join());
      }
    },

    // Hide field and make it optional.
    hideField: function($form, $field, name) {
      $field.removeAttr('required').removeClass('required');
      // For select element, remove "required" class from related chosen element.
      if ($field.is('select')) {
        var chosen = $field.nextAll('.chosen-container');
        if (chosen.length > 0) {
          chosen.removeClass('required');
        }
      }
      $field.closest('.form-wrapper').addClass('hidden')
        .find('label').removeClass('js-form-required form-required');
      Drupal.behaviors.manhProgressiveProfiling.removeFieldFromRequired($form, name);
    },

    // Remove a field name from the hidden "required fields" input.
    removeFieldFromRequired: function($form, name) {
      var $requiredField = $form.find("input[name='required_fields']");
      var required = $requiredField.val().split(',');
      var index = required.indexOf(name);
      if (index > -1) {
        required.splice(index, 1);
        $requiredField.val(required.join());
      }
    },

    // Reset a field value to empty.
    resetFieldValue: function($form, $field, name) {
      // Account for special handling required by Chosen widget.
      if ($field.is('select')) {
        $field.val('_none');

        // Alert Chosen that the field has changed.
        $field.trigger('chosen:updated');
      }
      else {
        $field.val('');
      }
      // Trigger an event to indicate prepopulation has reset the field.
      $field.trigger('manh_prepopulate:reset');
    },

    // Determines if a field is empty.
    fieldNeedsResponse: function(fieldVal, isExpiredForm, isExpiredField) {
      return (fieldVal === '' || fieldVal === null || fieldVal === '_none'
        || (isExpiredForm && !isExpiredField))
    }
  };

  Drupal.behaviors.manhPrepopulate = {
    $forms: $(),
    user: null,
    attach : function(context, settings) {
      var cookieConsent = true;

      // Get variables from manhCookiebot, if defined.
      if (typeof Drupal.manhUtils.manhCookiebot !== 'undefined') {
        // Do not track cookies unless we have consent.
        if (!Drupal.manhUtils.manhCookiebot.hasConsent()) {
          cookieConsent = false;
        }
      }
      else {
        // If they aren't, bounce.
        cookieConsent = false;
      }

      var self = this;

      // Get variables from drupalSettings, if defined.
      if (typeof settings.manhPrepopulate !== 'undefined') {
        if (typeof settings.manhPrepopulate.forms !== 'undefined') {
          var formIDs = settings.manhPrepopulate.forms;
          for (var i = 0; i < formIDs.length; i++) {
            self.$forms = self.$forms.add('#' + formIDs[i], context);
          }
        }
      }

      // Start prepopulation if any forms require it.
      if (self.$forms.length) {
        // Prepopulate with cookie values.
        if (cookieConsent) {
          self.prepopulate();
        }

        // Query the endpoint for supplementary user data.
        $.ajax({
          url: '/ajax/prepopulate',
          // Whether this succeeds or fails, we still want to attempt
          // progressive profiling if necessary.
          complete: function (response) {
            var data = response.responseJSON;
            if (cookieConsent && typeof data !== 'undefined' && typeof data.user !== 'undefined') {
              self.user = data.user;
              self.prepopulate();
            }

            // Run progressive profiling on any forms which may need it.
            Drupal.behaviors.manhProgressiveProfiling.run();
          }
        });
      }

      // TODO: See if it makes sense to move this into manh_campaign_tracker.
      // Remove the protocol prefix from the conversion URL to support tracking in Marketo emails.
      var conversion_url = window.location.href.replace(/https?:\/\//i, "");
      // Remove the query parameters to support email link generation with additional parameters.
      conversion_url = conversion_url.split('?')[0];
      self.$forms.find("input[name='field_conversion_url[0][value]']").val(conversion_url);
    },

    /**
     * Runs prepopulation on available fields. If a user has been returned by
     * the API call, its values are used, otherwise applicable cookies are used.
     */
    prepopulate: function() {
      var self = this;
      var user = this.user;
      // If a user object is set, populate the form with its values.
      if (user) {
        for (var field in user) {
          if (user.hasOwnProperty(field)) {
            var fieldId = marketoLookupIds[field];
            // If the field ID is set, look it up and set its value.
            if (typeof fieldId !== 'undefined') {
              // Populate the field with the user value.
              self.populateField(field, fieldId, user[field]);
            }
          }
        }
      }
      // No user object is set. Populate the form with cookie values.
      else {
        // Iterate through the fields we care about.
        for (var field in marketoLookupIds) {
          if (marketoLookupIds.hasOwnProperty(field)) {
            var fieldId = marketoLookupIds[field];
            var cookieName = 'manh.forms.' + field;
            var cookieVal = $.cookie(cookieName);
            if (typeof cookieVal !== 'undefined') {
              // Populate the field with the cookie value.
              self.populateField(field, fieldId, cookieVal);
            }
          }
        }
      }
    },

    /**
     * Handles value population of a single field.
     */
    populateField: function(field, fieldId, value) {
      var $element = $('#' + fieldId);
      $element.val(value);
      $element.trigger('change');
      if ($element.is('select')) {
        $element.trigger('chosen:updated');
      }
      else if ($element.is('input[type=checkbox]')) {
        $element.attr('checked', value)
      }

      // The prepopulated email is saved for comparison on submission.
      if (field === 'field_email') {
        Drupal.behaviors.manhProgressiveProfiling.originalEmail = $element.val();
      }

      // Trigger an event to indicate prepopulation is complete.
      $element.trigger('manh_prepopulate:complete');
    }
  };

})(jQuery, Drupal);
