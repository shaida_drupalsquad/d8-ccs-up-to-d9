<?php

/**
 * @file
 * Contains \Drupal\manh_prepopulate\Controller\PrepopulateController.php
 */

namespace Drupal\manh_prepopulate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Manh Prepopulate routes.
 */
class PrepopulateController extends ControllerBase {

  /**
   * The array mapping Marketo fields to Drupal fields for prepopulation.
   *
   * @var array
   */
  protected static $fields = [
    'email' => 'field_email',
    'firstName' => 'field_first_name',
    'lastName' => 'field_last_name',
    'company' => 'field_company_name',
    'title' => 'field_title',
    'country' => 'field_country',
    'phone' => 'field_telephone',
    'Do_you_have_a_project__c' => 'field_have_project',
    'What_is_your_project_timeline__c' => 'field_project_timeline',
    'Do_you_have_a_budget__c' => 'field_have_budget',
    'How_did_you_hear_about_us_del__c' => 'field_heard_how'
  ];

  /**
   * Returns any known data about the user in question.
   *
   * @param Request $request
   *   The current request object.
   *
   * @return JsonResponse
   *   The response containing any known user data.
   */
  public function prepopulate(Request $request) {
    $response = [];

    // Get the user's email address from their cookies, then look up the user.
    $email = $cookies = $request->cookies->get('manh_forms_field_email');
    $user = $this->getUser($email);

    // If we got a user from Marketo, format and return their data.
    if (isset($user)) {
      $formatted_user = [];
      foreach (PrepopulateController::$fields as $api_name => $field_name) {
        if (isset($user[$api_name])) {
          $formatted_user[$field_name] = $user[$api_name];
        }
      }
      $response['user'] = $formatted_user;
    }

    return new JsonResponse($response);
  }

  /**
   * Retrieves a Marketo lead with a given email address.
   *
   * @param string $email
   *   The email address for the user lookup.
   *
   * @return array
   *   Lead data set.
   */
  private function getUser($email) {
    if (!isset($email)) {
      return NULL;
    }

    // Get the Marketo settings.
    $config = \Drupal::config('marketo_ma.settings');

    // Attempt to retrieve a lead by their email address.
    // TODO: Cache access key and do manual query to avoid unnecessary API call.
    if ($config->get('tracking_method') == 'api_client') {
      /** @var MarketoMaApiClientInterface $marketo_client */
      $marketo_client = \Drupal::service('marketo_ma.api_client');
      $field_keys = array_keys(PrepopulateController::$fields);
      $lead = $marketo_client->getLeadByEmail($email, $field_keys);
      if (isset ($lead)) {
        return $lead->data();
      }
    }
    return NULL;
  }

}
