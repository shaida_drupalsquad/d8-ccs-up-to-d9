<?php

namespace Drupal\manh_scheduler\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a Batch example Form.
 */
class BatchSaveForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'batch_save_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Query nodes with document media.
    $query = \Drupal::entityQuery('node')
      ->condition('status', [0, 1], 'IN');

    $or_condition = $query->orConditionGroup()
      ->exists('field_bio_doc_media')
      ->exists('field_doc_media')
      ->exists('field_earnings_doc_media');

    $query->condition($or_condition);
    $nids = $query->execute();

    $batch = [
      'title' => $this->t('Resaving @count nodes...', [
        '@count' => count($nids),
      ]),
      'operations' => [],
      'init_message' => t('Commencing'),
      'progress_message' => t('Resaved @current out of @total.'),
      'error_message' => t('An error occurred during processing'),
      'finished' => '\Drupal\manh_scheduler\SchedulerBatchOps::batchFinishedCallback',
    ];

    foreach ($nids as $nid) {
      $batch['operations'][] = [
        '\Drupal\manh_scheduler\SchedulerBatchOps::resaveBatch', [$nid],
      ];
    }

    batch_set($batch);

  }

}
