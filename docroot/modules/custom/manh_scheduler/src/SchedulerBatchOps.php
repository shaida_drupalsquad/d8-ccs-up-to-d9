<?php

namespace Drupal\manh_scheduler;

use Drupal\node\Entity\Node;

class SchedulerBatchOps {

  public static function resaveBatch($nid) {
    $results = [];
    $node = Node::load($nid);
    $langs = $node->getTranslationLanguages();
    $saved_langs = [];

    // Loop through the node's languages, resaving them.
    if (!empty($langs)) {
      foreach ($langs as $lang) {
        $lang_code = $lang->getId();

        if ($lang_code !== 'und') {
          $translation = $node->getTranslation($lang_code);
          $results[] = $translation->save();
          $saved_langs[] = $lang_code;
        }
      }
    }

    $context['message'] = 'Resaving node ' . $nid . ' languages: ' . implode(', ', $saved_langs);
    $context['results'] = $results;
  }

  public function batchFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One translation resaved.', '@count translations resaved.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
