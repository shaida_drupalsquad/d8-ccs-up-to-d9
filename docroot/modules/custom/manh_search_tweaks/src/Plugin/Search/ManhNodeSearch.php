<?php

/**
 * @file
 * Contains \Drupal\manh_search_tweaks\Plugin\Search\ManhNodeSearch.
 */

namespace Drupal\manh_search_tweaks\Plugin\Search;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\search\Plugin\SearchIndexingInterface;
use Drupal\Search\SearchQuery;
use Drupal\node\Plugin\Search\NodeSearch;

/**
 * Copy of Node's search plugin set to search limited node types.
 *
 * @SearchPlugin(
 *   id = "manh_node_search",
 *   title = @Translation("Manh Content")
 * )
 */
class ManhNodeSearch extends NodeSearch implements AccessibleInterface, SearchIndexingInterface {

  /**
   * {@inheritdoc}
   */
  protected function findResults() {
    // Retrieve the langcode of the interface language.
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Replace any filters provided in the URL and use our own.
    $parameters = $this->getParameters();
    $parameters['f'] = [
      'type:article',
      'type:document',
      'type:event',
      'type:image',
      'type:page',
      'type:press_release',
      'type:video',
      'type:webinar',
      'language:' . $langcode,
    ];

    // Update and invoke the search.
    $keywords = $this->keywords;
    $attributes = $this->getAttributes();
    $this->setSearch($keywords, $parameters, $attributes);

    return parent::findResults();
  }
}
