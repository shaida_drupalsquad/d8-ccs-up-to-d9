<?php

namespace Drupal\marketo_ma_personalization\Element;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Template\Attribute;

/**
 * Provides the personalization block render element.
 *
 * @RenderElement("personalization_block_block")
 */
class PersonalizationBlockBlock extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#theme' => 'personalization_block',
      '#cache' => [
        'contexts' => [
          'url.path',
          // Vary by permission to account for module-specific permissions.
          'user.permissions',
        ],
      ],
       '#pre_render' => [
         [$class, 'preRender']
       ],
    ];
  }

  /**
   * #pre_render callback for elements of type 'personalization_block'.
   *
   * @param array $element
   *   The render array for the element to be prepared.
   *
   * @return array
   *   The preprocessed render element.
   */
  public static function preRender(array $element) {
    $block = $element['#block'];

    // Expose entity-specific properties for rendering.
    $element['#block_id'] = $block->id;
    $element['#zone_id'] = $block->zone_id;

    $attributes = new Attribute();
    $attributes['class'] = [
      'rtp--content-personalization-block',
    ];
    $element['#attributes'] = $attributes;

    // Set a flag to show or hide the placeholder details based on permissions.
    // @todo Inject the user object somewhere instead.
    $element['#show_placeholder'] = \Drupal::currentUser()
      ->hasPermission('administer marketo personalization blocks');

    // Bubble entity-specific cache data.
    $cache_data = CacheableMetadata::createFromRenderArray($element);
    $cache_data->addCacheableDependency($block);
    $cache_data->applyTo($element);

    return $element;
  }

}
