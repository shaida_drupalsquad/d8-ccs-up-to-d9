<?php

namespace Drupal\marketo_ma_personalization\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\marketo_ma_personalization\PersonalizationBlockEntityInterface;

/**
 * Defines a Personalization Block configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "personalization_block",
 *   label = @Translation("Personalization Block"),
 *   fieldable = FALSE,
 *   handlers = {
 *     "list_builder" = "Drupal\marketo_ma_personalization\Entity\PersonalizationBlockListBuilder",
 *     "form" = {
 *       "add" = "Drupal\marketo_ma_personalization\Form\PersonalizationBlockForm",
 *       "edit" = "Drupal\marketo_ma_personalization\Form\PersonalizationBlockForm",
 *       "delete" = "Drupal\marketo_ma_personalization\Form\PersonalizationBlockDeleteForm",
 *     }
 *   },
 *   config_prefix = "personalization_block",
 *   admin_permission = "administer marketo personalization blocks",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/personalization-block/{personalization-block}/edit",
 *     "delete-form" = "/admin/structure/personalization-block/{personalization}/delete"
 *   }
 * )
 *
 * @package Drupal\marketo_ma_personalization\Entity
 */
class PersonalizationBlockEntity extends ConfigEntityBase implements PersonalizationBlockEntityInterface {

  /**
   * The ID of the personalization block entity.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label for the personalization block.
   *
   * @var string
   */
  public $name;

  /**
   * The Marketo zone ID to indicate the container to be replaced.
   *
   * @var string
   */
  public $zone_id;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Invalidate the block cache to update custom block-based derivatives.
    \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    // Invalidate the block cache to update custom block-based derivatives.
    \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
  }

}
