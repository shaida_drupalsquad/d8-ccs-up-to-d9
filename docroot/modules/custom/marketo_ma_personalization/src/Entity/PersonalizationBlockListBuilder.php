<?php

namespace Drupal\marketo_ma_personalization\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Entity list builder for Personalization Block entities.
 *
 * @package Drupal\marketo_ma_personalization\Entity
 */
class PersonalizationBlockListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['zone_id'] = $this->t('Zone Id');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['zone_id'] = $entity->zone_id;

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    // Add a message when there are no entities to display.
    $build['#empty'] = $this->t('There are no personalization blocks available.');

    return $build;
  }

}
