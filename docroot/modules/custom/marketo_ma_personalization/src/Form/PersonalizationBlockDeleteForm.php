<?php

namespace Drupal\marketo_ma_personalization\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form that handles removal of personalization block entities.
 *
 * @package Drupal\marketo_ma_personalization\Form
 */
class PersonalizationBlockDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this personalization block: @name?',
      ['@name' => $this->entity->name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('personalization_block.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the entity.
    $this->entity->delete();

    // Set a notification message and redirect.
    drupal_set_message($this->t('The personalization block @name has been deleted.',
      ['@name' => $this->entity->name]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
