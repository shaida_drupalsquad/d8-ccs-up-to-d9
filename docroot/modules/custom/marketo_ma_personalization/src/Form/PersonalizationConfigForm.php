<?php
/**
 * @file
 * Contains Drupal\marketo_ma_personalization\Form\PersonalizationConfigForm.
 */

namespace Drupal\marketo_ma_personalization\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Marketo Personalization integrations.
 *
 * @package Drupal\marketo_ma_personalization\Form
 */
class PersonalizationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_ma_personalization_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'marketo_ma_personalization.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('marketo_ma_personalization.settings');

    // @todo Hide additional fields based on this value.
    // @todo Add description text.
    // @todo Add conditional requirements for other fields.
    $form['enable_rtp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable RTP script'),
      '#default_value' => $config->get('enable_rtp'),
    ];

    // @todo Group additional fields together in fieldsets.
    // @todo Add description text.
    $form['rtp_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RTP Javascript Library'),
      '#default_value' => $config->get('rtp_url'),
    ];

    // @todo Add description text.
    $form['rtp_account'] = [
      '#type' => 'textfield',
      '#title' => 'RTP Account Id',
      '#default_value' => $config->get('rtp_account'),
    ];

    // @todo Add configuration for the tag option to use.
    // $form['rtp_tag_option'] = [];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory()
      ->getEditable('marketo_ma_personalization.settings');

    $settings->set('enable_rtp', $form_state->getValue('enable_rtp'))
      ->set('rtp_url', $form_state->getValue('rtp_url'))
      ->set('rtp_account', $form_state->getValue('rtp_account'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
