<?php
/**
 * @file
 * Contains Drupal\marketo_ma_predictive\Element\PredictiveContentBlock.
 */

namespace Drupal\marketo_ma_predictive\Element;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Template\Attribute;

/**
 * Provides the predictive content block render element.
 *
 * Configuration options for how the content is rendered from Marketo may be
 * provided within the '#rtp_configuration' value. For a list of these options
 * see
 * @link http://developers.marketo.com/javascript-api/web-personalization/rich-media-recommendation/ the Marketo documentation. @endlink
 *
 * @RenderElement("predictive_content_block")
 */
class PredictiveContentBlock extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#theme' => 'predictive_content_block',
      '#cache' => [
        'contexts' => [
          'url.path',
          // Vary by permission to account for module-specific permissions.
          'user.permissions',
        ],
      ],
      '#attached' => [
        'library' => [
          'marketo_ma_predictive/marketo_predictive',
        ],
        'drupalSettings' => [
          'marketoMaPredictive' => [
            'rtp_configuration' => [],
          ],
        ],
      ],
      '#pre_render' => [
        [$class, 'preRender']
      ]
    ];
  }

  /**
   * #pre_render callback for elements of type 'predictive_content_block'.
   *
   * @param array $element
   *   The render array for the element to be prepared.
   *
   * @return array
   *   The preprocessed render element.
   */
  public static function preRender(array $element) {

    $block = $element['#block'];

    // Expose entity-specific properties for rendering.
    $element['#block_id'] = $block->id;
    $element['#template_id'] = $block->template_id;
    $element['#rtp_class'] = 'RTP_RCMD2';
    $element['#rtp_block_class'] = 'rtp--predictive--' . $block->id;

    // Prepare attributes for the template.
    $attributes = new Attribute();
    $attributes['class'] = [
      $element['#rtp_class'],
      $element['#rtp_block_class'],
      'rtp--predictive--' . $block->template_id,
    ];
    $element['#attributes'] = $attributes;

    // Prepare a bin for any configuration items.
    // For options see http://developers.marketo.com/javascript-api/web-personalization/rich-media-recommendation/
    $element['#rtp_configuration'] = [];

    // Add the block to a list of them in drupalSettings.
    $element['#attached']['drupalSettings']['marketoMaPredictive']['rtp_blocks'][$block->id] = $element['#rtp_block_class'];

    // Expose configuration values within the attached settings.
    // @todo Determine a better way to more explicitly expose this value later in the process.
    $element['#attached']['drupalSettings']['marketoMaPredictive']['rtp_configuration'] = &$element['#rtp_configuration'];

    // Set a flag to show or hide the placeholder details based on permissions.
    // @todo Inject the user object somewhere instead.
    $element['#show_placeholder'] = \Drupal::currentUser()
      ->hasPermission('administer marketo predictive blocks');

    // Bubble entity-specific cache data.
    $cache_data = CacheableMetadata::createFromRenderArray($element);
    $cache_data->addCacheableDependency($block);
    $cache_data->applyTo($element);

    return $element;
  }

}
