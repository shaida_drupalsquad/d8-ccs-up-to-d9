<?php
/**
 * @file
 * Contains \Drupal\marketo_ma_predictive\Entity\PredictiveContentEntity.
 */

namespace Drupal\marketo_ma_predictive\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\marketo_ma_predictive\PredictiveContentEntityInterface;

/**
 * Defines a Predictive Content configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "predictive_content",
 *   label = @Translation("Predictive Content"),
 *   fieldable = FALSE,
 *   handlers = {
 *     "list_builder" = "Drupal\marketo_ma_predictive\Entity\PredictiveContentListBuilder",
 *     "form" = {
 *       "add" = "Drupal\marketo_ma_predictive\Form\PredictiveContentForm",
 *       "edit" = "Drupal\marketo_ma_predictive\Form\PredictiveContentForm",
 *       "delete" = "Drupal\marketo_ma_predictive\Form\PredictiveContentDeleteForm",
 *     }
 *   },
 *   config_prefix = "predictive_content",
 *   admin_permission = "administer marketo predictive blocks",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/predictive-content/{predictive-content}/edit",
 *     "delete-form" = "/admin/structure/predictive-content/{predictive-content}/delete"
 *   }
 * )
 *
 * @package Drupal\marketo_ma_predictive\Entity
 */
class PredictiveContentEntity extends ConfigEntityBase implements PredictiveContentEntityInterface {

  /**
   * The ID of the predictive content entity.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label for the predictive content block.
   *
   * @var string
   */
  public $name;

  /**
   * The Marketo template ID to render with.
   *
   * @var string
   */
  public $template_id;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Invalidate the block cache to update custom block-based derivatives.
    \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    // Invalidate the block cache to update custom block-based derivatives.
    \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
  }

}
