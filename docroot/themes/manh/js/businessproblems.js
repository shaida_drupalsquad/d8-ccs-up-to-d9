/**
 * @file
 * This jquery controls the accordion behavior on the Business Problems Displays. 
 */

(function($, Drupal) {
  "use strict";

  $(document).ready(function() {
    $('.business-problem-header').on('click', function() {
      $(this).parent('.business-problem-wrapper').toggleClass('open');
    })
  });

})(jQuery, Drupal);
