/**
 * @file
 * This jquery controls the contact us block in the secondary navigation section.
 */

// Make this variable available to either the Drupal or IR version as needed.
manhContactMenuBlockBehavior = {};

(function($){
  'use strict';

  manhContactMenuBlockBehavior = function() {
    var $arrowContent = $('.js-arrow-content');
    var $arrowIcon = $('.js-arrow-down-icon');
    var clicked = false;

    $arrowIcon
      // On click open the content container.
      // We would normally use toggleClass here, but this has
      // to work with mouseover so we can't.
      .on('click mouseover', function(e) {
        if (e.type === 'click') {
          if (clicked === false) {
            clicked = true;
            $arrowContent.addClass('open');
          }
          else {
            clicked = false;
            $arrowContent.removeClass('open');
          }
        }
        else {
          if (!$arrowContent.hasClass('open')) {
            $arrowContent
              .addClass('open')
              .on('mouseleave', function() {
                // If we've clicked the button after we've hovered over it,
                // keep it open.
                if (clicked === false) {
                  $arrowContent
                    .removeClass('open')
                    .off('mouseleave');
                }
              });
          }
        }
      });
  };
})(jQuery);
