/* Start Lazy Loader */
(function ($) {
  'use strict';

  Drupal.behaviors.startLazy = {
    attach: function(context, settings) {
      $(".lazy").lazy();
    }
  };

}(jQuery));
