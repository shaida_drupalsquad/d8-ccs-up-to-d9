/**
 * @file
 * Lightbox.
 */

(function($, Drupal) {
  "use strict";

  $(document).ready(function() {
    var $imageDisplaymode = $('.node--type-image .display-mode');
    var $imageOverlay = $('.image-fullsize-overlay');
    var $imageFull = $('.image-fullsize');

    $imageDisplaymode.addClass('selected');
    $imageDisplaymode.on('click', function() {
      $(this).next($imageFull).fadeIn(400);
      $imageOverlay.fadeIn(400);
    });

    $('.image-fullsize-close').on('click', function() {
      $imageFull.fadeOut(400);
      $imageOverlay.hide();
    });
  });

})(jQuery, Drupal);
