/**
 * @file
 * This jquery controls the accordion behavior on the phone icon on the mobile display. 
 */



(function($, Drupal) {

  Drupal.behaviors.showhide = {
    attach: function (context, settings) {
      if (context === document) {
        $('.phone-icon').on('click', function(){
          $('#block-mobilephoneblock').toggleClass('open');
        });
      }
    }
  };

})(jQuery, Drupal);
