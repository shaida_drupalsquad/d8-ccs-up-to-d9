/**
 * @file
 * This jquery dynamically adds a MORE link to the end of every secondary nav.
 */

(function ($, Drupal) {
  "use strict";

  function init($secondarynav) {
    var $target = $('.secondarymenu-level-wrapper.level-1');
    var $level2 = $('.secondarymenu-level-wrapper.level-2 ul.level-2');
    var $tabTrigger = $('.secondarymenu button.tabs__trigger');
    $target.addClass('tabs');
    $('li', $target).addClass('tabs__tab');
    $secondarynav.addClass('is-horizontal');
    var intrinsicWidth = $secondarynav.intrinsic('width');

    function openMenu() {
      $tabTrigger.toggleClass('open');
      $target.toggleClass('is-open');
      if ($level2.length > 0) {
        $level2.toggleClass('hidden');
      }
    }

    function handleResize() {
      var isHorizontal = $secondarynav.parent().width() > intrinsicWidth;
      $secondarynav.toggleClass('is-horizontal', isHorizontal);
      $secondarynav.toggleClass('is-collapse-enabled', !isHorizontal);
      if (isHorizontal) {
        $target.removeClass('is-open');
        if ($level2.length > 0) {
          $level2.removeClass('hidden');
        }
      }
    }
    $secondarynav.addClass('position-container is-horizontal-enabled is-collapsible');

    // Hide/show the responsive menu.
    $secondarynav.on('click.tabs', '[data-drupal-nav-tabs-trigger]', openMenu);

    // Handle tab resize.
    $(window).on('resize.tabs', Drupal.debounce(handleResize)).trigger('resize.tabs');
  }

  Drupal.behaviors.manhSecondaryNav = {
    attach: function (context) {
      if (context == document) {
        /* Set variable for the nav menu */
        var $secondarynav = $('.secondarymenu-main');
        if ($secondarynav.length) {
          var notSmartPhone = window.matchMedia('(min-width: 300px)');
          if (notSmartPhone.matches) {
            init($secondarynav);
          }
        }
      }
    }
  };

})(jQuery, Drupal);
