!((document, $, Drupal) => {
  'use strict';
  Drupal.behaviors.stickyHeader = {
    /* eslint-disable max-len */

    // Attach method will run when behaviors mounts.
    attach: function () {

      // Initialize variables.
      var body = document.body;
      var $search = $('.region-header-secondary #block-manh-search');
      var $menu_item = $('.block-header-navigation .menu-item');
      var $tel = $('.block-header-navigation .menu-item--tel');
      var $tel_link = $tel.find('a');
      var $self;
      var contact_offset;
      var search_offset;
      var tel_offset;

      window.onscroll = function () {
        if (window.scrollY !== 0) {
          // Update our width/offset values.
          contact_offset = $('.menu-item--contact').outerWidth(true);
          search_offset = $search.outerWidth(true);

          // Get width of links that will be hidden, so that we can offset the
          // tel number link.
          tel_offset = 0;
          $menu_item.each(function () {
            $self = $(this);
            if (!$self.hasClass('menu-item--contact') && !$self.hasClass('menu-item--tel')) {
              tel_offset += $self.outerWidth(true);
            }
          });

          // Add the sticky class to the header when you reach its scroll position.
          body.classList.add('ma-sticky-header');

          // Update transform offsets.
          $search.css('transform', 'translate(-' + contact_offset + 'px, -46px)');
          $tel_link.css('transform', 'translate(' + (tel_offset - search_offset - parseInt($tel_link.css('marginLeft'))) + 'px, -5px)');
        }
        else {
          // Remove 'sticky' when you leave the scroll position.
          body.classList.remove('ma-sticky-header');

          // Clear out transform offsets.
          $search.css('transform', 'translate(0, 0)');
          $tel_link.css('transform', 'translate(0, 0)');
        }
      };
    }
  };
})(document, jQuery, Drupal);
