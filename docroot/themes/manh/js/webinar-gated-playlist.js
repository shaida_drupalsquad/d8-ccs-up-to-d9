/**
 * @file
 * For handles problem with video row width when a webinar is gated.
 */
(function($, Drupal) {
  "use strict";

  Drupal.behaviors.manhWebinarGatedPlaylist = {
    attach: function(context, settings) {
      // This behavior must be triggered with each ajax update.
      var teaserImage = $('.l-top .page-teaser-image');
      if (teaserImage.length > 0) {
        // Existence of teaser image means non-playlist layout should be used.
        $('div[role="main"]').removeClass('with-playlist');
      }
    }
  };

})(jQuery, Drupal);
