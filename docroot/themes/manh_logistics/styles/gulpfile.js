'use strict';

// Getting started with Gulp
// You've already got Node JS installed don't you? If not hit up nodejs.org and hit the big install button.
//
// Install Gulp globally if you haven't already:
//
// npm install --global gulp
//
// Next install all gulp dependencies declared in the package.json file. From the theme directory run:
//
// npm install
//
// which references package.json and installs the nodes listed there.
// Sweet. Gulp should be ready to go. Let's make it do work. Here's a list of the available gulp commands.
//
// gulp
// This compiles sass and runs KSS node to generate the style guide.
// npm run gulp
//
// gulp sass
// Yup, you guessed it. This compiles sass.
// npm run gulp-sass
//
// gulp styleguide
// Runs KSS Node and generates the style guide.
// npm run gulp-styleguide
//
// gulp watch
// Watches the sass directory for changes. As soon as a file changes the sass task is fired and the css is regenerated.
// npm run gulp-watch
//
// gulp watch:styleguide
// Watches the sass directory for changes. As soon as a file changes the styleguide task is fired and the styleguide is rebuilt. You'll still need to refresh the styleguide page.
// npm run gulp-watch-styleguide
//
// gulp watch:browsersync
// Watches the sass directory for changes. As soon as the CSS is updated browser-sync will inject the new CSS.
// npm run gulp-browsersync

// If you don't have package.json run this command:
// npm install gulp gulp-ruby-sass gulp-autoprefixer gulp-livereload gulp-shell gulp-plumber kss --save-dev
// note that the --save-dev flag will write the package.json file with this list of module dependencies.

// You can and should modify the styleguide.md file in the sass folder and styleguide/Theme/index.html
// Change the page title, css links, and  header title


//////////////////////////////////////////////
// Include gulp
var gulp = require('gulp');

//////////////////////////////////////////////
// Include Our Plugins
var sass      = require('gulp-sass');
var prefix    = require('gulp-autoprefixer');
var plumber   = require('gulp-plumber');
var shell     = require('gulp-shell');
var svg2png   = require('gulp-svg2png');
var sync      = require('browser-sync');
var reload    = sync.reload;
var filter    = require('gulp-filter');
var imagemin  = require('gulp-imagemin');
var pngquant  = require('imagemin-pngquant');

//////////////////////////////////////////////
// Define file paths and configurations
var config = {
  images: 'images/*',
  build: 'images',
  sass: 'sass/{,**/}*.{scss,sass}',
  icons: './images/icons',
  svg2png: './images/icons/**/*.svg'
  // kss-node [source files to parse] [destination folder] --template [location of template files]
  // styleguide: 'kss-node <%= source %> <%= destination %> --template <%= template %>'
};

//////////////////////////////////////////////
// Compile Our Sass
gulp.task('sass', function() {
  gulp.src('./sass/{,**/}*.scss')
    .pipe(sass({
      outputStyle: 'nested',
      errLogToConsole: true
    }))
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('css'));
});

//////////////////////////////////////////////
// Compress svg and image files
gulp.task('images', function () {
  return gulp.src(config.images)
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    // Move compress image files to build folder
    .pipe(gulp.dest('images'));
});


//////////////////////////////////////////////
// Watch and recompile sass.
// Refreshes the CSS in the browser with browsersync.
gulp.task('watch', function() {

  // BrowserSync proxy setup
  sync({
      proxy: 'http://logistics.mcdev'
  });

  // Watch all my sass files and run styleguide task.
  gulp.watch('sass/{,**/}*.{scss,sass}', ['sass']);
  // Watch ./image folder for changes
  gulp.watch(config.images, [config.build]);
});


//////////////////////////////////////////////
// SVG to PNG Task
gulp.task('svg2png', function () {
    gulp.src('config.svg2png')
        .pipe(svg2png())
        .pipe(gulp.dest(path.icons));
});


//////////////////////////////////////////////
// Default Task
gulp.task('default', ['watch']);
