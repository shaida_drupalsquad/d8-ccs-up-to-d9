<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}

// Vagrant local development vm.
$aliases['mcdev'] = array(
  'uri' => 'manh.mcdev',
  'root' => '/home/vagrant/docroot/web',
  'uri' => 'manh.mcdev',
  'remote-host' => 'manh.mcdev',
  'path-aliases' => array(
    '%drush-script' => '/home/vagrant/docroot/bin/drush',
  ),
  'remote-user' => 'vagrant',
);

// Dev environment.
$aliases['dev'] = array(
  'parent' => '@pantheon.manhcom.dev',
);

// Staging/Test environment.
$aliases['test'] = array(
  'parent' => '@pantheon.manhcom.test',
);

// Production environment.
$aliases['live'] = array(
  'parent' => '@pantheon.manhcom.live',
);

// Multi-dev environments.
$aliases['develop'] = array(
  'parent' => '@pantheon.manhcom.develop',
);

if('vagrant' !== getenv('USER')) {
    $aliases['mcdev']['remote-host'] = 'manh.mcdev';
    $aliases['mcdev']['remote-user'] = 'vagrant';
    $aliases['mcdev']['ssh-options'] = '-o PasswordAuthentication=no -i ${HOME}/.vagrant.d/insecure_private_key';
}
