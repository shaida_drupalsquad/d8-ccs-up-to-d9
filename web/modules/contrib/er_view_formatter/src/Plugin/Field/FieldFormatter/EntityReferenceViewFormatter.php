<?php
/**
 * @file
 * Contains \Drupal\er_view_formatter\Plugin\Field\FieldFormatter\EntityReferenceViewFormatter.
 */

namespace Drupal\er_view_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\views\Entity\View;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Plugin implementation of the Entity Reference View Formatter.
 *
 * @FieldFormatter(
 *   id = "er_view_formatter",
 *   label = @Translation("Rendered view"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceViewFormatter extends EntityReferenceFormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    /**
     * @var View $entity
     */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity instanceof View) {
        // Use the first embed display if available.
        if ($entity->getDisplay('embed_1')) {
          $display_id = 'embed_1';
        }
        // Otherwise, use first block display if available.
        elseif ($entity->getDisplay('block_1')) {
          $display_id = 'block_1';
        }
        // Last resort: use default display.
        else {
          $display_id = 'default';
        }
        $executable = $entity->getExecutable();
        $executable->setDisplay($display_id);

        $elements[$delta] = [
          '#type' => 'container',
          'view' => $executable->render(),
        ];
      }
      else {
        $elements[$delta] = array('#markup' => t('This is not a view'));
      }
    }

    return $elements;
  }

  /**
   * Overrides getEntitiesToView to enforce that access is TRUE.
   *
   * @see ::getEntitiesToView()
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = [];
    foreach ($items as $delta => $item) {

      // Ignore items where no entity could be loaded in prepareView().
      if (!empty($item->_loaded)) {
        $entity = $item->entity;

        // Set the entity in the correct language for display.
        if ($entity instanceof TranslatableInterface) {
          $entity = \Drupal::entityManager()
            ->getTranslationFromContext($entity, $langcode);
        }
        $access = TRUE;

        if ($access) {
          // Add the referring item, in case the formatter needs it.
          $entity->_referringItem = $items[$delta];
          $entities[$delta] = $entity;
        }
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for menu entities.
    $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    return $target_type == 'view';
  }
}

