<?php

/**
 * @file
 * Provide access permissions by language.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function language_access_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.language_access':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_language_switch_links_alter().
 */
function language_access_language_switch_links_alter(array &$links, $type, Url $url) {
  $languages = \Drupal::languageManager()->getLanguages();
  foreach ($languages as $language) {
    if (!\Drupal::currentUser()->hasPermission('access language ' . $language->getId())) {
      if (isset($links[$language->getId()])) {
        unset($links[$language->getId()]);
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for 'user_form'.
 */
function language_access_form_user_form_alter(&$form, FormStateInterface $form_state) {
  if (isset($form['language']['preferred_langcode'])) {
    // Add our processing hook. Ensure the target user is available for
    // permission checking.
    $form['language']['preferred_langcode']['#pre_render']['language_access'] = 'language_access_limit_language_options';
    $form['language']['preferred_langcode']['#for_user'] = $form_state->getFormObject()->getEntity();
  }
}

/**
 * Implements hook_form_ID_alter() for 'node_form'.
 */
function language_access_form_node_form_alter(&$form, &$form_state) {
  if (isset($form['langcode']['widget'][0]['value'])) {
    $form['langcode']['widget'][0]['value']['#pre_render']['language_access'] = 'language_access_limit_language_options';
    $form['langcode']['widget'][0]['value']['#for_user'] = \Drupal::currentUser();
  }
}

/**
 * Implements hook_form_alter() for 'taxonomy_term_form'.
 */
function language_access_form_taxonomy_term_form_alter(&$form, &$form_state) {
  if (isset($form['langcode']['widget'][0]['value'])) {
    $form['langcode']['widget'][0]['value']['#pre_render']['language_access'] = 'language_access_limit_language_options';
    $form['langcode']['widget'][0]['value']['#for_user'] = \Drupal::currentUser();
  }
}

/**
 * Alters the language widget options.
 *
 * Limits the options to those that the user is allowed to access.
 *
 * @param array $element
 *   The language form element.
 *
 * @return array
 *   The language form element.
 */
function language_access_limit_language_options(array $element) {
  $languages = \Drupal::languageManager()->getLanguages();
  foreach ($element['#options'] as $id => $description) {
    if (isset($languages[$id]) && !$element['#for_user']->hasPermission('access language ' . $id)) {
      unset($element['#options'][$id]);
    }
  }
  unset($element['#for_user']);
  return $element;
}
