/**
 * @file
 * Monitors query string for campaign tracking variables and updates cookies.
 */
!(function($) {
  'use strict';

  Drupal.behaviors.manhCampaignTracker = {
    attach: function() {
      var campaignVariables = [];
      var trackedForms = [];
      var doTrack = false;
      var campaignVariableCount;
      var parsedDomain = psl.parse(window.location.hostname);
      var cookieDomain = "";

      // Get variables from drupalSettings, if defined.
      if (typeof drupalSettings.manhCampaignTracker !== 'undefined') {
        if (typeof drupalSettings.manhCampaignTracker.utmFields !== 'undefined') {
          campaignVariables = drupalSettings.manhCampaignTracker.utmFields;
        }
        if (typeof drupalSettings.manhCampaignTracker.utmFields !== 'undefined') {
          trackedForms = drupalSettings.manhCampaignTracker.trackedForms;
        }
      }
      campaignVariableCount = campaignVariables.length;

      // Retrieve query params.
      let queryParams = window.location.search.substr(1).split('&').reduce(function(params, param) {
        let [key, value] = param.split('=');
        if (key && key.length > 0) {
          params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
        }
        return params;
      }, {});
      let queryParamKeys = Object.keys(queryParams);

      // Check query string for campaign variables.
      if (queryParamKeys.length > 0) {
        for (let key of queryParamKeys) {
          let value = queryParams[key];
          // We only care about params which are tracked campaign variables.
          if (campaignVariables.indexOf(key) > -1) {
            doTrack = true;
            // Update the form elements.
            let $input = $('.field--name-field-' + key.replace('_', '-') + ' input');
            if ($input.length > 0) {
              $input.val(value);
            }
          }
        }
      }
      // Otherwise, attempt to pull from the cookie version.
      else {
        let cookie_params = [
          'utm_source',
          'utm_campaign',
          'utm_term',
          'utm_content',
          'utm_medium',
        ];
        for (i = 0; i < cookie_params.length; i++) {
          let key = cookie_params[i];
          let value = $.cookie(cookie_params[i]);
          if (campaignVariables.indexOf(key) > -1 && value) {
            let $input = $('.field--name-field-' + cookie_params[i].replace('_', '-') + ' input');
            if ($input.length > 0) {
              $input.val(value);
            }
          }
        }
      }

      // Prepares domain for cookie.
      if (parsedDomain.domain == 'manh.com') {
        cookieDomain = parsedDomain.domain;
      }
      else {
        // For testing on Pantheon.
        cookieDomain = parsedDomain.subdomain + '.' + parsedDomain.domain;
      }

      // If one or more campaign variables is in query string, set cookies.
      if (doTrack) {
        var i = 0;
        for (; i < campaignVariableCount; i++) {
          var campaignVariable = campaignVariables[i];
          if (queryParams.hasOwnProperty(campaignVariable)) {
            $.cookie(campaignVariable, queryParams[campaignVariable], { expires: 30, path: '/', domain: cookieDomain });
          }
          else {
            $.removeCookie(campaignVariable, { path: '/' });
          }
        }
      }

      $.each(trackedForms, function(formIndex, formID) {
        var $form = $("#" + formID);
        $.each(campaignVariables, function(fieldIndex, name) {
          var value = $.cookie(name);
          var fieldName = 'field_' + name + '[0][value]';
          $form.find("input[name='" + fieldName + "']").attr('value', value);
        });
      });

      // For gated content, add campaign query string variables to Marketo links.
      $('a.locked').each(function() {
        if (this.hostname == 'response.manh.com') {
          var searchSplit = this.search.split('?');
          var queryVars = [];
          if (searchSplit.length > 1) {
            var currentQuery = searchSplit[1].split('&');
            for (i = 0; i < currentQuery.length; i++) {
              var nameVal = currentQuery[i].split('=');
              queryVars[nameVal[0]] = nameVal[1];
            }
          }
          for (i = 0; i < campaignVariables.length; i++) {
            if (campaignVariables[i] in queryVars) {

              // Delete pre-existing value.
              delete (queryVars[campaignVariables[i]]);
            }
            var val = $.cookie(campaignVariables[i]);
            if (val != undefined) {
              queryVars[campaignVariables[i]] = val;
            }
          }

          // Build new query string from queryVars.
          var newQuery = [];
          i = 0;
          for (var key in queryVars) {
            newQuery[i++] = key + '=' + queryVars[key];
          }
          this.search = '?' + newQuery.join('&');
        }
      });
    }
  };
}(jQuery));
