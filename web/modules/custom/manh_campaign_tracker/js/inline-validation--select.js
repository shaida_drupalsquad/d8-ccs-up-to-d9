/**
 * @file
 * Inline validation functionality for Select fields.
 */

(function ($, Drupal) {
  Drupal.behaviors.inlineSelectValidationBehavior = {
    /**
     * Behavior attachment function for select element validation.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      var validateCallback = this.validate;
      var submitCallback = this.submit;
      $(context).find('select').once('inlineSelectValidation').each(function () {
        $(this).on('blur', validateCallback).on('chosen:hiding_dropdown', validateCallback);
        $(this).on('manh_prepopulate:complete', validateCallback);

        // Listen for a reset event if prepopulation or progressive profiling needs to reset the field value.
        $(this).on('manh_prepopulate:reset', function(eventObject) {
          var element = $(eventObject.target);
          Drupal.manhInlineValidation.reset(element);
        });
      });
      // Workaround: browser does not validate required "chosen" select elements.
      $(context).find('input[type=submit]').once('submitSelectValidation').each(function(){
        // Force validation of "chosen" elements on submit.
        $(this).on('click', submitCallback);
      });
    },

    /**
     * Validation callback for select fields.
     *
     * @param eventObject
     */
    validate: function (eventObject) {
      var element = $(eventObject.target);
      var value = element.val();

      // Check for chosen element.
      var chosen = $(element).nextAll('.chosen-container');
      if (chosen.length > 0) {
        element = chosen;
      }

      if (element.hasClass('required') && (value === '' || value === null || value === '_none')) {
        // Display error for missing entry in a required field.
        Drupal.manhInlineValidation.setInvalid(element, Drupal.t('This field is required.'));
      }
      else {
        // Display a successful validation of the entered value.
        Drupal.manhInlineValidation.setValid(element);
      }
    },

    /**
     * Submit callback to ensure select elements are not empty.
     *
     * @param eventObject
     */
    submit: function (eventObject) {
      // Get all required "chosen" elements.
      var chosen = $(eventObject.target).closest('form').find('.chosen-container.required');
      for (var i = 0; i < chosen.length; i++) {
        // Get value from related select element.
        var select = $(chosen[i]).closest('.form-type-select').find('select');
        if (select.length > 0) {
          var value = select.val();
          if (value === '' || value === null || value === '_none') {
            // Scroll to the affected element and display "required" message underneath it.
            var element = $(chosen[i]);
            Drupal.manhInlineValidation.setInvalid(element, Drupal.t('This field is required.'));
            $('html, body').animate({
              scrollTop: element.position().top
            }, 700);
            // Stop processing submit.
            return false;
          }
        }
      }
    }
  }
}(jQuery, Drupal));

