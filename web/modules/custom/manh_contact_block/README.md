This module provides the following functionality:

* Puts the default contact form into a block.
* Moves the current language's host country to the top of the country list
  dropdown for certain countries.
* Redirects to a confirmation page after submitting the contact form.

