<?php

/**
 * @file
 * Contains Drupal\manh_contact_block\Plugin\Block\ContactBlock.
 */

namespace Drupal\manh_contact_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\contact\Controller\ContactController;

/**
 * Provides a 'ContactBlock' block.
 *
 * @Block(
 *  id = "manh_contact_block",
 *  admin_label = @Translation("MANH contact_block"),
 * )
 */
class ContactBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    /*
    @TODO: Allow selecting which contact form to display.
    $options = array();
    $form['contact_form'] = array(
      '#type' => 'select',
      '#title' => $this->t('Contact Form'),
      '#description' => $this->t('Select the form to put in this block.'),
      '#options' => $options,
      '#default_value' => isset($this->configuration['contact_form']) ? $this->configuration['contact_form'] : '',
    );
    */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['contact_form'] = $form_state->getValue('contact_form');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /*
    @TODO: Make this load a specific form.
    $entity_manager = \Drupal::service('entity.manager');
    $contact_form = $entity_manager->getStorage('contact_form')->load('personal');
    */

    // Get the services needed for the controller.
    $renderer = \Drupal::service('renderer');

    // Load the controller from the core Contact module.
    $contact_controller = new ContactController($renderer);

    // Grab the default contact form.
    // Note: To get a specific form, pass it to this function. See to do above.
    $form = $contact_controller->contactSitePage();

    return $form;
  }

}
