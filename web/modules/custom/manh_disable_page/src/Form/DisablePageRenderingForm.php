<?php

/**
 * @file
 * Contains Drupal\manh_disable_page\Form\DisablePageRenderingForm.
 */

namespace Drupal\manh_disable_page\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Returns responses for Disabling Page Rendering module routes.
 */
class DisablePageRenderingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manh_disable_page_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('manh_disable_page.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['manh_disable_page.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = [];

    $options = array();
    foreach (\Drupal::entityManager()->getBundleInfo('node') as $bundle => $info) {
      $options[$bundle] = $info['label'];
    }
    $default_values = \Drupal::config('manh_disable_page.settings')->get('manh_disable_page_enabled');

    $form['manh_disable_page_enabled'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Disable Page Rendering'),
      '#options' => $options,
      '#default_value' => $default_values,
    );

    return parent::buildForm($form, $form_state);
  }

}
