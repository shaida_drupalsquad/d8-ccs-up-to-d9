<?php
/**
 * Implements Drupal\manh_fast404\EventSubscriber\ManhFast404EventSubscriber.
 */
namespace Drupal\manh_fast404\EventSubscriber;

use Drupal\Core\Database\Database;
use Drupal\fast404\EventSubscriber\Fast404EventSubscriber;
use Drupal\fast404\Fast404;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Intercepts requests to check for 404 conditions with minimal processing.
 */
class ManhFast404EventSubscriber extends Fast404EventSubscriber implements EventSubscriberInterface {
  /**
   * Ensures Fast 404 output returned if applicable.
   */
  public function onKernelRequest(GetResponseEvent $event) {
    // Let the parent class peform its checks first.
    parent::onKernelRequest($event);

    // If the parent hasn't already sent a 404 response, this code will run.
    $request = $this->requestStack->getCurrentRequest();

    // Get the path from the request.
    $path = $request->getPathInfo();
    $fast_404 = new Fast404($request);

    // Ignore calls to the homepage to avoid unnecessary processing.
    if (!isset($path) || $path == '/') {
      return;
    }

    // Check for a language path prefix.
    $lang_negotiation_url_info = \Drupal::config('language.negotiation')->get('url');
    if (!empty($lang_negotiation_url_info['source']) && $lang_negotiation_url_info['source'] == 'path_prefix') {
      // Separate the language path prefix if it exists.
      $pos = strpos($path, '/', 1);
      if ($pos !== FALSE) {
        $prefix = substr($path, 1, $pos - 1);
      }
      else {
        $prefix = substr($path, 1);
      }
      // If this string is one of the configured language prefixes, ignore it.
      if (in_array($prefix, $lang_negotiation_url_info['prefixes'])) {
        if ($pos !== FALSE) {
          $path = substr($path, $pos);
        }
        else {
          // This path is the front page for a language prefix.
          return;
        }
      }
    }

    $path_parts = explode('/', $path);

    // Check for resource path with non-matching URL alias.
    if ($this->isResourcePath($path_parts)) {
      // If path is not a valid URL alias, 404.
      $sql = "SELECT id FROM {path_alias} WHERE alias = :path";
      $alias_result = Database::getConnection()
        ->query($sql, array(':path' => $path))
        ->fetchField();
      if (!$alias_result) {
        // If redirect doesn't exist, 404.
        // Alias path record has beginning '/' while redirect does not.
        $redirect_path = substr($path, 1);
        $sql = "SELECT rid FROM {redirect} WHERE redirect_source__path = :path";
        $redirect_result = Database::getConnection()
          ->query($sql, [':path' => $redirect_path])
          ->fetchField();
        if (!$redirect_result) {
          $event->setResponse($fast_404->response(TRUE));
        }
      }
    }

    // Check for nonexistent node ID.
    if (count($path_parts) >= 3 && $path_parts[1] == 'node' && is_numeric($path_parts[2])) {
      $sql = "SELECT nid FROM {node} WHERE nid = :nid";
      $result = Database::getConnection()->query($sql, array(':nid' => $path_parts[2]))->fetchField();
      if (!$result) {
        $event->setResponse($fast_404->response(TRUE));
      }
    }
  }

  /**
   * Returns TRUE if the path matches a typical press release URL alias.
   *
   * @param array $path_parts
   *   Exploded Drupal path.
   *
   * @return bool
   *   TRUE if path matches a press release pattern, FALSE if not.
   */
  private function isResourcePath($path_parts) {
    if (count($path_parts) >= 5) {
      return ($path_parts[1] == 'resources'
        || $path_parts[1] == 'recursos'
        || $path_parts[1] == 'ressources'
        || $path_parts[1] == 'bronnen');
    }

    return FALSE;
  }
}