<?php
/**
 * @file
 * GatedContentProcessor object for an Article node.
 */

namespace Drupal\manh_gated_content;

class ArticleGatedContentProcessor extends DocumentGatedContentProcessor implements GatedContentProcessorInterface {
  // Currently, Article attachments are treated exactly like Documents.
  // Using a separate class in case treatment needs to change later.
}