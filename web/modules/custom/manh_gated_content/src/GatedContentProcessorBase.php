<?php
/**
 * @file
 * Base class for gated content processors.
 */

namespace Drupal\manh_gated_content;


use Drupal\Component\Render\HtmlEscapedText;
use Drupal\contact\Controller\ContactController;
use Drupal\Core\Entity\ContentEntityInterface;

// @todo: Refactor to include DI, MA-545.
abstract class GatedContentProcessorBase implements GatedContentProcessorInterface {
  /**
   * The entity being examined for gated content.
   * @var ContentEntityInterface
   */
  protected $entity;

  /**
   * Constructor.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  public function __construct(ContentEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function isGated() {
    if ($this->entity->hasField('field_gated') && $this->entity->hasField('field_gate_id')) {
      $gated = $this->entity->field_gated->value;
      $gate_id = $this->entity->field_gate_id->value;

      return ($gated && !empty($gate_id));
    }
    return FALSE;
  }

  protected function getGateHash() {
    $gate_id = $this->getGateId();
    if (!empty($gate_id)) {
      $gate_hash = md5($gate_id);
    }
    else {
      $gate_hash = NULL;
    }

    return $gate_hash;
  }

  /**
   * Compares passed-in gate hash to 'id' query parameter.
   *
   * @param string $gate_hash
   *   The gate hash code to match.
   *
   * @return boolean
   *   TRUE if $gate_hash matches 'id' query parameter.
   */
  protected static function isGatePassed($gate_hash){
    // Check the URL query arguments for a hash id.
    $hash_id = \Drupal::request()->query->get('id');
    if (!empty($hash_id)) {
      return ($hash_id == $gate_hash);
    }
    return FALSE;
  }

  /**
   * Generates a gate form URL based on the gate_id and langcode.
   *
   * @param string $gate_id
   * @param string $langcode
   * @return string
   */
  protected static function generateGateFormUrl($gate_id, $langcode) {
    // Default values.
    $domain = \Drupal::service('langcode_override.manager')->getOverriddenLanguageCode($langcode);
    $gate_form_url = 'http://response.manh.com/content-gate-2015-' . $domain;

    // Check if any alternate values are defined in configuration.
    $config = \Drupal::config('manh_gated_content.settings');
    if (!empty($config)) {
      $urls = $config->get('gate_form_urls');
      if (!empty($urls[$langcode])) {
        $gate_form_url = $urls[$langcode];
      }
    }

    $url = $gate_form_url . '?docid=' . $gate_id . '&domain=' . $domain;

    return $url;
  }

  /**
   * Return the gate ID.
   *
   * @return null|string gate ID.
   */
  protected function getGateId() {
    if ($this->entity->hasField('field_gate_id')) {
      $fields = $this->entity->getFields();
      $gate_id = $fields['field_gate_id']->getValue();
      if (!empty($gate_id[0]['value'])) {
        return $gate_id[0]['value'];
      }
    }
    return NULL;
  }

  /**
   * Creates the sidebar lazy builder and placeholder.
   *
   * @param array $build
   * @param string $langcode
   * @param string $gate_id
   * @param string $gate_hash
   */
  protected function addSidebarPlaceholder(&$build, $langcode, $gate_id, $gate_hash) {
    $sidebar_callback = '\Drupal\manh_gated_content\GatedContentProcessorBase::sidebarPostRenderCallback';
    if (!empty($build['field_gated_form_title']['0']['#context']['value'])) {
      $gated_form_title = $build['field_gated_form_title']['0']['#context']['value'];
    }
    else {
      $gated_form_title = NULL;
    }

    if (!empty($build['field_gated_form_desc'][0])) {
      $gated_form_desc = render($build['field_gated_form_desc'])->jsonSerialize();
    }
    else {
      $gated_form_desc = NULL;
    }

    if (!empty($build['field_gated_form_passed_title']['0']['#context']['value'])) {
      $gated_form_passed_title = $build['field_gated_form_passed_title']['0']['#context']['value'];
    }
    else {
      $gated_form_passed_title = NULL;
    }

    if (!empty($build['field_gated_form_passed_desc'][0])) {
      $gated_form_passed_desc = render($build['field_gated_form_passed_desc'])->jsonSerialize();
    }
    else {
      $gated_form_passed_desc = NULL;
    }

    if (!empty($build['field_gated_form_submit_text']['0']['#context']['value'])) {
      $submit_text = $build['field_gated_form_submit_text']['0']['#context']['value'];
    }
    else {
      $submit_text = (string) t('Submit');
    }

    $top_left_content = '';
    if (!empty($build['field_content_top_right'][0])) {
      $top_left_content .= render($build['field_content_top_right'])->jsonSerialize();
    }
    if (!empty($build['field_view_top_right'][0])) {
      $top_left_content .= render($build['field_view_top_right'])->jsonSerialize();
    }
    if (!empty($build['field_block_top_right'][0])) {
      $top_left_content .= render($build['field_block_top_right'])->jsonSerialize();
    }

    // Define asset type.
    // Use the bundle label unless it's a document when the doc type field
    // should be used.
    if ($build['#node']->bundle() == 'document') {
      $asset_type = $build['#node']->field_doc_type->value;
    } else {
      $asset_type = $build['#node']->type->entity->label();
    }

    $node_title = $build['#node']->field_page_title->value;
    $build['sidebar_placeholder'] = [
      '#lazy_builder' => [$sidebar_callback, [
          'langcode' => $langcode,
          'gate_id' => $gate_id,
          'gate_hash' => $gate_hash,
          'asset_name' => $node_title,
          'asset_type' => $asset_type,
          'gated_form_title' => $gated_form_title,
          'gated_form_desc' => $gated_form_desc,
          'gated_form_passed_title' => $gated_form_passed_title,
          'gated_form_passed_desc' => $gated_form_passed_desc,
          'submit_text' => $submit_text,
          'top_left_content' => $top_left_content,
        ]
      ],
      '#create_placeholder' => TRUE,
    ];
  }

  /**
   * Creates the link placeholder for gated content.
   * 
   * @param array $build
   * @param string $langcode
   * @param string $gate_id
   * @param string $gate_hash
   */
  protected function addLinkPlaceholder(&$build, $langcode, $gate_id, $gate_hash) {
    $link_callback = '\Drupal\manh_gated_content\GatedContentProcessorBase::linkPostRenderCallback';
    if (!empty($build['field_contact_link']['0']['#url'])) {
      $contact_url = $build['field_contact_link']['0']['#url']->toString();
    }
    else {
      $contact_url = NULL;
    }

    if (!empty($build['field_contact_text']['0']['#context']['value'])) {
      $contact_text = $build['field_contact_text']['0']['#context']['value'];
    }
    else {
      $contact_text = NULL;
    }

    $build['link_placeholder'] = [
      '#lazy_builder' => [$link_callback, [
        'langcode' => $langcode,
        'gate_id' => $gate_id,
        'gate_hash' => $gate_hash,
        'contact_url' => $contact_url,
        'contact_text' => $contact_text,
      ]
      ],
      '#create_placeholder' => TRUE,
    ];
  }
  
  /**
   * Post-render-cache callback: markup for the gated form sidebar area.
   */
  public static function sidebarPostRenderCallback($langcode, $gate_id, $gate_hash, $asset_name, $asset_type,
                                                   $gated_form_title, $gated_form_desc, $gated_form_passed_title,
                                                   $gated_form_passed_desc, $submit_text, $top_left_content) {
    $replace = [];
    if (self::isGatePassed($gate_hash)) {
      // Render gated form passed title and description.
      $replace = [
        '#theme' => 'sidebar_gate_passed',
        '#title' => $gated_form_passed_title,
        '#description' => $gated_form_passed_desc,
        '#top_left_content' => $top_left_content,
      ];
    }
    else {
      // Load the gated_content contact form.
      $renderer = \Drupal::service('renderer');
      $contact_controller = new ContactController($renderer);
      $gated_content_form_entity = \Drupal::entityTypeManager()
        ->getStorage('contact_form')
        ->load('gated_content');
      $form = $contact_controller->contactSitePage($gated_content_form_entity);
      if (!empty($form)) {
        // Set gate ID and asset name in hidden fields.
        $form['field_document_id']['widget'][0]['value']['#value'] = $gate_id;
        $form['field_asset_name']['widget'][0]['value']['#value'] = $asset_name;
        $form['field_asset_type']['widget'][0]['value']['#value'] = $asset_type;
        $form['field_reference']['widget'][0]['value']['#value'] = $gate_hash;
        $form['field_country']['widget']['#options']['_none'] = t('- Select One -');
        $form['field_have_project']['widget']['#options']['_none'] = t('- Select One -');
        $form['field_have_budget']['widget']['#options']['_none'] = t('- Select One -');
        $form['field_project_timeline']['widget']['#options']['_none'] = t('- Select One -');

        // Update submit text.
        $form['actions']['submit']['#value'] = new HtmlEscapedText($submit_text);

        // Render gated form title, description, and gate form.
        $replace = [
          '#theme' => 'sidebar_gated_form',
          '#title' => $gated_form_title,
          '#description' => $gated_form_desc,
          '#gate_form_embed' => render($form),
        ];
      }
    }

    return $replace;
  }

  /**
   * Post-render-cache callback to alter markup for gated content contact link.
   */
  public static function linkPostRenderCallback($langcode, $gate_id, $gate_hash, $contact_url, $contact_text) {
    if (self::isGatePassed($gate_hash) && !empty($contact_url) && !empty($contact_text)) {
      // Create a contact link.
      $replace = [
        '#contact_url' => $contact_url,
        '#contact_text' => $contact_text,
        '#theme' => 'contact_link',
      ];
    }
    else {
      // Need some markup, or else the node template logic will fail.
      $replace = [
        '#type' => 'markup',
        '#markup' => '<span class="empty"></span>',
      ];
    }

    return $replace;
  }

}
