<?php
/**
 * @file
 * Factory for GatedContentProcessor objects.
 */

namespace Drupal\manh_gated_content;


use Drupal\Core\Entity\ContentEntityInterface;

class GatedContentProcessorFactory {
  /**
   * Return a gated content processor object for a content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being checked for gated content.
   *
   * @return GatedContentProcessorInterface
   */
  public static function getGatedContentProcessor(ContentEntityInterface $entity) {
    if ($entity->getEntityTypeId() == 'node') {
      /** @var \Drupal\node\NodeInterface $entity */
      if ($entity->getType() == 'document') {
        return new DocumentGatedContentProcessor($entity);
      }
      elseif ($entity->getType() == 'article') {
        return new ArticleGatedContentProcessor($entity);
      }
      elseif ($entity->getType() == 'video') {
        return new VideoGatedContentProcessor($entity);
      }
      elseif ($entity->getType() == 'webinar') {
        return new WebinarGatedContentProcessor($entity);
      }
      elseif ($entity->getType() == 'interactive') {
        return new InteractiveGatedContentProcessor($entity);
      }
    }
    return NULL;
  }
}
