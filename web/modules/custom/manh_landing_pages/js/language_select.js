/**
 * @file
 * Causes language select list to behave like a dropdown menu.
 */
(function ($, Drupal) {
  Drupal.behaviors.manhLandingPagesLanguageSelect = {
    attach : function(context) {
      $('select.manh-landing-pages-language-select').each(function(){
        var chosen = $(this).siblings('div.chosen-container');
        if (chosen.length == 0) {
          $(this).chosen(
            {
              disable_search: true,
              width: '95%' // Necessary to trigger chosen on hidden select element.
            }
          ).on('change', function () {
            var url = $(this).val();
            if (url) {
              window.location.href = url;
            }
          });
        }
      });
    }
  };
}(jQuery, Drupal));