<?php
/**
 * @file
 * Contains \Drupal\manh_langcode_override\OverrideManager.
 */

namespace Drupal\manh_langcode_override;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class OverrideManager {

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service to identify language codes from.
   */
  protected $languageManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service to load config values from.
   */
  protected $config;

  /**
   * Creates the OverrideManager service object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory) {
    $this->languageManager = $language_manager;
    $this->config = $config_factory;
  }

  /**
   * Get the overridden language codes.
   *
   * @return array
   *   Array keyed by Drupal language code. Value is the overridden code.
   */
  public function getLanguageCodeOverrides() {
    $config = $this->config->get('language.negotiation');
    $url_settings = $config->get('url');

    // Overrides will match path prefix settings (client requirement).
    if (!empty($url_settings['source']) && $url_settings['source'] == 'path_prefix') {
      $overrides = $url_settings['prefixes'];
      return $overrides;
    }

    return [];
  }

  /**
   * Get the overridden value for a language code.
   *
   * @param string $langcode
   *   (Optional) The language code to check for an overridden value. If not
   *   provided the current language code is used.
   *
   * @return string
   *   The overridden value. If not overridden, the original value is returned.
   */
  public function getOverriddenLanguageCode($langcode = NULL) {
    // Use the current language code by default.
    if (is_null($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
    }

    $overrides = $this->getLanguageCodeOverrides();
    if (!empty($overrides[$langcode])) {
      return $overrides[$langcode];
    }

    return $langcode;
  }

}
