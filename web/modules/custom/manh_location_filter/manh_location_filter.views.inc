<?php

/**
 * @file
 * Contains manh_location_filter.views.inc.
 */

/**
 * Implements hook_views_data_alter().
 */
function manh_location_filter_views_data_alter(array &$data) {
  $data['node_field_data']['locations'] = array(
    'title' => t('Locations'),
    'filter' => array(
      'title' => t('Locations'),
      'help' => t('Chose an office location from a dropdown list.'),
      'field' => 'nid',
      'id' => 'manh_location_filter_locations',
    ),
  );
}
