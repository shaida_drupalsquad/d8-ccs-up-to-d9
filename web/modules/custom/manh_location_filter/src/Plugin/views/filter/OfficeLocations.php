<?php

/**
 * @file
 * Definition of Drupal\manh_location_filter\Plugin\views\filter\OfficeLocations.
 */

namespace Drupal\manh_location_filter\Plugin\views\filter;

use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;

/**
 * Filters by given list of office locations.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("manh_location_filter_locations")
 */
class OfficeLocations extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Locations');
    $this->definition['options callback'] = array($this, 'generateOptions');
  }

  /**
   * Skip query altering if no options have been chosen.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation if no options have been chosen.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Generate the list of locations for the dropdown.
   *
   * @return array $locations
   *   An array of office location titles keyed by node ID.
   */
  public function generateOptions() {
    $language = \Drupal::languageManager()->getCurrentLanguage();
    $langcode = $language->getId();
    $default_offices = \Drupal::config('manh_location_filter.settings')->get('manh_location_filter_default_office');
    $default_nid = isset($default_offices[$langcode]) ? $default_offices[$langcode] : NULL;

    // Get a list of published offices.
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'office', '=', $langcode)
      ->condition('status', 1, '=', $langcode)
      ->sort('title');

    // Load the office data.
    $nids = $query->execute();
    $nodes = Node::loadMultiple($nids);

    // Create an array of office names keyed by the NID.
    $locations = array();
    $default = NULL;
    foreach ($nodes as $nid => $node) {
      $translation = \Drupal::service('entity.repository')->getTranslationFromContext($node);
      if ($nid == $default_nid) {
        $default = $translation->getTitle();
      }
      else {
        $locations[$nid] = $translation->getTitle();
      }
    }
    
    // Prepend the default location.
    if (!empty($default)) {
      $locations = [$default_nid => $default] + $locations;
    }

    return $locations;
  }

}
