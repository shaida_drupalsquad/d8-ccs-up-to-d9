<?php

namespace Drupal\manh_marketo\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\manh_marketo\FormSubmission;
use Drupal\marketo_ma\Lead;

class LeadSyncedEvent extends Event {

  const LEAD_SYNCED = 'manh_marketo.lead_synced';

  protected $formSubmission;

  protected $lead;

  protected $responseData;

  /**
   * Constructs a LeadSyncedEvent.
   *
   * @param Drupal\manh_marketo\FormSubmission $form_submission
   *   The form submission object.
   * @param Drupal\marketo_ma\Lead $lead
   *   The Marketo Lead object.
   * @param array $response_data
   *   The response data from Marketo.
   */
  public function __construct(FormSubmission $form_submission, Lead $lead, array $response_data) {
    $this->formSubmission = $form_submission;
    $this->lead = $lead;
    $this->responseData = $response_data;
  }

  /**
   * Getter for formSubmission.
   *
   * @return Drupal\manh_marketo\FormSubmission
   *   Returns a FormSubmission object.
   */
  public function getFormSubmission() {
    return $this->formSubmission;
  }

  /**
   * Getter for lead.
   *
   * @return Drupal\marketo_ma\Lead
   *   Returns the Lead object.
   */
  public function getLead() {
    return $this->lead;
  }

  /**
   * Getter for the lead's id.
   *
   * @return int
   *   Returns the Lead ID.
   */
  public function getLeadId() {
    return $this->lead->id();
  }

  /**
   * Getter for responseData.
   *
   * @return array
   *   Returns an array of the response data.
   */
  public function getResponseData() {
    return $this->responseData;
  }

}
