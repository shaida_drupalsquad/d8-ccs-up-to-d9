<?php

namespace Drupal\manh_marketo\EventSubscriber;

use Drupal\manh_marketo\Event\LeadSyncedEvent;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to LeadSyncedEvent::LEAD_SYNCED event.
 *
 * Trigger a Marketo campaign using the data submitted to the form.
 */
class CampaignRequestSubscriber implements EventSubscriberInterface {

  /**
   * The Marketo MA API Client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * Creates a new CampaignRequestSubscriber instance.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $marketo_ma_api_client
   *   The Marketo MA API Client.
   */
  public function __construct(MarketoMaApiClientInterface $marketo_ma_api_client) {
    $this->marketoMaApiClient = $marketo_ma_api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LeadSyncedEvent::LEAD_SYNCED][] = ['campaignRequest'];
    return $events;
  }

  /**
   * Trigger a campaign in Marketo.
   *
   * Called whenever the LeadSyncedEvent::LEAD_SYNCED event is dispatched.
   *
   * @param Drupal\manh_marketo\Event\LeadSyncedEvent $event
   *   A LeadSyncedEvent object.
   */
  public function campaignRequest(LeadSyncedEvent $event) {
    $response = $this->marketoMaApiClient->requestCampaign(
      $event->getFormSubmission()->get('campaign_id'),
      $event->getLeadId()
    );

    // Record response data from trigerring the campaign.
    $event->getFormSubmission()->addApiCall(
      'request_campaign',
      $response
    );

  }

}
