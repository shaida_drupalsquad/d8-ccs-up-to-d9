<?php

namespace Drupal\manh_marketo\EventSubscriber;

use Drupal\contact\Entity\Message;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\manh_marketo\Event\LeadSyncedEvent;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to LeadSyncedEvent::LEAD_SYNCED event.
 *
 * Also create a Marketo custom object using the data submitted to the form.
 */
class SubmissionStatusSubscriber implements EventSubscriberInterface {

  /**
   * The Marketo MA Service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The Entity Type Manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new ContactMessageInsert instance.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The Marketo MA Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(MarketoMaServiceInterface $marketo_ma_service, EntityTypeManagerInterface $entityTypeManager) {
    $this->marketoMaService = $marketo_ma_service;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LeadSyncedEvent::LEAD_SYNCED][] = ['updateSubmissionStatus'];
    return $events;
  }

  /**
   * Update the Contact Message entity submission status field.
   *
   * Called whenever the LeadSyncedEvent::LEAD_SYNCED event is dispatched.
   *
   * @param Drupal\manh_marketo\Event\LeadSyncedEvent $event
   *   A LeadSyncedEvent object.
   */
  public function updateSubmissionStatus(LeadSyncedEvent $event) {
    /* @var \Drupal\manh_marketo\FormSubmission */
    $form_submission = $event->getFormSubmission();

    // Set the FormSubmission object submission status.
    $submission_data = $form_submission->getSubmissionData();
    $submission_data['submission_data'] = $form_submission->isComplete() ? "1" : "0";
    $form_submission->setSubmissionData($submission_data);

    // Get the Message entity ID.
    $message_id = $submission_data['id'][0]['value'];
    // Set the Message entity object submission status field.
    $message_entity = Message::load($message_id);
    $message_entity->field_submission_status->value = $form_submission->isComplete() ? "1" : "0";
    // Save the Message entity.
    $message_entity->save();

    // If this FormSubmission did successfully complete all of the API calls,
    // put it into the marketo_failed_submissions queue.
    if (!$form_submission->isComplete()) {
      // Create a new standard object and set some attributes that we will need
      // as we rerun queued submissions.
      $data = new \stdClass();
      $data->leadId = $form_submission->get('lead_id');
      $data->campaignId = $form_submission->get('campaign_id');
      $data->cookie = $form_submission->get('cookie');
      $data->email = $form_submission->getSubmissionData()['field_email'][0]['value'];
      $data->messageId = $message_id;
      $data->apiCalls = $form_submission->getApiCalls();
      $data->mappedData = $form_submission->getMappedData();

      \Drupal::queue('marketo_failed_submissions')->createItem($data);
    }
  }

}
