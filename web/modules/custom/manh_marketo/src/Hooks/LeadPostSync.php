<?php

namespace Drupal\manh_marketo\Hooks;

use CSD\Marketo\Response;
use Drupal\contact\MessageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This is an extended implementation of the marketo_ma_contact insert hook.
 *
 * This hook implements the same behavior with the exception of submitting the
 * data as a custom object instead of syncing it to a Lead object.
 */
class LeadPostSync extends \Drupal\marketo_ma_contact\Hooks\ContactMessageInsert {

  /**
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $apiClient;

  /**
   * @var string
   *   The object identifier being targeted for synchronization in Marketo.
   */
  protected $targetObject = 'webForms_c';

  /**
   * Creates a new LeadPostSync instance.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $apiClient
   */
  public function __construct(MarketoMaServiceInterface $marketo_ma_service, EntityTypeManagerInterface $entityTypeManager, MarketoMaApiClientInterface $apiClient) {
    $this->marketo_ma_service = $marketo_ma_service;
    $this->entityTypeManager = $entityTypeManager;
    $this->apiClient = $apiClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('marketo_ma'),
      $container->get('entity_type.manager'),
      $container->get('marketo_ma.api_client')
    );
  }

  /**
   * Implements hook_marketo_ma_lead_post_sync().
   *
   * @see manh_marketo_marketo_ma_lead_post_sync()
   */
  public function leadPostSync(MessageInterface $message, Response $response, Lead $lead) {
    if ($tracking_enabled = $this->isTrackingEnabled($message->bundle())) {
      $data = $this->determineMappedData($message);
      $result = $response->getResult();

      // Ensure the Lead ID is clearly identified.
      if (isset($result[0]) && isset($result[0]['id'])) {
        $data['leadID'] = $result[0]['id'];
      }

      $result = $this->apiClient->syncCustomObject(
        $this->targetObject,
        'createOnly',
        [$data]
      );
    }
  }

}
