<?php

namespace Drupal\manh_marketo\Plugin\QueueWorker;

use Drupal\contact\Entity\Message;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\manh_marketo\Service\SubmissionHandler;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates Marketo lead.
 *
 * @QueueWorker(
 *   id = "marketo_failed_submissions",
 *   title = @Translation("Marketo Failed Submissions"),
 *   cron = {"time" = 15}
 * )
 */
class MarketoFailedSubmissions extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The object identifier being targeted for synchronization in Marketo.
   *
   * @var string
   */
  protected $targetObject = 'webForms_c';

  /**
   * The Marketo MA API Client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $apiClient;

  /**
   * The Marketo MA Service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The submission handler service.
   *
   * @var \Drupal\manh_marketo\Service\SubmissionHandler
   */
  protected $submissionHandler;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The period of time that must pass before an item may be re-executed.
   * The black out period is 1200 seconds or 20 minutes.
   *
   * @const int
   */
  const BLACKOUT_PERIOD = 1200;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $api_client
   *   The marketo API client.
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The marketo service.
   * @param \Drupal\manh_marketo\Service\SubmissionHandler $submission_handler
   *   The submission handler service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MarketoMaApiClientInterface $api_client = NULL, MarketoMaServiceInterface $marketo_ma_service = NULL, SubmissionHandler $submission_handler, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->apiClient = $api_client;
    $this->marketoMaService = $marketo_ma_service;
    $this->submissionHandler = $submission_handler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('marketo_ma.api_client'),
      $container->get('marketo_ma'),
      $container->get('manh_marketo.submission_handler'),
      $container->get('logger.channel.manh_marketo')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // @TODO: Refactor this to prevent it from hitting API limits. In the
    // meantime, exit early to prevent the queue from firing.
    throw new SuspendQueueException('All items have been re-queued and should be processed later.');

    // Prevent the same queue item from being re-executed within the same period
    // of time by checking if it was recently re-queued.
    if (isset($data->lastExecuted)) {
      // Has the blackout period passed to allow this item to re-execute?
      if (time() - $data->lastExecuted < self::BLACKOUT_PERIOD) {
        // Suspend further queue execution since this item has likely already
        // passed all the way through the queue.
        throw new SuspendQueueException('All items have been re-queued and should be processed later.');
      }
    }

    // Checking for the Lead ID confirms that the Lead was successfully synced
    // with Marketo. If the Lead was not successfully synced with Marketo,
    // we need to do that first. Furthermore, if the number of API calls stored
    // in $data->apiCalls, i.e. the number of API calls executed/logged for
    // this contact message submission, is not equal to the number of API calls
    // we expect to execute for each submission, we need to rerun all of the API
    // calls.
    if (isset($data->leadId) || $this->submissionHandler->getNumApiCalls() !== count($data->apiCalls)) {
      // Load the Message entity with ID $data->messageId.
      $message = Message::load($data->messageId);

      // Ensure that the Message entity with the entity id $data->messageId
      // loaded properly. There was an inadvertent scenario, which has since
      // been fixed in code, where Message entities were being deleted prior to
      // being processed in the queue. This condition would prevent them from
      // being run and throw many errors.
      if (is_null($message)) {
        // Log a warning when a Message entity cannot be loaded using the ID
        // $data->messageId and note that the submission has been removed from
        // the queue.
        $context = ['@entityId' => $data->messageId];
        $message = 'Failed to load Message entity with entity ID (@entityId). This submission has been removed from the queue.';
        $this->logger->warning($message, $context);

        // Return out of this method, which will remove this item from the
        // queue. This is necessary in the event that the Message entity with
        // $data->messageId does not exist.
        return;
      }
      else {
        // Get the FormSubmission object for the Message entity.
        $form_submission = $this->submissionHandler->createFormSubmission($message);

        // Persist the API call data.
        $form_submission->setApiCalls($data->apiCalls);
      }

      // We will iterate through each of the API calls we make to Marketo to
      // identify which API call(s) did not resolve successfully. We will run
      // the ones that did not resolve successfully.
      foreach ($data->apiCalls as $key => $api_call) {
        // If this API call failed, execute the failed API call(s).
        if ($api_call['status'] === FALSE) {
          switch ($key) {
            // Sync custom objects.
            case 'sync_custom_object':
              $this->syncCustomObject($form_submission, $data);
              break;

            // Trigger a campaign.
            case 'request_campaign':
              $this->requestCampaign($form_submission, $data);
              break;

            // Associate Lead activity.
            case 'associate_lead_activity':
              $this->associateLeadActivity($form_submission, $data);
              break;
          }
        }
      }
    }
    // Lead ID is not defined. This would happen in the scenario that the Lead
    // did not properly sync with Marketo i.e. the API call in
    // \Drupal\marketo_ma\Service\MarketoMaApi::syncLead() did not resolve
    // successfully.
    //
    // @see \Drupal\marketo_ma\Service\MarketoMaApi::syncLead()
    else {
      // Get Lead from SubmissionData email.
      $lead = $this->apiClient->getLeadByEmail($data->email);
      // Update the Lead. Subsequently, all of the the other API calls will be
      // called via the follow execution stack:
      //
      // hook_lead_post_sync() @see manh_marketo.module
      // Drupal\manh_marketo\Service\SubmissionHandler::doLeadPostSync()
      // Drupal\manh_marketo\Event\LeadSyncedEvent is dispatched
      // Drupal\manh_marketo\EventSubscriber\ObjectSyncSubscriber
      // Drupal\manh_marketo\EventSubscriber\CampaignRequestSubscriber
      // Drupal\manh_marketo\EventSubscriber\AssociateLeadSubscriber
      // Drupal\manh_marketo\EventSubscriber\SubmissionStatusSubscriber
      $this->marketoMaService->updateLead($lead);

      // Updating the Lead has failed.
      if ($this->marketoMaService->getUpdateLeadResult()) {
        // Throw an exception and try to update/create this Lead in a future
        // cron run.
        throw new \Exception("Updating the Lead has failed. Message will re-enter the queue.");
      }

      // Retrieve the FormSubmission for this process to confirm success.
      $form_submission = $this->submissionHandler->getFormSubmission();
    }

    // Confirm all API calls were successful this time around.
    $is_successful = TRUE;
    foreach ($form_submission->getApiCalls() as $api_call) {
      if ($api_call['status'] === FALSE) {
        $is_successful = FALSE;
      }
    }

    // If any of the API calls still have not resolved successfully, requeue to
    // retry later.
    if (!$is_successful) {
      // Make a copy of our data item to avoid any leftovers associated with it.
      $requeue_item = clone $data;

      // Track the current status of the API calls.
      $requeue_item->apiCalls = $form_submission->getApiCalls();

      // Update our queue data item to include the current time as the last
      // execution time. This will be used separately to ensure the same item
      // isn't processed repeatedly in the same cron execution.
      $requeue_item->lastExecuted = time();

      // Re-queue the item.
      // @todo Load this queue via DI.
      $queue = \Drupal::queue('marketo_failed_submissions');
      $queue->createItem($requeue_item);

      // Log the API failure and re-queueing.
      $context = [ '@id' => $data->messageId ];
      $this->logger->warning("Some API call(s) for message ID (@id) have failed. Re-queueing for later attempt.", $context);

      // Return to stop execution on this item and remove it from the queue.
      return;
    }

    // Load the Message entity with ID $data->messageId.
    $message = Message::load($data->messageId);

    // Ensure that the Message entity with the entity id $data->messageId loaded
    // properly. There was an inadvertent scenario, which has since been fixed
    // in code, where Message entities were being deleted prior to being
    // processed in the queue. This condition would prevent them from being run
    // and throw many errors.
    if (is_null($message)) {
      // Log a warning when a Message entity cannot be loaded using the ID
      // $data->messageId and note that the submission has been removed from the
      // queue.
      $context = ['@entityId' => $data->messageId];
      $message = 'Failed to load Message entity with entity ID (@entityId). This submission has been removed from the queue.';
      $this->logger->warning($message, $context);

      // Return out of this method, which will remove this item from the
      // queue. This is necessary in the event that the Message entity with
      // $data->messageId does not exist.
      return;
    }
    else {
      // If all of the API calls resolved successfully, update the contact form
      // Message entity. Set the field_submission_status to "1" i.e. TRUE.
      $message->field_submission_status->value = "1";
      $message->save();
    }
  }

  public function syncCustomObject($form_submission, $data) {
    // Collect the data to submit to the custom object.
    $custom_object_data = $data->mappedData;
    $custom_object_data['leadID'] = $data->leadId;

    // Make the API call and store the response.
    $response = $this->apiClient->syncCustomObject(
      $this->targetObject,
      'createOnly',
      [$custom_object_data]
    );

    // Record response data.
    $form_submission->addApiCall(
      'sync_custom_object',
      $response
    );
  }

  public function requestCampaign($form_submission, $data) {
    // Make the API call and store the response.
    $response = $this->apiClient->requestCampaign(
      $data->campaignId,
      $data->leadId
    );

    // Record response data.
    $form_submission->addApiCall(
      'request_campaign',
      $response
    );
  }

  public function associateLeadActivity($form_submission, $data) {
    // Make the API call and store the response.
    $response = $this->apiClient->associateLead(
      $data->leadId,
      $data->cookie
    );

    // Record response data.
    $form_submission->addApiCall(
      'associate_lead_activity',
      $response
    );
  }

}
