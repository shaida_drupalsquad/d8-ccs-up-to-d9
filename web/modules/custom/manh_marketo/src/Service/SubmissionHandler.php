<?php

namespace Drupal\manh_marketo\Service;

use CSD\Marketo\Response;
use Drupal\contact\MessageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\manh_marketo\Event\LeadSyncedEvent;
use Drupal\manh_marketo\FormSubmission;
use Drupal\marketo_ma\Lead;
use Drupal\marketo_ma\Service\MarketoMaServiceInterface;
use Drupal\marketo_ma\Service\MarketoMaApiClientInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a submission handler object for the Marketo-connected forms.
 *
 * @package Drupal\manh_marketo
 */
class SubmissionHandler {

  /**
   * An event dispatcher.
   *
   * @var Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The form submission object.
   *
   * @var \Drupal\manh_marketo\FormSubmission
   */
  protected $formSubmission;

  /**
   * The Marketo MA Service.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaServiceInterface
   */
  protected $marketoMaService;

  /**
   * The Marketo MA API Client.
   *
   * @var \Drupal\marketo_ma\Service\MarketoMaApiClientInterface
   */
  protected $marketoMaApiClient;

  /**
   * The Entity Type Manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The unique number of API calls we make to Marketo for a contact message.
   *
   * @var int
   */
  protected $numApiCalls = 4;

  /**
   * Constructs a SubmissionHandler object.
   *
   * @param \Drupal\marketo_ma\Service\MarketoMaServiceInterface $marketo_ma_service
   *   The Marketo MA Service.
   * @param \Drupal\marketo_ma\Service\MarketoMaApiClientInterface $marketo_ma_api_client
   *   The Marketo MA API Client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(MarketoMaServiceInterface $marketo_ma_service, MarketoMaApiClientInterface $marketo_ma_api_client, EntityTypeManagerInterface $entityTypeManager, EventDispatcherInterface $event_dispatcher) {
    $this->marketoMaService = $marketo_ma_service;
    $this->marketoMaApiClient = $marketo_ma_api_client;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Creates a FormSubmission object.
   *
   * @param Drupal\contact\MessageInterface $message
   *   The contact message object.
   *
   * @return \Drupal\manh_marketo\FormSubmission
   *   The FormSubmission object.
   */
  public function createFormSubmission(MessageInterface $message) {
    $this->formSubmission = FormSubmission::createFromMessage($message, $this->marketoMaService, $this->marketoMaApiClient, $this->entityTypeManager);
    return $this->formSubmission;
  }

  /**
   * Gets the formSubmission attribute.
   *
   * @param array $data
   *   Data that includes the message id.
   *
   * @return \Drupal\manh_marketo\FormSubmission
   *   The formSubmission attribute.
   */
  public function getFormSubmission(array $data = []) {
    if (is_null($this->formSubmission)) {
      if (!empty($data['webFormSubmissionID'])) {
        $this->setFormSubmissionByMessageId($data['webFormSubmissionID']);
      }
    }

    return $this->formSubmission;
  }

  /**
   * Set FormSubmission by Message id.
   *
   * @param $message_id
   *   Message id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setFormSubmissionByMessageId($message_id) {
    $message = $this->entityTypeManager
      ->getStorage('contact_message')
      ->load($message_id);

    if ($message) {
      // The contact form from which the message was submitted.
      $contact_form = $message->getContactForm();

      // Get the Marketo campaign ID from the Contact Form third party settings.
      $campaign_id = $contact_form->getThirdPartySetting('manh_contact_block', 'campaign_id', NULL);

      $this->formSubmission = $this->createFormSubmission($message);

      // Set the Message entity ID on the FormSubmission.
      $this->formSubmission->set('message_id', $message->id());

      // If the campaign_id is set, set it on the FormSubmission object.
      if (isset($campaign_id)) {
        $this->formSubmission->set('campaign_id', $campaign_id);
      }
    }

  }

  /**
   * Getter for the eventDispatcher attribute.
   *
   * @return Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   The eventDispatcher attribute.
   */
  public function getEventDispatcher() {
    return $this->eventDispatcher;
  }

  /**
   * Getter for the eventDispatcher attribute.
   *
   * @param Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher object.
   */
  public function setEventDispatcher(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Getter for the numApiCalls attribute.
   *
   * @return int
   *   The unique number of API calls made to Marketo for a contact message.
   */
  public function getNumApiCalls() {
    return $this->numApiCalls();
  }

  /**
   * Alter the lead data being uploaded to remove the cookie.
   *
   * If the cookie is included with the synchronization data then the cookie may
   * become associated with a known lead, but cannot be used later to merge the
   * anonymous activity into the known lead later.
   *
   * @see manh_marketo_marketo_ma_lead_alter()
   */
  public function doLeadAlter(&$data) {
    $this->getFormSubmission($data)->set('cookie', $data['cookies']);
    unset($data['cookies']);
  }

  /**
   * The LeadPostSync actions.
   *
   * @param CSD\Marketo\Response $response
   *   The Response object from Marketo.
   * @param Drupal\marketo_ma\Lead $lead
   *   The Lead from Marketo.
   *
   * @see manh_marketo_marketo_ma_lead_post_sync()
   */
  public function doLeadPostSync(Response $response, Lead $lead) {
    // Abort if there was no successful Lead ID provided.
    if (!isset($response->getResult()[0]) || !isset($response->getResult()[0]['id'])) {
      return;
    }

    $form_submission = $this->getFormSubmission();
    // Set the Lead ID on the Lead object and FormSubmission object.
    $lead_id = $response->getResult()[0]['id'];
    $lead->set('id', $lead_id);
    $form_submission->set('lead_id', $lead_id);

    // Record response data from creating or updating the Lead.
    $this->getFormSubmission()->addApiCall(
      'sync_lead',
      $response
    );

    // Instantiate a LeadSyncedEvent object.
    $lead_synced = new LeadSyncedEvent(
      $form_submission,
      $lead,
      $response->getResult()[0]
    );

    // Dispatch the LeadSyncedEvent.
    $this->eventDispatcher->dispatch(LeadSyncedEvent::LEAD_SYNCED, $lead_synced);
  }

}
