<?php
/**
 * @file
 * Contains \Drupal\manh_menu_formatter\Plugin\Field\FieldFormatter\ColumnMenuFormatter.
 */

namespace Drupal\manh_menu_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\system\Entity\Menu;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Plugin implementation of the Column Menu Formatter.
 *
 * @FieldFormatter(
 *   id = "manh_menu_formatter_column",
 *   label = @Translation("Rendered menu: Columns"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ColumnMenuFormatter extends EntityReferenceFormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    /**
     * @var Menu $entity
     */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity instanceof Menu) {
        $menu_tree = \Drupal::menuTree();
        $menu_name = $entity->id();

        // Build the menu tree.
        /** @var \Drupal\Core\Menu\MenuActiveTrailInterface $active_trail_service */
        $active_trail_service = \Drupal::getContainer()->get('menu.active_trail');
        $active_trail_ids = $active_trail_service->getActiveTrailIds($menu_name);
        $parameters = new MenuTreeParameters();
        $parameters->onlyEnabledLinks()->setActiveTrail($active_trail_ids);
        $tree = $menu_tree->load($menu_name, $parameters);
        $manipulators = array(
          array('callable' => 'menu.default_tree_manipulators:checkAccess'),
          array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
        );
        $tree = $menu_tree->transform($tree, $manipulators);
        $built_menu = $menu_tree->build($tree);

        // Add "unlinked" value to each manh_nav_drawers.non_linked_item.
        $add_class = function(&$tree) use (&$add_class) {
          foreach($tree as $key => $item) {
            if ($item['url']->isRouted() && $item['url']->getRouteName() == 'manh_nav_drawers.non_linked_item') {
              $tree[$key]['unlinked'] = TRUE;
            }
            if (!empty($item['below'])) {
              $add_class($tree[$key]['below']);
            }
          }
        };
        $add_class($built_menu['#items']);
        $built_menu['#theme'] = 'manh_menu_columnar';

        $elements[$delta] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'manh-column-menu',
            ],
          ],
          'menu' => $built_menu,
        ];
      }
      else {
        $elements[$delta] = array('#markup' => t('This is not a menu'));
      }
    }

    return $elements;
  }

  /**
  * Overrides getEntitiesToView to enforce that access is TRUE.
  *
  * @see ::getEntitiesToView()
  */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = [];

    foreach ($items as $delta => $item) {
      // Ignore items where no entity could be loaded in prepareView().
      if (!empty($item->_loaded)) {
        $entity = $item->entity;

        // Set the entity in the correct language for display.
        if ($entity instanceof TranslatableInterface) {
          $entity = \Drupal::entityManager()->getTranslationFromContext($entity, $langcode);
        }

        $access = TRUE;

        if ($access) {
          // Add the referring item, in case the formatter needs it.
          $entity->_referringItem = $items[$delta];
          $entities[$delta] = $entity;
        }
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for menu entities.
    $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    return $target_type == 'menu';
  }
}
