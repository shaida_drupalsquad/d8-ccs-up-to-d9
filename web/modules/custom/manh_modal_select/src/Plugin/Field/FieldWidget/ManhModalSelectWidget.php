<?php
/**
 * Contains \Drupal\manh_modal_select\Field\Plugin\Field\FieldWidget\ManhModalSelectWidget.
 */

namespace Drupal\manh_modal_select\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\er_modal_select\Plugin\Field\FieldWidget\EntityReferenceModalSelectWidget;
use Drupal\manh_modal_select\SelectWidget\ManhEntityQueryWidget;

/**
 * Plugin implementation of the 'manh_modal_select' widget.
 *
 * @FieldWidget(
 *   id = "manh_modal_select",
 *   label = @Translation("Entity Reference Modal Select - MANH extension"),
 *   description = @Translation("Browse and select referenced entities in a modal dialog. Includes filtering on Page Display Type."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ManhModalSelectWidget extends EntityReferenceModalSelectWidget {
  /**
   * {@inheritdoc}
   */
  public function renderModalAjaxResponse($reference_field_id, array &$form, FormStateInterface $form_state) {
    $modal_widget = new ManhEntityQueryWidget($form_state, $reference_field_id, $this->getFieldSettings());
    $modal_form = \Drupal::formBuilder()->getForm($modal_widget);
    $response = new AjaxResponse();

    // Attach the libraries we need and
    // set the attachments for this Ajax response.
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $response->setAttachments($form['#attached']);

    // Open a modal with the widget element.
    $response->addCommand(new OpenModalDialogCommand(t('Select an entity'), $modal_form, ['width' => '70%']));

    return $response;
  }
}