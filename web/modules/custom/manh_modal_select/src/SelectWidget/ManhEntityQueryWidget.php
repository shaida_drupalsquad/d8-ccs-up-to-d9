<?php
/**
 * Contains \Drupal\manh_modal_select\SelectWidget\ManhEntityQueryWidget.
 */

namespace Drupal\manh_modal_select\SelectWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\er_modal_select\SelectWidget\FilteredEntityQueryWidget;
use Drupal\field\Entity\FieldStorageConfig;

class ManhEntityQueryWidget extends FilteredEntityQueryWidget {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manh_modal_select_filtered_entity_query';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $display_filter = ['0' => t('- Any -')] + $this->getDisplayTypeOptions();
    $form['filter']['display_type'] = array(
      '#type' => 'select',
      '#title' => t('Display Type'),
      '#options' => $display_filter,
      '#default_value' => 0,
      '#ajax' => array(
        'callback' => array($this, 'updateMatches'),
        'wrapper' => 'entity-query-matches',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
    );

    return $form;
  }

  /**
   * Returns the filter options for Display Type.
   *
   * @return array
   *   Options for the Display Type filter.
   */
  private function getDisplayTypeOptions() {
    $config = FieldStorageConfig::loadByName('node', 'field_display_type');
    $settings = $config->getSettings();

    return $settings['allowed_values'];
  }

  /**
   * Returns array of entities matching the filter.
   *
   * @param string $bundle
   *   Bundle filter.
   * @param string $title_substring
   *   Title filter.
   * @param string $langcode
   *   Language code filter.
   * @param int $status
   *   Status filter.
   * @param string $display_type
   *   Display Type filter.
   *
   * @return array
   *   Array of Entity objects.
   */
  protected function getMatchingEntities($bundle = NULL, $title_substring = NULL, $langcode = NULL, $status = NULL, $display_type = NULL) {
    $entities = array();

    /** @var \Drupal\Core\Entity\Query\QueryInterface $query */
    $query = $this->buildMatchQuery($bundle, $title_substring, $langcode, $status);

    // Add display type to the query.
    if (!empty($display_type)) {
      $query->condition('field_display_type', $display_type, '=');
    }

    $ids = $query->execute();
    if (!empty($ids)) {
      $controller = \Drupal::entityTypeManager()->getStorage($this->getTargetEntityTypeId());
      $entities = $controller->loadMultiple($ids);
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMatchingEntitiesElement(FormStateInterface $form_state) {
    $bundle = $form_state->getValue('bundle');
    $title = $form_state->getValue('title');
    $langcode = $form_state->getValue('langcode');
    if (empty($langcode)) {
      // Convert the "Any" status to NULL.
      $langcode = NULL;
    }

    if ($this->isEntityStatusSupported()) {
      $status = $form_state->getValue('status');
      if ($status == -1) {
        // Convert the "Any" status to NULL.
        $status = NULL;
      }
    }
    else {
      $status = NULL;
    }

    $display_type = $form_state->getValue('display_type');
    if (empty($display_type)) {
      // Convert the "Any" status to NULL.
      $display_type = NULL;
    }

    // Set up the Type (bundle) filter, if appropriate.
    $allowed_bundles = $this->getAllowedBundles();
    if (!empty($allowed_bundles)) {
      if (count($allowed_bundles) == 1) {
        $bundle_keys = array_keys($allowed_bundles);
        $bundle = reset($bundle_keys);
      }
    }
    $matching_entities = $this->getMatchingEntities($bundle, $title, $langcode, $status, $display_type);
    $header['title'] = array('data' => t('Title'));
    if (!empty($allowed_bundles)) {
      $header['type'] = array('data' => t('Type'));
    }
    $header['lang'] = array('data' => t('Language'));
    if ($this->isEntityStatusSupported()) {
      $header['status'] = array('data' => t('Status'));
    }
    $header['display_type'] = array('data' => t('Display Type'));

    $display_type_options = $this->getDisplayTypeOptions();

    $options = array();
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    foreach ($matching_entities as $entity) {
      $translations = [];
      /** @var  \Drupal\Core\TypedData\TranslatableInterface $entity */
      if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
        $languages = $entity->getTranslationLanguages();
        foreach ($languages as $l) {
          if ($langcode == NULL || $langcode == $l->getId()) {
            $translations[] = $entity->getTranslation($l->getId());
          }
        }
      }
      else {
        $translations[] = $entity;
      }

      /** @var \Drupal\Core\Entity\EntityInterface $item */
      foreach ($translations as $item) {
        $option_key = $item->id() . ':' . $item->language()->getId();
        if (method_exists($item, 'getTitle')) {
          $options[$option_key]['title'] = $item->getTitle();
        }
        else {
          $options[$option_key]['title'] = $item->label();
        }
        if (!empty($allowed_bundles)) {
          if (method_exists($item, 'getType')) {
            $options[$option_key]['type'] = $item->getType();
          }
          else {
            $options[$option_key]['type'] = $item->bundle();
          }
        }
        $options[$option_key]['lang'] = $item->language()->getName();
        if ($this->isEntityStatusSupported()) {
          $target_type = $this->getTargetEntityTypeId();
          if ($target_type == 'node') {
            $val = $item->status->getValue();
            $options[$option_key]['status'] = $val[0]['value'] ? t('Published') : t('Unpublished');
          }
          elseif ($target_type == 'block_content') {
            $val = $item->content_translation_status->getValue();
            $options[$option_key]['status'] = $val[0]['value'] ? t('Published') : t('Unpublished');
          }
        }
        $options[$option_key]['display_type'] = '';
        if (!empty($item->field_display_type)) {
          $display_type_key = $item->field_display_type->value;
          if (!empty($display_type_key)) {
            $options[$option_key]['display_type'] = $display_type_options[$display_type_key];
          }
        }
      }
    }

    return array(
      'match' => array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $options,
        '#empty' => t('No matching items.'),
        '#multiple' => FALSE,
        '#js_select' => TRUE,
      ),
    );
  }
}