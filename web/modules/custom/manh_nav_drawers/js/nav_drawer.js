/**
 * @file
 * Nav drawer code: must be usable on both Drupal and Investor Relations site.
 */

/**
 * Default translation function: no translation.
 * For Drupal integration, override this with Drupal.t.
 */
manhNavDrawerTranslationFunction = function(originalString) {
  return originalString;
};

(function ($) {
  /**
   * Defines the desktop version of the nav drawer.
   */
  navDrawerDesktop = {
    width : 0,
    container_width : 0,
    close_timer : null,
    /**
     * Initialize the nav drawer.
     */
    initialize : function() {
      if ($('.top-navigation-desktop').hasClass('manh-processed')) {
        return;
      }
      this.setSubNavDimensions();
      this.addClasses();
      var navDrawer = this;
      var subItemNumber = 0;
      $('.top-navigation-desktop .top-items ul.menu > li a').hover(function(){
        // If this is the active link, close the drawer.
        if ($(this).hasClass('active')) {
          $('a.nav-drawer-close').hover();
          return;
        }

        navDrawer.clearActive();
        $(this).addClass('active');

        // Figure out which item number was clicked, and scroll to that item.
        var classList = $(this).attr('class').split(/\s+/);
        $.each( classList, function(index, item){
          var itemMatch = item.match(/^item-(\d+)$/);
          if (itemMatch && itemMatch.length == 2) {
            subItemNumber = itemMatch[1];
          }
        });
        var subItems = $(this).closest('.top-items').next('.sub-items');
        if (!subItems.hasClass("open")) {
          // Drawer will be opening; do not animate to sub-item.
          navDrawer.setSubItem(subItemNumber);
          subItems.addClass("open").animate({
            height: 'toggle'
          });
        }
        else {
          // Drawer already open; animate to correct sub-item.
          navDrawer.showSubItem(subItemNumber);
        }
        return false;
      });
      $('a.nav-drawer-close').click(function(){
        navDrawer.clearActive();
        if (navDrawer.close_timer != null) {
          window.clearTimeout(navDrawer.close_timer);
          navDrawer.close_timer = null;
        }
        if ($(this).closest('.sub-items').hasClass("open")) {
          $(this).closest('.sub-items').removeClass("open").animate({
            height: 'toggle'
          });
        }
      });
      $('.top-navigation-desktop').mouseleave(function(){
        if ($('.top-navigation-desktop .sub-items').hasClass('open')) {
          navDrawer.close_timer = window.setTimeout(function () {
            if ($('.top-navigation-desktop .sub-items').hasClass('open')) {
              $('.top-navigation-desktop a.nav-drawer-close').click();
            }
          }, 10);
        }
      });
      $('.top-navigation-desktop').mouseenter(function(){
        if (navDrawer.close_timer != null) {
          window.clearTimeout(navDrawer.close_timer);
          navDrawer.close_timer = null;
        }
      });
      $('.top-navigation-desktop').addClass('manh-processed');
    },

    /**
     * Set width and height of sub-nav items.
     */
    setSubNavDimensions : function() {
      this.width = $('.top-navigation-desktop').width();
      this.container_width = this.width;
      $('.top-navigation-desktop .sub-items-container').width(this.container_width);
      var navDrawer = this;
      $('.top-navigation-desktop .sub-item').each(function(){
        $(this).width(navDrawer.width);
      });
    },

    /**
     * Set classes necessary for animating sub-items.
     */
    addClasses : function() {
      // Set item-number classes to top links.
      var delta = 0;
      $('.top-navigation-desktop .top-items ul.menu li a').each(function(){
        $(this).addClass('item-' + delta);
        delta++;
      });

      // Set item-number classes to sub-items.
      delta = 0;
      $('.top-navigation-desktop .sub-item').each(function(){
        $(this).addClass('item-' + delta);
        delta++;
      });
    },

    /**
     * Show the specified submenu item (animated version).
     */
    showSubItem : function(delta) {
      var xPosition = -(this.width * delta);
      $('.top-navigation-desktop .sub-items .sub-items-container').animate({ left : xPosition });
    },

    /**
     * Show the specified submenu item (non-animated version).
     */
    setSubItem : function(delta) {
      var xPosition = -(this.width * delta);
      $('.top-navigation-desktop .sub-items .sub-items-container').css('left', xPosition);
    },

    /**
     * Clear the active state on top items.
     */
    clearActive : function() {
      $('.top-navigation-desktop .top-items ul.menu > li a').removeClass('active');
    }
  };

  /**
   * Defines the mobile version of the nav drawer.
   */
  navDrawerMobile = {
    initialize : function() {
      this.collapseAll();
      var navDrawer = this;
      var duration = 400;
      if ($('.top-navigation-mobile').hasClass('manh-processed')) {
        return;
      }

      $('.top-navigation-mobile div.menu-items > ul > li').addClass('top-item').children('ul').children('li').each(function(){
        navDrawer.addRedundantLinks($(this));
      });
      $('li.manh-menu-header > a').removeAttr('href');
      $('.top-navigation-mobile li.menu-item > a').click(function(){
        var listItem = $(this).closest('li.menu-item');
        var subMenu = listItem.children('ul.menu');
        if (subMenu.length > 0) {
          if (subMenu.hasClass("open")) {
            subMenu.removeClass('open').slideUp();
            subMenu.parent('li').removeClass('open');
            return false;
          }
          else {
            listItem.siblings('li.menu-item').children('ul.menu').removeClass('open').slideUp().parent('li').removeClass('open');
            subMenu.addClass('open').slideDown();
            var parent_li = subMenu.parent('li');
            parent_li.addClass('open');
            if (!parent_li.hasClass('top-item')) {
              // Scroll to this item.
              var parent_ul = parent_li.parent('ul');
              var ul_top = parent_ul.offset().top;
              var li_top = parent_li.offset().top;
              parent_ul.animate({
                scrollTop : li_top - ul_top
              });
            }

            return false;
          }
        }
      });
      $('.top-navigation-mobile').addClass('manh-processed')
    },

    collapseAll : function() {
      $('.top-navigation-mobile .menu-items > ul.menu ul.menu').css('display', 'none');
    },

    // For parents that need to have expand/collapse behavior, duplicate the link as a child.
    addRedundantLinks : function(parentListItem) {
      var navDrawer = this;
      var link = parentListItem.children('a');
      var list = parentListItem.children('ul');
      if (link.length > 0 && list.length > 0) {
        list.children('li').each(function(){navDrawer.addRedundantLinks($(this))});
        if (!parentListItem.hasClass('manh-menu-header')) {
          // Prepend this link to the child list.
          list.prepend('<li class="menu-item"><a href="' + link.attr('href') + '">' +
          manhNavDrawerTranslationFunction('Overview') + '</a></li>');
        }
      }
    }
  };
}(jQuery));
