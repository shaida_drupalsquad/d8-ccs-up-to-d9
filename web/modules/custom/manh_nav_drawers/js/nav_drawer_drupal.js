/**
 * @file
 * Provides Drupal behavior integration for navigation drawer javascript.
 */
(function ($, Drupal) {
  // Set the translation function.
  manhNavDrawerTranslationFunction = Drupal.t;

  Drupal.behaviors.manhNavDrawer = {
    attach : function() {
      navDrawerDesktop.initialize();
      navDrawerMobile.initialize();
    }
  };
}(jQuery, Drupal));
