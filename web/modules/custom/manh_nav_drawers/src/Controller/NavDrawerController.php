<?php
/**
 * @file
 * Contains \Drupal\manh_nav_drawers\Controller\NavDrawerController
 */

namespace Drupal\manh_nav_drawers\controller;

use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Menu\MenuTreeParameters;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class NavDrawerController extends ControllerBase {
  /**
   * For the IR site, returns HTML for all menus in a series of divs.
   */
  public function getAllMenus() {
    $rendered  = '<div id="manh-ir-site-selector" class="block-manh-site-selector">' . $this->getSiteSelector() . '</div>';
    $rendered .= '<div id="manh-ir-header">' . $this->getHeaderMenu() . '</div>';
    $rendered .= '<div id="manh-ir-drawers-desktop">' . $this->getDesktopMenu() . '</div>';
    $rendered .= '<div id="manh-ir-drawers-mobile">' . $this->getMobileMenu() . '</div>';
    $rendered .= '<div id="manh-ir-footer">' . $this->getFooterMenu() . '</div>';
    $response = Response::create($rendered);
    $response->headers->set('Access-Control-Allow-Origin', '*');
    return $response;
  }

  /**
   * Returns the desktop menu HTML markup.
   */
  public function getDesktopMenu() {
    $block_config = \Drupal::configFactory()->get('block.block.topnavigationdrawerdesktop');
    $content = manh_nav_drawers_get_desktop_content($block_config);
    $rendered = \Drupal::service('renderer')->render($content);
    // Replace internal links with absolute.
    $base_url = "https://www.manh.com/";
    $search = ['href="/', 'srcset="/', 'src="/'];
    $replace = ['href="' . $base_url, 'srcset="' . $base_url, 'src="' . $base_url];
    $rendered = str_replace($search, $replace , $rendered);

    return $rendered;
  }

  /**
   * Returns the mobile menu HTML markup.
   */
  private function getMobileMenu() {
    $mobile_blocks = \Drupal::config('manh_nav_drawers.settings')->get('mobile_blocks_ir');

    // Create render array.
    $content = [];
    foreach ($mobile_blocks as $key => $definition) {
      if (!empty($definition['type']) && !empty($definition['id'])) {
        if ($definition['type'] == 'Block') {
          $content['#' . $key] = Block::load($definition['id'])->getPlugin()->build();
        }
        elseif ($definition['type'] == 'Menu') {
          $content['#' . $key] = manh_nav_drawers_get_mobile_content($definition['id']);
        }
        elseif ($definition['type'] == 'BlockContent') {
          $entity = BlockContent::load($definition['id']);
          $content['#' . $key] = entity_view($entity, 'default');
        }
      }
    }
    $content['#theme'] = 'manh_ir_mobile_menu';
    $rendered = \Drupal::service('renderer')->render($content);

    // Replace internal links with absolute.
    $base_url = "https://www.manh.com/";
    $rendered = str_replace('href="/', 'href="' . $base_url , $rendered);
    $rendered = str_replace('action="/', 'action="' . $base_url , $rendered);

    return $rendered;
  }

  /**
   * Returns the footer menu HTML markup.
   */
  private function getFooterMenu() {
    // Load footer block IDs from configuration.
    $footer_blocks = \Drupal::config('manh_nav_drawers.settings')->get('footer_blocks_ir');

    // Create render array.
    $content = $this->loadBlocks($footer_blocks);
    $content['#theme'] = 'manh_ir_footer';
    $rendered = \Drupal::service('renderer')->render($content);

    // Replace internal links with absolute.
    $base_url = "https://www.manh.com/";
    $rendered = str_replace('href="/', 'href="' . $base_url , $rendered);

    return $rendered;
  }

  /**
   * Returns the site selector HTML markup for investor relations.
   */
  private function getSiteSelector() {
    // Load site selector block ID from configuration.
    $definition = \Drupal::config('manh_nav_drawers.settings')->get('site_selector_block_ir');

    // Create render array.
    if (!empty($definition['type']) && !empty($definition['id'])) {
      if ($definition['type'] == 'Block') {
        $content = Block::load($definition['id'])->getPlugin()->build();
      }
      elseif ($definition['type'] == 'BlockContent') {
        $entity = BlockContent::load($definition['id']);
        $content = entity_view($entity, 'default');
      }
    }
    $rendered = \Drupal::service('renderer')->render($content);

    // Replace internal links with absolute.
    $base_url = "https://www.manh.com/";
    $rendered = str_replace('href="/', 'href="' . $base_url , $rendered);

    return $rendered;
  }

  /**
   * Returns the header menu HTML markup for investor relations.
   */
  private function getHeaderMenu() {
    // Load header block IDs from configuration.
    $header_blocks = \Drupal::config('manh_nav_drawers.settings')->get('header_blocks_ir');

    // Create render array.
    $content = $this->loadBlocks($header_blocks);
    $content['#theme'] = 'manh_ir_header';
    // Remove the search autocomplete class.
    $content['#search_block']['keys']['#attributes']['class'] = [];
    // Final render.
    $rendered = \Drupal::service('renderer')->render($content);

    // Replace internal links with absolute.
    $base_url = "https://www.manh.com/";
    $rendered = str_replace('href="/', 'href="' . $base_url , $rendered);
    $rendered = str_replace('action="/', 'action="' . $base_url , $rendered);

    return $rendered;
  }

  /**
   * Load blocks from configuration settings and create render array.
   *
   * @param array $blocks
   *   Block settings from configuration.
   * @return array
   *   Render array.
   */
  private function loadBlocks($blocks) {
    $result = [];
    foreach ($blocks as $key => $definition) {
      if (!empty($definition['type']) && !empty($definition['id'])) {
        if ($definition['type'] == 'Block') {
          $result['#' . $key] = Block::load($definition['id'])->getPlugin()->build();
        }
        elseif ($definition['type'] == 'BlockContent') {
          $entity = BlockContent::load($definition['id']);
          $result['#' . $key] = entity_view($entity, 'default');
        }
      }
    }

    return $result;
  }

  /**
   * Placeholder route for non-linked menu items.
   */
  public function nonLinkedItem() {
    // For bots that attempt to crawl this link, redirect to front page.
    $dest_url = \Drupal\Core\Url::fromRoute('<front>');
    $response = new RedirectResponse($dest_url->toString(), 301);
    return $response;
  }
}
