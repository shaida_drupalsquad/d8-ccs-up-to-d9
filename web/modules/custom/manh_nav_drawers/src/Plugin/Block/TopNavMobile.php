<?php
/**
 * @file
 * Contains \Drupal\manh_nav_drawers\Plugin\Block\TopNavMobile
 */
namespace Drupal\manh_nav_drawers\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides mobile version of Top Nav block.
 *
 * @Block(
 *   id = "manh_top_nav_mobile",
 *   admin_label = @Translation("Top Navigation - Mobile"),
 *   category = @Translation("Blocks")
 * )
 */
class TopNavMobile extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $map = \Drupal::config('manh_nav_drawers.settings')->get('menu_language_map');
    $result = '';
    if (!empty($map[$lang])) {
      $result = manh_nav_drawers_get_mobile_content($map[$lang]);
    }
    else {
      $result = [
        '#type' => 'markup',
        '#markup' => t('Mobile menu not configured'),
      ];
      \Drupal::logger('manh_nav_drawers')->error('No configuration in manh_nav_drawers.settings for language: ' . $lang);
    }
    return $result;
  }
}