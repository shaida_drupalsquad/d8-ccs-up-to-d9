<?php

namespace Drupal\manh_personalization\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\manh_landing_pages\Service\PageHelper;
use Drupal\manh_langcode_override\OverrideManager;
use Drupal\node\Entity\Node;

/**
 * Helper service to identify filters for personalized content.
 */
class FilterHelper {

  /**
   * The manh page helper service to use for identifying page types.
   *
   * @var \Drupal\manh_landing_pages\Service\PageHelper $pageHelper
   */
  protected $pageHelper;

  /**
   * The storage handler for taxonomy terms for batch loading.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface $termStorageHandler
   */
  protected $termStorageHandler;

  /**
   * The language override manager service for identifying locale categories.
   *
   * @var \Drupal\manh_langcode_override\OverrideManager $languageOverrideManager
   */
  protected $languageOverrideManager;

  /**
   * Marketo string translation replacements.
   *
   * This is organized here to ease maintenance. Before it is used in
   * `translateForMarketo()` it is statically separated for use to avoid
   * unnecessary processing in each function call.
   *
   * @see \Drupal\manh_personalization\Service\FilterHelper::translateForMarketo()
   */
  const MARKETO_REPLACEMENTS = [
    '-' => ' ',
    '&' => 'and',
    '™' => '',
    ',' => '',
  ];

  /**
   * FilterHelper constructor.
   *
   * @param \Drupal\manh_landing_pages\Service\PageHelper $page_helper
   *   The page helper service to use to identify page types.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $type_manager
   *   The entity type manager service for loading the term storage handler.
   * @param \Drupal\manh_langcode_override\OverrideManager $language_override_manager
   *   The language code override service to identify language codes for filters.
   */
  public function __construct(PageHelper $page_helper, EntityTypeManagerInterface $type_manager, OverrideManager $language_override_manager) {
    $this->pageHelper = $page_helper;
    $this->languageOverrideManager = $language_override_manager;

    // Load the Term storage handler for later use.
    $this->termStorageHandler = $type_manager->getStorage('taxonomy_term');
  }

  /**
   * Get a list of filters to be applied relative to a given node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node to determine category filters for.
   *
   * @return string[]
   *   An array of category filters to be used for a given node.
   */
  public function getFiltersByNode(Node $node) {
    // Identify term fields to use by page type.
    switch ($this->pageHelper->getPageType($node)) {
      case 'product':
        $fields = [
          'field_manhattan_footprint',
        ];
        break;

      case 'industry':
        $fields = [
          'field_term_industry',
        ];
        break;

      // Intentionally pass through to default handling.
      case 'page':
      default:
        $fields = [
          'field_manhattan_footprint',
        ];
    }

    // Load terms from whitelisted fields.
    // This has been disabled to only apply language filters at this time.
    // $terms = $this->extractFieldTerms($node, $fields);

    // Map terms to Marketo categories.
    // This has been disabled to only apply language filters at this time.
    // $filters = $this->mapTermsToMarketo($terms);

    // Add the current language for filtering.
    $language = $this->languageOverrideManager->getOverriddenLanguageCode();
    $filters[] = $this->translateForMarketo(strtolower($language));

    return $filters;
  }

  /**
   * Helper function to aggregate term data from a list of fields.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node to determine filters from.
   * @param array $fields
   *   A list of field identifiers to extract term values from.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   An array of taxonomy terms to be used as content filters.
   */
  protected function extractFieldTerms(Node $node, array $fields) {
    $terms = [];

    // Load only the term IDs to defer loading of multiple terms in one batch.
    $term_ids = [];
    foreach ($fields as $field_id) {
      // Fail gracefully if this node doesn't have a field.
      if ($node->hasField($field_id)) {
        foreach ($node->get($field_id)->getValue() as $key => $value) {
          // Key by entity Id to avoid loading duplicates.
          $term_ids[$value['target_id']] = $value['target_id'];
        }
      }
    }

    // Load the complete batch of terms if any were identified.
    if (!empty($term_ids)) {
      $terms = $this->termStorageHandler->loadMultiple(array_keys($term_ids));
    }

    return $terms;
  }

  /**
   * Map taxonomy term entities to Marketo categories.
   *
   * Marketo does not support any special characters within category names, so
   * Drupal terms containing special characters have to be translated to match
   * a Marketo-specific alternative.
   *
   * @param \Drupal\taxonomy\TermInterface[] $terms
   *   An array of loaded taxonomy terms to be prepared for Marketo reference.
   *
   * @return string[]
   *   An array of Marketo-compatible category names.
   */
  protected function mapTermsToMarketo(array $terms) {
    $output = [];

    foreach ($terms as $term) {
      $output[] = $this->translateForMarketo($term->label());
    }

    return $output;
  }

  /**
   * A helper function to alter a string to match Marketo naming conventions.
   *
   * @param string $input
   *   The string to be translated to Marketo standards.
   *
   * @return string
   *   The Marketo translation of the given string with all special characters
   *   replaced.
   */
  public function translateForMarketo($input) {
    static $find;
    static $replace;

    // Only calculate this once.
    if (!isset($find)) {
      $find = array_keys($this::MARKETO_REPLACEMENTS);
      $replace = array_values($this::MARKETO_REPLACEMENTS);
    }

    return str_replace($find, $replace, $input);
  }

}
