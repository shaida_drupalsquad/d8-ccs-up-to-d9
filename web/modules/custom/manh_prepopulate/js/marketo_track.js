/**
 * @file
 * Tracks Marketo visitWebPage web activity for target links.
 */
!(function ($) {
  'use strict';

  Drupal.behaviors.marketoTrack = {
    attach: function (context, settings) {
      $('.marketo-track', context).on('click', function (e) {
        // Check for the existence of mktoMunchkinFunction during click event.
        // Can not check before b/c the munchkin.js is loaded asynchronously.
        if (typeof mktoMunchkinFunction === 'function') {
          mktoMunchkinFunction('visitWebPage', {url: $(this).attr('href')});
        }
      });
    }
  }
})(jQuery);
