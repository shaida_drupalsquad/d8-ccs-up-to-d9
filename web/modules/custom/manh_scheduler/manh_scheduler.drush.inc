<?php

/**
 * @file
 * Manh Scheduler drush commands.
 */

use Drupal\node\Entity\Node;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Implements hook_drush_command().
 */
function manh_scheduler_drush_command() {
  $items = [];

  $items['manh-bulk-resave'] = [
    'description' => dt('Trigger save on node translations with document media.'),
    'aliases' => ['ma-br'],
  ];
  $items['manh-bulk-update-entity-langcode'] = [
    'description' => dt('Update langcode for entity.'),
    'aliases' => ['ma-buel'],
    'arguments' => [
      'entity' => 'The entity type you want to update.',
      'langcode_to_remove' => 'Langcode to remove defaults to \'und\'.',
      'langcode_to_keep' => 'Langcode to keep defaults to \'en\'',
    ],
    'examples' => [
      'drush ma-buel media' => 'Update media entity by removing \'und\' langcode while keeping \'en\'.',
      'drush ma-buel node fr es' => 'Update media entity by removing \'fr\' langcode while keeping \'es\'.',
      'drush ma-buel node fr es --batch_size=20' => 'Update media entity by removing \'fr\' langcode while keeping \'es\' in batches of 20.',
    ],
    'options' => [
      'batch_size' => [
        'description' => 'The number of entities to process in a single batch request.',
        'example-value' => '20',
      ],
      'debug-message' => [
        'description' => 'Debug messaging (defaults to FALSE).',
        'example-value' => 'TRUE',
      ],
    ],
  ];

  return $items;
}

/**
 * Callback to trigger a bulk resave.
 */
function drush_manh_scheduler_manh_bulk_resave() {
  // Query nodes with document media.
  $query = \Drupal::entityQuery('node')
    ->condition('status', [0, 1], IN);

  $or_condition = $query->orConditionGroup()
    ->exists('field_bio_doc_media')
    ->exists('field_doc_media')
    ->exists('field_earnings_doc_media');

  $query->condition($or_condition);
  $nids = $query->execute();

  $operations = [];
  foreach ($nids as $nid) {
    $operations[] = [
      '_manh_scheduler_resave_batch',
      [$nid],
    ];
  }

  if ($operations) {
    $batch = [
      'title' => t('Bulk Resave...'),
      'operations' => $operations,
      'finished' => '_manh_scheduler_resave_batch_finished',
    ];
    batch_set($batch);
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    drush_backend_batch_process();
  }
}

/**
 * Determine what entity we are going to send to the batch for processing.
 *
 * @param string $entity
 *   Entity to update.
 * @param string $langcode_to_remove
 *   Language code to remove.
 * @param string $langcode_to_keep
 *  Language code to keep.
 */
function drush_manh_scheduler_manh_bulk_update_entity_langcode($entity = '', $langcode_to_remove = 'und', $langcode_to_keep = 'en') {
  $entity_list = ['node', 'term', 'media', 'block', 'taxonomy_term'];

  if (empty($entity) || !in_array($entity, $entity_list)) {
    drush_print(dt( "Please enter a valid entity type: @entity_list.", ['@entity_list' => implode(', ', $entity_list)]));
    return;
  }

  switch ($entity) {
    case 'node':
      $storage = 'node';
      break;
    case 'media':
      $storage = 'media';
      break;
    case 'block':
      $storage = 'block_content';
      break;
    case 'taxonomy_term':
    case 'term':
      $storage = 'taxonomy_term';
      break;
  }

  $entity_type_manager = \Drupal::service('entity_type.manager');
  $entity_storage = $entity_type_manager->getStorage($storage);
  $ids = $entity_storage
    ->getQuery()
    ->condition('langcode', $langcode_to_remove)
    ->execute();

  if (empty($ids)) {
    drush_print(dt( "No matches for entity: @entity with langcode: @langcode_remove.", ['@entity' => $entity, '@langcode_remove' => $langcode_to_remove]));
    return;
  }
  else {
    drush_print(dt( "@matches matches for entity: @entity with langcode: @langcode_remove.", ['@matches' => count($ids), '@entity' => $entity, '@langcode_remove' => $langcode_to_remove]));
  }

  $items = $entity_type_manager
    ->getStorage($storage)
    ->loadMultiple($ids);

  $batch_size = drush_get_option('batch_size', 10);
  $verbose = drush_get_option('debug-message', FALSE);
  $chunks = array_chunk($items, $batch_size);

  foreach ($chunks as $chunk) {
    $ops[] = [
      'drush_manh_agenda_entity_langcode_worker',
      [$chunk, $langcode_to_remove, $langcode_to_keep, $verbose],
    ];
    continue;
  }

  $batch = [
    'title' => dt('Processing entities to update'),
    'init_message' => dt('Loading entities for processing...'),
    'operations' => $ops,
    'finished' => 'drush_manh_agenda_entity_langcode_callback',
  ];

  // Set the batch to start processing nodes.
  batch_set($batch);
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();
}

/**
 * Entity langcode update batch worker.
 *
 * Updates the langcode for the specific entity and fields.
 *
 * @param \Drupal\Core\Entity\ContentEntityBase[] $entities
 *   Array of entities to update.
 * @param string $langcode_to_remove
 *   Langcode to remove.
 * @param string $langcode_to_keep
 *   Langcode to keep.
 * @param boolean $verbose
 *   Verbose messaging.
 *
 * @throws
 */
function drush_manh_agenda_entity_langcode_worker(array $entities, $langcode_to_remove, $langcode_to_keep, $verbose = FALSE) {

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
  }
  $context['message'] = 'Updating ' . count($entities) . ' ' . $entities[0]->getEntityTypeId() . ' entities.';
  foreach ($entities as $entity) {
    $entity_update = drush_manh_agenda_entity_update_langcode($entity, $langcode_to_remove, $langcode_to_keep, $context, $verbose);
    if ($entity_update) {
      $context['sandbox']['progress']++;
      $context['results'][] = $entity->id();
    }
  }
}

/**
 * Updates the langcode for the specific entity and fields.
 *
 * @param \Drupal\Core\Entity\ContentEntityBase $entity
 *   Array of entities to update.
 * @param string $langcode_to_remove
 *   Langcode to remove.
 * @param string $langcode_to_keep
 *   Langcode to keep.
 * @param array $context
 *   Context for batch api.
 * @param boolean $verbose
 *   Verbose messaging.
 *
 * @return boolean
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function drush_manh_agenda_entity_update_langcode(ContentEntityBase $entity, $langcode_to_remove, $langcode_to_keep, $context, $verbose = FALSE) {
  // Check that the entity langcode is correct.
  $context['message'] = 'Verbose message:  ' . $verbose;
  if ($entity->language()->getId() != $langcode_to_remove) {
    if ($verbose) {
      $context['message'] = 'In batch for entity id:  ' . $entity->id() . ' entity does not have lang code ' . $langcode_to_remove . '.';
    }
    return FALSE;
  }
  $field_definitions = $entity->getFieldDefinitions();

  // Check that the translation exist.
  if (!$entity->hasTranslation($langcode_to_keep)) {
    $values_to_keep = $entity->toArray();
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_definition->getSetting('handler') == 'default:paragraph') {
        foreach ($values_to_keep[$field_name] as $paragraph_reference) {
          $paragraph = \Drupal::service('entity_type.manager')
            ->getStorage('paragraph')
            ->loadRevision($paragraph_reference['target_revision_id']);
          $this->change_entity_langcode($paragraph, $langcode_to_remove, $langcode_to_keep);
        }
      }
    }
    $entity->set('langcode', $langcode_to_keep);
    if ($verbose) {
      $context['message'] = 'Entity type ' . $entity->bundle() . ' ' . $entity->id() . ' ' . $entity->label() . ' was set to ' . $langcode_to_keep;
    }
    $entity->save();
    return TRUE;
  }

  // Extract values of translation to keep.
  $values_to_keep = $entity->getTranslation($langcode_to_keep)->toArray();

  // Update paragraph fields recursively.
  foreach ($field_definitions as $field_name => $field_definition) {
    if ($field_definition->getSetting('handler') == 'default:paragraph') {
      foreach ($values_to_keep[$field_name] as $paragraph_reference) {
        $paragraph = \Drupal::service('entity_type.manager')
          ->getStorage('paragraph')
          ->loadRevision($paragraph_reference['target_revision_id']);
        drush_manh_agenda_entity_update_langcode($paragraph, $langcode_to_remove, $langcode_to_keep, $context, $verbose);
      }
    }
  }

  // The removed translation needs to be saved or we can not change the
  // the language to that.
  $entity->removeTranslation($langcode_to_keep);
  $entity->save();

  // Set new langcode.
  $entity->set('langcode', $langcode_to_keep);

  // Copy translatable field values.
  foreach ($values_to_keep as $field_name => $field_values) {

    if ($verbose) {
      $context['message'] = 'In batch for entity id:  ' . $entity->id() . 'updating field: ' . $field_name . '.';
    }
    if ($entity->getFieldDefinition($field_name)
        ->isTranslatable() && !in_array($field_name, ['default_langcode'])) {
      $entity->set($field_name, $field_values);
    }
  }

  // Save entity again.
  $entity->save();
  if ($verbose) {
    $context['message'] = 'Entity type ' . $entity->bundle() . ' ' . $entity->id() . ' ' . $entity->label() . 'default language was saved again, DOUBLE TAP!';
  }
  return TRUE;
}

/**
 * Batch finished callback. Outputs a quantity alert to the command line.
 */
function drush_manh_agenda_entity_langcode_callback($success, $results, $operations) {
  if ($success) {
    drush_print(dt('Finished processing @count langcode updates.', ['@count' => count($results)]));
  }
  else {
    drush_print(dt('Finished with errors.'));
  }
}

/**
 * Node resave batch function.
 *
 * @param int $nid
 *   The node identifier from the batch operation.
 * @param $context
 *   Context for the batch process.
 */
function _manh_scheduler_resave_batch($nid, &$context) {
  // Load the node and identify the languages for the node.
  $node = Node::load($nid);
  $langs = $node->getTranslationLanguages();
  $saved_langs = [];

  // Loop through the node's languages, resaving them.
  if (!empty($langs)) {
    foreach ($langs as $lang) {
      $lang_code = $lang->getId();
      $translation = $node->getTranslation($lang_code);
      $translation->save();
      $saved_langs[] = $lang_code;
    }
  }

  $node->save();

  $context['message'] = 'Node ' . $nid . ' languages ' . implode(', ', $saved_langs) . ' saved...';
  $context['results'][] = $nid;
}

/**
 * Bulk resave batch finish function.
 *
 * @param bool $success
 *   The success of the batch process.
 * @param array $results
 *   An array of results.
 * @param array $operations
 *   An array of operations preformed by the batch proccess.
 */
function _manh_scheduler_resave_batch_finished($success, array $results, array $operations) {
  if ($success) {
    drupal_flush_all_caches();
    drush_print('Cache rebuild complete.');
    drush_print(count($results) . ' nodes updated.');
  }
  else {
    drush_print('Finished with an error.');
  }
}
