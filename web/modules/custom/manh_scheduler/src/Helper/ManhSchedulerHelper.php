<?php

namespace Drupal\manh_scheduler\Helper;

use Drupal\file\Entity\File;
use Drupal\media_entity\Entity\Media;

/**
 * Helper service to handle document file destinations.
 */
class ManhSchedulerHelper {

  /**
   * Determines if media entity is in use.
   *
   * Returns TRUE if media entity is referenced in a node via the
   * field_doc_media field, or FALSE otherwise.
   *
   * @param \Drupal\media_entity\Entity\Media $media_entity
   *   Media entity to query for.
   *
   * @return bool
   *   Returns TRUE if file is in use.
   */
  public static function mediaInUse(Media $media_entity) {
    $in_use = TRUE;

    if (!empty($media_entity->id())) {
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      $node_query_results = $node_storage->getQuery()
        ->condition('field_doc_media', $media_entity->id())
        ->accessCheck(FALSE)
        ->execute();
      $in_use = !empty($node_query_results);
    }
    else {
      $in_use = FALSE;
    }

    return $in_use;
  }

  /**
   * Helper function to make a media entity public or private.
   *
   * @param \Drupal\media_entity\Entity\Media $media_entity
   *   Media entity to update file URI for.
   * @param string $src_string
   *   (optional) Source file stream to replace. Defaults to "public://".
   * @param string $dest_string
   *   (optional) Destination file stream to use, including sub-directory if
   *   applicable. Defaults to "private://manh-unpublished/".
   * @param bool $force_save
   *   (optional) Whether or not to force the file to resave, regardless of
   *   whether or not the file was changed or if the destination did not change.
   *   Useful for when setting to private during hook_media_insert().
   *
   * @return mixed
   *   Return managed file entity if successful, or FALSE otherwise.
   */
  public static function moveMediaFile(Media $media_entity, $src_string = 'public://', $dest_string = 'private://manh-unpublished/', $force_save = FALSE) {
    // @TODO: Remove the bundle check and extend this to work with images
    // in addition to documents.
    if (!empty($media_entity) && $media_entity->bundle() == 'document') {
      // Retrieve the file entity and its URI.
      $media_field_name = 'field_document_file';
      $file_entity = !empty($media_entity->get($media_field_name)) ? $media_entity->get($media_field_name)->entity : NULL;
      $uri = !empty($file_entity) ? $file_entity->getFileUri() : NULL;

      // Exit early if there's no file.
      if (empty($uri)) {
        return FALSE;
      }

      // Attempt to replace the URI.
      // Catch duplicate `manh-unpublished` entries.
      $dest_uri = str_replace('manh-unpublished/manh-unpublished/', 'manh-unpublished/', $uri);
      // Last minute fix for public.
      $dest_uri = str_replace('public://manh-unpublished/', 'public://', $dest_uri);
      // Replace public/private.
      $dest_uri = str_replace($src_string, $dest_string, $dest_uri);

      // Exit early if there was no change to the URI.
      if (!$force_save) {
        if ($dest_uri == $file_entity->getFileUri()) {
          return FALSE;
        }
      }

      // Create the directory if needed.
      $directory = dirname($dest_uri);
      if (!file_exists($directory)) {
        mkdir($directory, 0775, TRUE);
      }

      // Attempt to move the file.
      // @TODO: Extend this to not fail if using stage_file_proxy and the
      // file hasn't been accessed manually.
      $dest_file = file_move($file_entity, $dest_uri, FILE_EXISTS_REPLACE);

      // If the file was successfully moved, save it so that the file is
      // moved in the file system and the file entity is updated.
      if (!empty($dest_file) || $force_save) {
        $file_entity->setFileUri($dest_uri);
        $file_entity->save();

        // Set a message.
        if (strpos($uri, $dest_string) === FALSE) {
          if ($dest_string == 'public://') {
            \Drupal::messenger()->addMessage(t('Moved attached documents to the public file directory.'));
          }
          else {
            \Drupal::messenger()->addMessage(t('Moved attached documents to the private file directory.'));
          }
        }
        elseif (strpos($uri, 'public://manh-unpublished/') !== FALSE) {
          \Drupal::messenger()->addMessage(t('Fixed path issue on attached documents.'));
        }
      }

      // Return file entity if successful.
      return $file_entity;
    }

    // Return false if we got this far.
    return FALSE;
  }

  /**
   * Make media files permanent.
   *
   * This helper function is used to correct file entities on media entities
   * that are not set to permanent. If a file entity is attached to a media
   * entity, it should be permanent since it's likely intended to be referenced
   * long term.
   *
   * @param \Drupal\media_entity\Entity\Media $media_entity
   *   The media entity to update files for.
   *
   * @return bool
   *   Return TRUE if the file entity was updated.
   */
  public static function setMediaFilePermanent(Media $media_entity) {
    // Get the media file entity.
    $media_field_name = 'field_' . $media_entity->bundle() . '_file';
    $file_entity = !empty($media_entity->get($media_field_name)) ? $media_entity->get($media_field_name)->entity : NULL;

    // If this is a document and the file isn't permanent, make it permanent.
    // Otherwise, form validation errors may occur when users try to save the
    // translation.
    if (!empty($file_entity)) {
      if (!$file_entity->isPermanent()) {
        $file_entity->setPermanent();
        $file_entity->save();
        \Drupal::messenger()->addMessage(t('Updated file entity to be permanent.'));
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Creates new file translations.
   *
   * This helper method is used to create new file entities for translated
   * media entities. Files are not normally translatable, so this works around
   * that behavior.
   *
   * @param \Drupal\media_entity\Entity\Media $media_entity
   *   The media entity to update files for.
   * @param bool $allow_saving
   *   (optional) Whether or not to allow saving the media translation entity
   *   within this method call.
   *
   * @return bool
   *   Return TRUE if the file entity was updated.
   */
  public static function setMediaFileTranslation(Media $media_entity, $allow_saving = TRUE) {
    // Ensure the correct language code is appended to files.
    $lang_manager = \Drupal::service('language_manager');
    $langs = $lang_manager->getLanguages();
    $lang_code = $media_entity->language()->getId();

    // Get media translation.
    $media_translation = $media_entity->hasTranslation($lang_code) ? $media_entity->getTranslation($lang_code) : $media_entity;

    // Get the media file entity.
    $media_field_name = 'field_' . $media_entity->bundle() . '_file';
    $file_entity = !empty($media_translation->get($media_field_name)) ? $media_translation->get($media_field_name)->entity : NULL;
    $require_save = FALSE;

    // If this is a document and the file isn't permanent, make it permanent.
    // Otherwise, form validation errors may occur when users try to save the
    // translation.
    if (!empty($file_entity)) {
      $file_translation = $file_entity->hasTranslation($lang_code) ? $file_entity->getTranslation($lang_code) : NULL;

      if (empty($file_translation)) {
        // Get the default values to set new translation to.
        $file_to_clone_entity = $file_entity->createDuplicate();
        $file_to_clone = $file_to_clone_entity->toArray();

        // Enforce a new UUID.
        $uuid_service = \Drupal::service('uuid');
        $uuid = $uuid_service->generate();
        $file_to_clone['uuid'][0]['value'] = $uuid;

        // Enforce the new langcode.
        $file_to_clone['langcode'] = $lang_code;

        // Create new file translation. Save the result in a var so that we can
        // pull the appropriate file entity later.
        $file_translation = File::create($file_to_clone);
        $require_save = TRUE;
      }

      if (!empty($langs)) {
        $file_name = $file_translation->getFilename();
        $file_uri = $file_translation->getFileUri();
        $langcode_override = \Drupal::service('langcode_override.manager');

        // Check the filename against each language ID in order to set the
        // correct filename and uri.
        $lang = $langs[$lang_code];

        if (isset($langs[$lang_code])) {
          $lang_id = $langs[$lang_code]->getId();
          $old_lang_id = $lang_id;
          $old_id = '-' . $old_lang_id . '.';
          $lang_segment = '-' . $langcode_override->getOverriddenLanguageCode($lang_id) . '.';

          // If this translation doesn't exist yet, loop through the other
          // languages for a matching ID for the one we're starting from.
          foreach ($langs as $lang) {
            $old_lang_id_temp = $lang->getId();
            if (strpos($file_name, "-$old_lang_id_temp.") !== FALSE) {
              $old_lang_id = $old_lang_id_temp;
              $old_id = "-$old_lang_id_temp.";
              break;
            }
          }

          // If the translation doesn't exist yet (e.g. `en` => `fr`), we need
          // to copy rather than move.
          if ($lang_id !== $old_lang_id) {
            $copy = TRUE;
          }
          else {
            $copy = FALSE;
          }

          $new_name = str_replace([$lang_segment, $old_id], $lang_segment, $file_name);
          $new_uri = str_replace([$lang_segment, $old_id], $lang_segment, $file_uri);

          if ($new_name !== $file_name) {
            // Create the directory if needed.
            $directory = dirname($new_uri);
            if (!file_exists($directory)) {
              mkdir($directory, 0775, TRUE);
            }

            // File move calls $file->save(), so we need to do an unmanaged
            // move to safely move it over.
            if ($copy) {
              file_unmanaged_copy($file_uri, $new_uri, FILE_EXISTS_REPLACE);
            }
            else {
              file_unmanaged_move($file_uri, $new_uri, FILE_EXISTS_REPLACE);
            }

            $file_translation->setFilename($new_name);
            $file_translation->setFileUri($new_uri);
            $require_save = TRUE;

            \Drupal::service('messenger')->addMessage(t('Updated filename to use @lang langcode.', [
              '@lang' => $langcode_override->getOverriddenLanguageCode($lang_id),
            ]));
          }
        }
      }
    }

    // If any of the above steps required saving, try to save the file and
    // media entities.
    if ($require_save) {
      // Save the new file if we have one.
      if (!empty($file_translation)) {
        $file_translation->save();
      }
      // Otherwise, save the existing file.
      else {
        $file_entity->save();
      }

      // If new files were created, update the relevant media entities.
      if (!empty($file_translation)) {
        $media_entity->set($media_field_name, $file_entity);
        $media_translation->set($media_field_name, $file_translation);

        // If saving the Media entity is disallowed, it's assumed that the media
        // entity will be saved manually outside of this method (such as during
        // hook_entity_presave()).
        if ($allow_saving) {
          $media_translation->save();
        }
        else {
          return TRUE;
        }
      }
      return TRUE;
    }

    return FALSE;
  }

}
