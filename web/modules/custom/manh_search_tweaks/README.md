This module performs the following functions:

- Adds a search plugin based on Node's search plugin that limits the results to
  nodes of a certain type. These types are hardcoded in the findResults function
  in the ManhNodeSearch class.
