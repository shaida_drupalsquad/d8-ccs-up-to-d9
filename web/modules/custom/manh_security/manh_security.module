<?php

/**
 * @file Main functionality for manh security Module.
 */

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\contact\Entity\Message;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function manh_security_form_user_pass_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['#validate'] = ['manh_security_form_user_pass_xss'];

}

/**
 * Short term solution for cleaning XSS from user/password form values.
 *
 * MA-1044: We got results from pentest that user/password was vulnerable to
 * XSS. It looks to be unique to MANH, neither Momentum or Logistics are
 * affected, but we need a short gap solution until we can uncover the root
 * cause.
 *
 * @param $form array
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 *
 * @see \Drupal\user\Form\UserPasswordForm::validateForm().
 */
function manh_security_form_user_pass_xss(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\user\UserStorage $user_storage */
  $user_storage = \Drupal::service('entity_type.manager')
    ->getStorage('user');
  // Clean text like '<script>' from value.
  $name = Xss::filter(trim($form_state->getValue('name')));
  // Try to load by email.
  $users = $user_storage->loadByProperties(['mail' => $name]);
  if (empty($users)) {
    // No success, try to load by name.
    $users = $user_storage->loadByProperties(['name' => $name]);
  }

  $account = reset($users);
  if ($account && $account->id()) {
    // Blocked accounts cannot request a new password.
    if (!$account->isActive()) {
      $form_state->setErrorByName('name', $this->t('%name is blocked or has not been activated yet.', ['%name' => $name]));
    }
    else {
      $form_state->setValueForElement(['#parents' => ['account']], $account);
    }
  }
  else {
    $form_state->setErrorByName('name', t('%name is not recognized as a username or an email address.', ['%name' => $name]));
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function manh_security_contact_message_presave(EntityInterface $entity) {
  // We are filtering all user submitted data for messages so downstream
  // services like Marketo have clean data.
  $message_fields = $entity->getFields();
  foreach ($message_fields as $field_name => $field_type ) {
    // Update user submitted fields to filter malicious text.
    if (strpos($field_name, 'field') !== FALSE) {
      $xss_filtered_value = Xss::filter($entity->get($field_name)->value);
      $entity->set($field_name, $xss_filtered_value);
    }
    elseif ($field_name == 'message') {
      $entity->setMessage(Xss::filter($entity->getMessage()));
    }
  }
}