<?php
/**
 * @file
 * Contains \Drupal\manh_site_selector\Plugin\Block\SiteSelector.
 */
namespace Drupal\manh_site_selector\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Manhattan Associates Site Selector block.
 *
 * @Block(
 *   id = "manh_site_selector",
 *   admin_label = @Translation("Site/Language Selector"),
 *   category = @Translation("Blocks")
 * )
 */
class SiteSelector extends BlockBase {
  private static $localeMap = [
    'en' => 'United States',
    'en-in' => 'India',
    'en-gb' => 'United Kingdom',
    'en-au' => 'Australia',
    'en-nl' => 'Netherlands',
    'es' => 'América Latina',
    'ja' => '日本',
    'zh-hans' => '中国',
    'nl' => 'Nederland',
    'fr' => 'France',
    'nl-be' => 'België',
    'en-hk' => 'Hong Kong',
    'en-id' => 'Indonesia',
    'en-nz' => 'New Zealand',
    'en-sg' => 'Singapore',
    'en-za' => 'South Africa',
    'en-ae' => 'United Arab Emirates',
    'fi' => 'Suomi',
    'hu' => 'Magyarország',
    'is' => 'Ísland',
    'de' => 'Deutschland',
    'it' => 'Italia',
    'pt-br' => 'Brasil',
    'pt-pt' => 'Portugal',
    'ms-my' => 'Malaysia',
    'nb' => 'Norge',
    'pl' => 'Polska',
    'ro' => 'România',
    'ru' => 'Россия',
    'es-es' => 'España',
    'es-pa' => 'Panamá',
    'sv' => 'Sverige',
    'th' => 'ประเทศไทย',
  ];

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $locale_name = $lang;
    if (isset(self::$localeMap[$lang])) {
      $locale_name = self::$localeMap[$lang];
    }
    $render = [
      '#langcode' => $lang,
      '#locale_name' => $locale_name,
      '#destination' => \Drupal\Core\Url::fromUserInput($config['link_destination'])->toString(),
      '#theme' => 'manh_block_site_selector',
    ];
    return $render;
  }


  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['link_destination'] = [
      '#type' => 'textfield',
      '#title' => t('Site Switcher Page Path'),
      '#description' => t('Enter the path of the site switcher page.'),
      '#default_value' => isset($config['link_destination']) ? $config['link_destination'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('link_destination', $form_state->getValue('link_destination'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $link_destination = $form_state->getValue('link_destination');

    if (!\Drupal::pathValidator()->isValid($link_destination)) {
      $form_state->setErrorByName('link_destination', t('The Site Switcher Page path must be a valid Drupal path.'));
    }
  }

}
