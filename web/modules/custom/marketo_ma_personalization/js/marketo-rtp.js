(function ($, Drupal) {

  Drupal.marketoMaPersonalization = {

    /**
     * Load the RTP script onto the page.
     */
    addRtpScript: function() {
      if (drupalSettings.marketoMaPersonalization.enable_rtp) {
        var rtp_url = drupalSettings.marketoMaPersonalization.rtp_url;
        var rtp_account = drupalSettings.marketoMaPersonalization.rtp_account;

        // Load the RTP script.
        // @todo Enable configuration of the tag option to use.
        (function (c, h, a, f, i, e) {
          c[a] = c[a] || function () {
              (c[a].q = c[a].q || []).push(arguments)
            };
          c[a].a = i;
          c[a].e = e;
          var g = h.createElement("script");
          g.async = true;
          g.type = "text/javascript";
          g.src = f + '?aid=' + i;
          var b = h.getElementsByTagName("script")[0];
          b.parentNode.insertBefore(g, b);
        })(window, document, "rtp", rtp_url, rtp_account);

        return true;
      }

      // Return false if the script was not added. Most likely because
      // RTP was disabled.
      return false;
    },

    /**
     * Report the page view through the RTP script.
     */
    sendPageView: function() {
      if (drupalSettings.marketoMaPersonalization.enable_rtp) {
        // Send page view (required by  the recommendation)
        rtp('send', 'view');
      }
    },

    /**
     * Populate recommendation zones by loading the campaign.
     */
    getCampaign: function() {
      // Populate the recommendation zone
      rtp('get', 'campaign', true);
    }
  };

  Drupal.behaviors.marketoMaPersonalization = {

    attach: function(context, settings) {
      // Only execute once per page load.
      $(document).once('marketo-rtp').each(function() {
        // Only trigger if the setting is enabled.
        if (typeof settings.marketoMaPersonalization.enable_rtp !== 'undefined' &&
          settings.marketoMaPersonalization.enable_rtp) {

          Drupal.marketoMaPersonalization.addRtpScript();
          Drupal.marketoMaPersonalization.sendPageView();
          Drupal.marketoMaPersonalization.getCampaign();
        }
      });
    }
  }

})(jQuery, Drupal);
