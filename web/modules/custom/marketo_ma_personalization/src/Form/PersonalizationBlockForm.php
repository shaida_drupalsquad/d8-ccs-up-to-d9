<?php

namespace Drupal\marketo_ma_personalization\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity form class for Personalization Block entities.
 *
 * @package Drupal\marketo_ma_personalization\Form
 */
class PersonalizationBlockForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\marketo_ma_personalization\PersonalizationBlockEntityInterface $entity */
    $entity = $this->entity;

    // Change the page title for the edit operation.
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit personalization_block: @name',
        ['@name' => $entity->name]);
    }

    // The personalization_block name.
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $entity->name,
      '#description' => $this->t("Block name."),
      '#required' => TRUE,
    ];

    // The unique machine name of the personalization_block.
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#default_value' => $entity->id,
      '#disabled' => !$entity->isNew(),
      '#machine_name' => [
        'source' => ['name'],
        'exists' => 'personalization_block_load'
      ],
    ];

    // The zone ID for reference in Marketo.
    // @todo Add validation or a warning for an invalid ID.
    $form['zone_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Marketo Zone Id'),
      '#maxlength' => 255,
      '#default_value' => $entity->zone_id,
      '#description' => $this->t("The zone Id to be configured in a Marketo In Zone web campaign."),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    $form_state->setRedirect('personalization_block.list');
  }

}
