<?php
/**
 * @file
 * Contains \Drupal\marketo_ma_personalization\PersonalizationBlockEntityInterface.
 */

namespace Drupal\marketo_ma_personalization;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining a personalization block entity type.
 *
 * @package Drupal\marketo_ma_personalization
 */
interface PersonalizationBlockEntityInterface extends ConfigEntityInterface {
}
