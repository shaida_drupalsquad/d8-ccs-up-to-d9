(function ($, Drupal) {

  Drupal.marketoMaPredictive = {

    /**
     * Configure and populate predictive content recommendations.
     */
    loadContentRecommendations: function() {
      // Abort if there are no rtp blocks to populate.
      if (drupalSettings.marketoMaPredictive.rtp_blocks === undefined) {
        return;
      }

      // Load template configuration options via drupalSettings.
      var template_configurations = drupalSettings.marketoMaPredictive.rtp_configuration;

      // Send the configuration command.
      if (!$.isEmptyObject(template_configurations)) {
        rtp("set", "rcmd", "richmedia", template_configurations);
      }

      // Populate the blocks.
      rtp("get", "rcmd", "richmedia");
    }
  };

  Drupal.behaviors.marketoMaPredictive = {

    attach: function(context, settings) {
      // Only execute once per page load.
      $(document).once('marketo-predictive').each(function() {
        // Only trigger if the setting is enabled.
        if (typeof settings.marketoMaPersonalization.enable_rtp !== 'undefined' &&
          settings.marketoMaPersonalization.enable_rtp &&
          typeof rtp !== 'undefined') {

          // @todo Confirm the presence of predictive content blocks.
          Drupal.marketoMaPredictive.loadContentRecommendations();
        }
      });
    }
  }

})(jQuery, Drupal);
