<?php
/**
 * @file
 * Contains Drupal\marketo_ma_predictive\Entity\PredictiveContentListBuilder.
 */

namespace Drupal\marketo_ma_predictive\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Entity list builder for predictive content entities.
 *
 * @package Drupal\marketo_ma_predictive\Entity
 */
class PredictiveContentListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['template_id'] = $this->t('Template Id');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['template_id'] = $entity->template_id;

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {

    $build = parent::render();

    // Add a message when there are no entities to display.
    $build['#empty'] = $this->t('There are no predictive content blocks available.');

    return $build;
  }


}
