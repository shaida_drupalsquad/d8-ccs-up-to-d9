<?php
/**
 * Contains \Drupal\marketo_ma_predictive\Form\PredictiveContentForm.
 */

namespace Drupal\marketo_ma_predictive\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity form class for Predictive Content entities.
 *
 * @package Drupal\marketo_ma_predictive\Form
 */
class PredictiveContentForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\marketo_ma_predictive\PredictiveContentEntityInterface $entity */
    $entity = $this->entity;

    // Change page title for the edit operation
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit predictive_content: @name',
        ['@name' => $entity->name]);
    }

    // The predictive_content name.
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $entity->name,
      '#description' => $this->t("Block name."),
      '#required' => TRUE,
    ];

    // The unique machine name of the predictive_content.
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#default_value' => $entity->id,
      '#disabled' => !$entity->isNew(),
      '#machine_name' => [
        'source' => ['name'],
        'exists' => 'predictive_content_load'
      ],
    ];

    // The RTP template ID from Marketo to be used.
    $form['template_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Marketo Template ID'),
      '#default_value' => $entity->template_id,
      '#description' => $this->t('The template ID to use from Marketo. Examples of the templates may be found <a href=":url">here.</a>',
        [':url' => 'http://developers.marketo.com/javascript-api/web-personalization/rich-media-recommendation/#example_of_rich_media_recommendation_template_1']),
      '#options' => [
        'template1' => $this->t('Template 1: Horizontal Alignment'),
        'template2' => $this->t('Template 2: Vertical Alignment'),
        'template3' => $this->t('Template 3: Vertical with Title and Description Only'),
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    $form_state->setRedirect('predictive_content.list');
  }

}
