<?php

/**
 * @file
 * Contains Drupal\marketo_ma_predictive\Plugin\Block\PredictiveSuggestionsBlock.
 */

namespace Drupal\marketo_ma_predictive\Plugin\Block;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Predictive Content Suggestions block.
 *
 * @Block(
 *   id = "predictive_suggestions_block",
 *   admin_label = @Translation("Predictive Suggestions Block"),
 *   category = @Translation("Marketo"),
 *   deriver = "Drupal\marketo_ma_predictive\Plugin\Derivative\PredictiveSuggestionsBlock",
 *   context = {
 *     "node" = @ContextDefinition(
 *       "entity:node",
 *       label = @Translation("Current Node")
 *     )
 *   }
 * )
 */
class PredictiveSuggestionsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The storage handler to load predictive content entities.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $storageHandler;

  /**
   * PredictiveSuggestionsBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Then entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->storageHandler = $entity_type_manager->getStorage('predictive_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#type' => 'predictive_content_block',
      '#block' => $this->storageHandler->load($this->getDerivativeId()),
      '#node' => $this->getContextValue('node'),
    ];

    return $build;
  }

}
