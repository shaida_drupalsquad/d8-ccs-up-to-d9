<?php

/**
 * @file
 * Contains Drupal\marketo_ma_predictive\Plugin\Derivative\PredictiveSuggestionsBlock.
 */

namespace Drupal\marketo_ma_predictive\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This deriver enables blocks to be created for Marketo content suggestions.
 *
 * @see \Drupal\marketo_ma_predictive\Entity\PredictiveContentEntity
 */
class PredictiveSuggestionsBlock extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $predictiveContentStorage;

  /**
   * Constructs new PredictiveSuggestionsBlock deriver.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface|\Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service to load entity storage from.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->predictiveContentStorage = $entity_type_manager->getStorage('predictive_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Load all configured predictive content blocks.
    $entities = $this->predictiveContentStorage->loadMultiple();

    // Reset the discovered definitions.
    $this->derivatives = [];

    // Rebuild the discovered definitions.
    foreach ($entities as $id => $entity) {
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['admin_label'] = $this->t('Predictive Content: @name',
        ['@name' => $entity->label()]);

      // Add cache tags to ensure lists are updated.
      $this->derivatives[$id] += ['cache_tags' => []];
      $this->derivatives[$id]['cache_tags'] += $entity->getEntityType()->getListCacheTags();
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
