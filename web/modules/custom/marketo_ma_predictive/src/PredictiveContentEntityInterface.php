<?php
/**
 * @file
 * Contains \Drupal\marketo_ma_predictive\PredictiveContentEntityInterface.
 */

namespace Drupal\marketo_ma_predictive;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining a predictive content entity type.
 *
 * @package Drupal\marketo_ma_predictive
 */
interface PredictiveContentEntityInterface extends ConfigEntityInterface {

}
