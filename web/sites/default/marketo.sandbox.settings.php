<?php

/**
 * @file
 * Marketo Sandbox configuration file.
 *
 * This file contains configuration overrides that are specific to
 * environments that will point to the Marketo Sandbox instance, as opposed
 * to the Marketo Production instance. Most notably, this applies to the
 * Pantheon Test environments and individual developer's local environments.
 */

/**
 * Define configuration overrides for marketo_ma.settings Munchkin and REST API.
 */
$config['marketo_ma.settings']['munchkin']['api_private_key'] = 'D1VnJH9AVul0bKdyFO0LAgpRzfCGUHsMP-fYlf0OC3cwM8KYOdw085rbsSy5mB0m3Qc';
$config['marketo_ma.settings']['munchkin']['account_id'] = 'k7u8kLfnQBN0rMJy9RjgUbG6gkyQrmAArXHZE1JpNCswMxw0GYOKC1fPW9gNRSGJE8N0D0czTwsWGOQYfg';
$config['marketo_ma.settings']['rest']['client_id'] = '9GYyEvJ7lTz2O6jcNLteXtpqVxUJFQBnOVzcGDJtXrMwM46KtJRyUecoHwlLB5UrXuYFODy5HoL2QU-bs6tz0KJIJ_AuHFu--2EHZWLfYZCMb5Xby-A';
$config['marketo_ma.settings']['rest']['client_secret'] = 'UBYclSof_3bx5HPjFawqvVLBSfAsKbq50DEKkRI7EiYwM65CnPbDzGzjN2ol0jIYjMXfw-uf83xwVcRCaIb5ss1sF8NQgH7z3xC9rqb4v5SFKw';

/**
 * Define configuration overrides for contact.form.contact_us.
 */
// Set the Marketo Sandbox Contact Us campaign ID.
$config['contact.form.contact_us']['third_party_settings']['manh_contact_block']['campaign_id'] = '1235';

/**
 * Define configuration overrides for contact.form.gated_content.
 */
// Set the Marketo Sandbox Gated Content campaign ID.
$config['contact.form.gated_content']['third_party_settings']['manh_contact_block']['campaign_id'] = '1240';
