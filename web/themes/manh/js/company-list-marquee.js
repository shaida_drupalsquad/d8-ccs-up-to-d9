!(($, Drupal) => {
  'use strict';

  Drupal.behaviors.companyListMarquee = {
    attach: function(context, settings) {

      var $marquee = $('.companylist-marquee__list', context);

      $marquee.each(function() {
        var $this = $(this);

        // - Marquee only animates when `company_display_type` is not `text`.
        var $isTextDisplay = $this.hasClass('display--text');
        if ($isTextDisplay) {
          return;
        }

        // Grab the parent setting for display type based on attribute.
        var $companyListDisplayType = $this.closest('[data-company-display-type]');
        if (!$companyListDisplayType.length) {
          return;
        }

        // Set preferred slidesToShow count.
        // Rules:
        // - Marquee only animates when there are more logos, than space.
        // -- Company List viewmode `block_1_column` (page has 2 sidebars) = 2 logo.
        // -- Company List viewmode `block_2_column` (page has 1 sidebars) = 4 logos.
        // -- Company List viewmode `block_3_column` (page has no sidebars) = 6 logos.
        var $companyDisplayType = $companyListDisplayType.attr('data-company-display-type');
        var slidesToShow = 0;
        var slidesToShowDesk = 0;
        var slidesToShowMobile = 0;

        if ($companyDisplayType == 'block-1-columns') {
          slidesToShow = 2;
          slidesToShowDesk = 2;
          slidesToShowMobile = 2;
        }
        else if ($companyDisplayType == 'block-2-columns') {
          slidesToShow = 4;
          slidesToShowDesk = 2;
          slidesToShowMobile = 2;
        }
        else if ($companyDisplayType == 'block-3-columns') {
          slidesToShow = 6;
          slidesToShowDesk = 4;
          slidesToShowMobile = 2;
        }

        $this
          .slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            infinite: true,
            slidesToShow: slidesToShow,
            slidesToScroll: 1,
            speed: 8000,
            responsive: [
              {
                breakpoint: 1020,
                settings: {
                  slidesToShow: slidesToShowDesk,
                  slidesToScroll: slidesToShowDesk
                }
              },
              {
                breakpoint: 639,
                settings: {
                  slidesToShow: slidesToShowMobile,
                  slidesToScroll: slidesToShowMobile
                }
              }
            ]
          });

      });
    }
  };

})(jQuery, Drupal);
