/**
 * @file
 * Adds Drupal behaviors for the contact menu block.
 */

(function(Drupal) {
  'use strict';

  Drupal.behaviors.blockDrag = {
    attach: function (context, settings) {
      if (context === document) {
        // This is defined in contact-menu-block.js
        manhContactMenuBlockBehavior();
      }
    }
  };

})(Drupal);
