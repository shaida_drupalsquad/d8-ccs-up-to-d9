/**
 * @file
 * Adjustments to the contact form.
 */

(function($, Drupal) {
  "use strict";

  Drupal.behaviors.contactAdjustments = {
    attach: function(context, settings) {
      if (context == document) {
        // Add an alert message area below the "Interested In" field.
        $('#edit-field-interested-in-wrapper .form-item').after('<div id="alert-interested-in"></div>');

        // Show or hide alert message depending on "Interested In" selection.
        $('#edit-field-interested-in').change(function () {
          var val = $('#edit-field-interested-in').val();
          if (val == 'Education and Training') {
            $('#alert-interested-in')
              .addClass('alert')
              .html(Drupal.t('Please Note: Manhattan training classes are ' +
                'closed events, limited to Manhattan customers and registered ' +
                'partners only.'));
          }
          else if (val == 'Careers') {
            $('#alert-interested-in')
              .addClass('alert')
              .html(Drupal.t('Please Note: If you are interested in a career at ' +
                'Manhattan Associates, please view our ' +
                '<a href="/about-us/careers/career-opportunities">Career Opportunities</a>. ' +
                'Please create an account and upload a resume to be considered ' +
                'for open positions.'));
          }
          else {
            $('#alert-interested-in')
              .removeClass('alert')
              .html('');
          }
        });
      }
    }
  };

})(jQuery, Drupal);
