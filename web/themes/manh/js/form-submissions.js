!((Drupal, $) => {
  'use strict';
  Drupal.behaviors.formSubmissions = {
    /* eslint-disable max-len */

    // Attach method will run when behaviors mounts.
    attach: function(context) {
      var $forms = $('.contact-message-form form, form.contact-message-form', context);

      if (!$forms.length) {
        return;
      }

      this.setupFormSubmission($forms);
    },

    setupFormSubmission: function($forms) {
      var self = this;
      // Accessible text that is read.
      var message = Drupal.t('Thank you! Please wait while we process your submission.');
      var $messageElement = $('<span id="form-submitting-text" class="submitting__mobile" tabindex="0">' + message + '</span>');

      $forms.each(function () {
        var $form = $(this);

        $form.on('submit', function() {
          $form
            .addClass('submitting')
            .append($messageElement);
          $('#form-subbmitting-text').focus();
        });
      });
    }

  };
})(Drupal, jQuery);
