!(($, Drupal) => {
  'use strict';

  Drupal.behaviors.manhVideoGatedPlaylist = {
    attach: function(context, settings) {

      // Main Slider.
      $('.main-slider', context)
        .slick({
          arrows: false,
          autoplay: true,
          autoplaySpeed: 8000,
          cssEase: 'ease-in-out',
          dots: true,
        });

      // Standard Slider.
      $('.slick-list-container .slick-list', context)
        .addClass('has-arrows')
        .slick({
          infinite: false,
          slidesToShow: 3,
          slidesToScroll: 2,
          swipe: true,
          responsive: [
            {
              breakpoint: 1020,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 720,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

      $('.logos .slick-list', context)
        .slick({
          arrows: false,
          autoplay: true,
          autoplaySpeed: 0,
          cssEase: 'linear',
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 1,
          speed: 8000,
          responsive: [
            {
              breakpoint: 1020,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 720,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

      // Stamp body if there are active sliders on the page.
      const $body = $('body');
      if (!$body.hasClass('slider-with-arrows') && $('.slick-initialized.has-arrows').length) {
        $body.addClass('slider-with-arrows');
      }
    }
  };

})(jQuery, Drupal);
