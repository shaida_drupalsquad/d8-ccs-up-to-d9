/* Lazy Loader Vidyard */
(function ($) {

  Drupal.behaviors.lazyVidYard = {
    attach: function(context, settings) {

      $.Lazy('lazyLoadVidyard', function (element, response) {

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://play.vidyard.com/embed/v4.js';
        document.head.appendChild(script);

        response(true);
      });
    }
  };

}(window.jQuery, window.Drupal));
