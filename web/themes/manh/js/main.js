/**
 * @file
 * Related Information Carousel
 */

(function($) {
  "use strict";

  var sw = document.body.clientWidth,
    breakpointSize = getBreakpointSize();

  // Set up manager objects for each carousel on the page.
  var $carousels = $('.carousel-wrapper');
  var carouselManagers = [];
  $carousels.each(function(i, c) {
    var $c = $(c);
    var manager = {
      $carousel: $c,
      $cList: $('.carousel-list', $c),
      $cWidth: $c.outerWidth(),
      $li: $('li.carousel-slide', $c),
      $liLength: $('li.carousel-slide', $c).size(),
      $prev: $('.carousel-nav .prev', $c.parent()),
      $next: $('.carousel-nav .next', $c.parent()),
      animLimit: 0,
      current: 0,
      multiplier: 1,

      // Determine the size and number of panels to reveal.
      sizeCarousel: function() {
        this.current = 0;
        if (breakpointSize == 'medium') {
          this.multiplier = 2;
        }
        else if (breakpointSize == 'large') {
          this.multiplier = 3;
        }

        this.animLimit = Math.max(this.$liLength - this.multiplier, 0);

        // Set panel widths.
        this.$li.outerWidth(this.$cWidth / this.multiplier);

        // Hide arrows if there aren't enough items to scroll.
        if (this.multiplier >= this.$liLength) {
          this.$prev.hide();
          this.$next.hide();
        }
        else {
          this.$prev.show();
          this.$next.show();
        }

        // Initialize scroll arrow states.
        this.$prev.addClass('scroll-end');
        if (this.$liLength <= this.multiplier) {
          this.$next.addClass('scroll-end');
        }
      },

      // Animate Carousel. CSS transitions used for the actual animation.
      // Note: 330 is the width of an individual slide (with padding).
      posCarousel: function() {
        var pos = -this.current * 330;
        this.$cList.css("left", pos);

        this.$prev.toggleClass('scroll-end', this.current == 0);
        this.$next.toggleClass('scroll-end', this.current == this.animLimit);
      }
    };

    // Previous Button Click.
    manager.$prev.click(function(e) {
      e.preventDefault();
      if (manager.current > 0) {
        manager.current--;
      }
      manager.posCarousel();
    });

    // Next Button Click.
    manager.$next.click(function(e) {
      e.preventDefault();
      if (manager.current < manager.animLimit) {
        manager.current++;
      }
      manager.posCarousel();
    });

    carouselManagers.push(manager);
  });

  $(document).ready(function() {
    $.each(carouselManagers, function(i, m) {
      m.sizeCarousel();
    });
  });

  // On Window Resize.
  $(window).resize(function() {
    sw = document.body.clientWidth;

    // Conditional CSS http://adactio.com/journal/5429/
    breakpointSize = getBreakpointSize();

    $.each(carouselManagers, function(i, m) {
      m.$cWidth = m.$carousel.width();
      m.sizeCarousel();
      m.posCarousel();
    });
  });

  // Determine the breakpoint size based on the "content" property of the body.
  function getBreakpointSize() {
    return window.getComputedStyle(document.body, ':after').getPropertyValue('content')
      .replace(/'/g, '').replace(/"/g, '');
  }

  // FitText plugin.
  $(document).ready(function() {
    $('.stat-image-text').fitText(1.5, { minFontSize: '11px', maxFontSize: '18px' });
  });

})(jQuery);

(function($) {
  "use strict";

  //set target blank for all "Connect" menu links
  $(document).ready(function() {
    // TODO: Renable this, but with more specific targeting.
    // $('.menu-item a[href^="http"]').attr('target','_blank');
  });
})(jQuery);
