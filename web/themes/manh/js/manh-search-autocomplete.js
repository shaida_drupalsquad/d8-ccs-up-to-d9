/**
 * @file
 * Customizations for autocomplete behavior.
 */
(function($, Drupal) {
  "use strict";

  Drupal.behaviors.manhSearchAutocomplete = {
    attach : function(context, settings) {
      if (context === document) {
        // Disable autocomplete on main search form.
        $('.search-form .form-autocomplete')
          .removeClass('form-autocomplete');

        // Get autocomplete inputs.
        var $autocomplete = $(context).find('input.form-autocomplete');
        var $ui_autocomplete = $(context).find('.ui-autocomplete');

        // Add a throbber div.
        $autocomplete.after('<div class="ui-autocomplete-throbber"></div>');

        // Toggle classes on events.
        $autocomplete.on('autocompleteclose', function(event, node) {
          $autocomplete.removeClass('is-active');
        });
        $autocomplete.on('autocompleteopen', function(event, node) {
          $autocomplete.addClass('is-active');
          $ui_autocomplete = $(context).find('.ui-autocomplete');

          var no_results = $(context).find('.ui-autocomplete-field-group').hasClass('no_results');

          if (no_results) {
            $ui_autocomplete.addClass('no-results');
            $autocomplete.removeClass('is-active');
          }
          else {
            $ui_autocomplete.removeClass('no-results');
            $autocomplete.addClass('is-active');
          }
        });
      }
    }
  };

})(jQuery, Drupal);