/**
 * @file
 * Custom select element/ddSlick integration for Manhattan Associates.
 */
(function($, Drupal) {
  "use strict";

  Drupal.behaviors.manhSelect = {
    attach : function(context, settings) {
      if (context == document) {
        // On contact forms, create clones of all select elements for ddslick.
        $('.contact-form select').each(function () {
          var id = $(this).attr('id');
          var name = $(this).attr('name');
          var newId = id + '-dd';
          var newName = name + '-dd';
          $(this).clone().attr('id', newId).attr('name', newName).insertAfter(this).ddslick({
            originalId: id,
            onSelected: function(data) {
              // Set the value in the hidden select element for Drupal form processing.
              var value = data.selectedData.value;
              var id = '#' + data.settings.originalId;
              var oldValue = $(id).val();
              if (value != oldValue) {
                $(id).val(value).trigger('change');
              }
            }
          });

          // Hide but keep the original element for form processing.
          $(this).hide();
        });

        // Sub-industry selector requires an update on each change of the Industry field.
        $('#edit-field-industry').change(function(){
          // Remove and re-create the sub-industry clone.
          $('#edit-field-industry-sub-dd').remove();
          $('#edit-field-industry-sub')
            .clone()
            .attr('id', 'edit-field-industry-sub-dd')
            .insertAfter('#edit-field-industry-sub')
            .ddslick({
              originalId: 'edit-field-industry-sub',
              onSelected: function(data) {
                // Set the value in the hidden select element for Drupal form processing.
                var value = data.selectedData.value;
                var id = '#' + data.settings.originalId;
                var oldValue = $(id).val();
                if (value != oldValue) {
                  $(id).val(value).trigger('change');
                }
              }
            });
        });
      }

      // Invoke ddslick on exposed filter for Office Locations view.
      // If the cloned exposed form element does not already exist...
      if ($('#edit-locations-dd').length == 0) {
        // For the exposed filter on the office locations form, create a clone of the select element.
        $('#views-exposed-form-office-locations-block-1 select[name="locations"]')
          .clone()
          .attr('id', 'edit-locations-dd')
          .attr('name', 'locations-dd')
          .appendTo('#views-exposed-form-office-locations-block-1');
        $('#views-exposed-form-office-locations-block-1 select[name="locations"]').hide();
        $('#views-exposed-form-office-locations-block-1 select#edit-locations-dd').ddslick({
          onSelected: function (data) {
            var value = data.selectedData.value;
            var oldValue = $('#views-exposed-form-office-locations-block-1 select[name="locations"]').val();
            if (value != oldValue) {
              $('#views-exposed-form-office-locations-block-1 select[name="locations"]').val(value).trigger('change');
            }
          }
        });
      }
      if ($('#edit-locations-dd').length == 0) {
        // For the exposed filter on the office locations form, create a clone of the select element.
        $('#views-exposed-form-office-locations-with-map-block-1 select[name="locations"]')
          .clone()
          .attr('id', 'edit-locations-dd')
          .attr('name', 'locations-dd')
          .appendTo('#views-exposed-form-office-locations-with-map-block-1');
        $('#views-exposed-form-office-locations-with-map-block-1 select[name="locations"]').hide();
        $('#views-exposed-form-office-locations-with-map-block-1 select#edit-locations-dd').ddslick({
          onSelected: function (data) {
            var value = data.selectedData.value;
            var oldValue = $('#views-exposed-form-office-locations-with-map-block-1 select[name="locations"]').val();
            if (value != oldValue) {
              $('#views-exposed-form-office-locations-with-map-block-1 select[name="locations"]').val(value).trigger('change');
            }
          }
        });
      }
    }
  };

})(jQuery, Drupal);