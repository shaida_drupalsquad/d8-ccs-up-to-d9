/**
 * @file
 *
 */

(function($) {
  "use strict";

  $(document).ready(function() {
    $('body').addClass('js');
    var $menu = $('.region-header-mobile-menu');
    var $menulink = $('.menu-toggle a');

    $menulink.on('click', function() {
      $menulink.toggleClass('active');
      $menu.toggleClass('active');
      return false;
    });
  });

  $(document).ready(function() {
    $('body').addClass('js');
    var $menu = $('.region-header-mobile-menu-landing');
    var $menulink = $('.menu-toggle a');

    $menulink.on('click', function() {
      $menulink.toggleClass('active');
      $menu.toggleClass('active');
      Drupal.attachBehaviors('div.region-header-mobile-menu-landing');
      return false;
    });
  });

})(jQuery);
