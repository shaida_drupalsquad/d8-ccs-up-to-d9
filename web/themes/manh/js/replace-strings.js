/**
 * @file
 * Replaces non-formatted TM and R to ™ and ®.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.replaceString = {
    attach: function (context, settings) {
      $('body :not(script)', context).contents().filter(function() {
        return this.nodeType === 3;
      }).replaceWith(function() {
        return this.nodeValue.replace(/[™®©]/g, '<sup>$&</sup>');
      });
    }
  }
})(jQuery, Drupal);
