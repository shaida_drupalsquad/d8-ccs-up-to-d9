(function($, Drupal) {
  "use strict";

  Drupal.behaviors.searchMobile = {
    attach: function(context, settings) {
      if (context === document) {
        if (Modernizr.touch) {
          $('.region-header-secondary .search-block-form h2').on('click', function() {
            $('#search-block-form').css('width', '200px');
            return false;
          })
        }
      }
    }
  };

})(jQuery, Drupal);