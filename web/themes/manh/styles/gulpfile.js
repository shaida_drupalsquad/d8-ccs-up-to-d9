/*eslint strict: ['error, 'global']*/
'use strict';

// Getting started with Gulp
// You've already got Node JS installed don't you? If not hit up nodejs.org and hit the big install button.
//
// A .nvmrc file is located in the theme to identify the node version
//
// Switch to the node version defined for this project. Uses the .nvmrc file to identify the node version.
// nvm use
//
// If output states that the node version is not installed, run this command to install the node version.
// nvm install
//
// Next install all dependencies declared in the package.json file. All dependancies will be installed to /node_modules directory.
// npm install
//
// Sweet. Gulp should be ready to go. Let's make it do work. Here's a list of the available gulp commands.
//
// gulp (or build)
// The main build process that does all the things. If not leaving the site in the "watch" state, this is the normal "run the theme build" command.
// `npm run gulp` or `npm run build`
//
// gulp sass (or compile)
// Runs the compiling of the styles one time. Does not compile the IR CSS files.
// `npm run gulp-sass` or `npm run compile`
//
// gulp watch (or watch)
// Watches the sass directory for changes. As soon as a file changes the sass task is fired and the css is regenerated.
// npm run gulp-watch or npm run watch
//
// gulp styleguide
// Runs the styleguide build process. Not currently part of `build` task.
// npm run gulp-styleguide


//////////////////////////////////////////////
// Include gulp
var gulp = require('gulp');

//////////////////////////////////////////////
// Include Our Plugins
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var shell = require('gulp-shell');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var replace = require('gulp-replace');
var runSequence = require('run-sequence');

//////////////////////////////////////////////
// Define file paths and configurations
var config = {
  images: 'images/*',
  build: 'images',
  sass: 'sass/{,**/}*.{scss,sass}',
  icons: './images/icons'
};

//////////////////////////////////////////////
// Compile Our Sass
gulp.task('sass', function () {
  gulp.src([
    './sass/{,**/}*.scss',
    '!./sass/fonts.scss',
    '!./sass/nav_drawer.scss',
    '!./sass/ir.scss'
  ])
    .pipe(sass({
      outputStyle: 'nested',
      errLogToConsole: true
    }))
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('css'));
});

//////////////////////////////////////////////
// Compile Our Sass for IR distrubution.
gulp.task('sass-ir', function () {
  gulp.src([
    './sass/manh.scss',
    './sass/fonts.scss',
    './sass/nav_drawer.scss',
    './sass/ir.scss'
  ])
    .pipe(sass({
      outputStyle: 'nested',
      errLogToConsole: true
    }))
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(replace('../images/', 'https://www.manh.com/themes/manh/styles/images/'))
    .pipe(gulp.dest('css/ir'));
});

//////////////////////////////////////////////
// Compress svg and image files
gulp.task('images', function () {
  return gulp.src(config.images)
    .pipe(imagemin({
      progressive: true,
      use: [pngquant()]
    }))
    // Move compress image files to build folder
    .pipe(gulp.dest('images'));
});


//////////////////////////////////////////////
// Generate a style guide using KSS
gulp.task('styleguide', shell.task([
  config.styleguide
], {
  templateData: {
    source: 'sass',
    destination: 'styleguide',
    template: 'styleguide/Theme'
  }
}
));


//////////////////////////////////////////////
// Watch and recompile sass.
// Refreshes the CSS in the browser with browsersync.
gulp.task('watch', function () {

  // Watch all my sass files.
  gulp.watch('sass/{,**/}*.{scss,sass}', ['sass']);
  // Watch all my ir sass files.
  gulp.watch('sass/{,**/}*.{scss,sass}', ['sass-ir']);
  // Watch ./image folder for changes
  gulp.watch(config.images, [config.build]);
});

//////////////////////////////////////////////
// Default Task
gulp.task('default', function (callback) {
  runSequence(
    ['images', 'sass', 'sass-ir'],
    callback
  );
});
